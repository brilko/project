# О проекте
Это учебный проект команды Битый байт в компании Отус, направление обученния C# ASP.NET Core разработчик. Тема проекта -- интренет магазин для магазина одежды с одной точкой продаж.

# Команда
- Тарабановская Юлия 
- Брилько Дмитрий 
- Василий Симонов (ушёл)
- Кадыров Тимур (ушёл)
- Нургали Динара (ушла)
- Серебряков Григорий (ушёл)

# Лицензия
MIT -- свободное использование.

