﻿namespace EmailSender
{
    public class SmtpSettings
    {
        public string EmailFrom { get; set; } = string.Empty;
        public string EmailTo { get; set; } = string.Empty;
        public string SmtpHost { get; set; } = string.Empty;
        public int SmtpPort { get; set; }
        public string SmtpUser { get; set; } = string.Empty;
        public string SmtpPass { get; set; } = string.Empty;
        public bool IsSSLConnection { get; set; }
    }
}
