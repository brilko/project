﻿namespace EmailLibrary
{
    public interface IEmailService
    {
        Task Send(string subject, string text, string? to = null, string? from = null);
    }
}
