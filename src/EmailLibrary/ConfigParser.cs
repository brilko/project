﻿using EmailSender;
using Microsoft.Extensions.Configuration;

namespace EmailLibrary
{
    internal class ConfigParser
    {
        public SmtpSettings ReadSMTPSettings()
        {
            var config = new ConfigurationBuilder().AddUserSecrets<SmtpSettings>().Build();
            IConfigurationSection smtpSettingsConfig = config.GetSection("SMTPSettings") ?? throw new Exception();
            var smtpSettings = new SmtpSettings()
            {
                EmailFrom = smtpSettingsConfig["EmailFrom"],
                EmailTo = smtpSettingsConfig["EmailTo"],
                SmtpHost = smtpSettingsConfig["SmtpHost"],
                SmtpPort = int.Parse(smtpSettingsConfig["SmtpPort"]),
                SmtpPass = smtpSettingsConfig["SmtpPass"],
                SmtpUser = smtpSettingsConfig["SmtpUser"],
                IsSSLConnection = bool.Parse(smtpSettingsConfig["IsSSLConnection"])
            };
            return smtpSettings;
        }
    }
}
