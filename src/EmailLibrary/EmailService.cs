using EmailLibrary;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace EmailSender
{
    public class EmailService : IEmailService
    {
        private readonly SmtpSettings smtpSettings;

        public EmailService(SmtpSettings? smtpSettings = null)
        {
            this.smtpSettings = smtpSettings ?? new ConfigParser().ReadSMTPSettings();
        }

        public async Task Send(string subject, string text, string? to = null, string? from = null)
        {
            // create message
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(from ?? smtpSettings.EmailFrom));
            email.To.Add(MailboxAddress.Parse(to ?? smtpSettings.EmailTo));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Text) { Text = text };

            // send email
            using SmtpClient smtp = new();
            await smtp.ConnectAsync(smtpSettings.SmtpHost, smtpSettings.SmtpPort, 
                smtpSettings.IsSSLConnection);
            await smtp.AuthenticateAsync(smtpSettings.SmtpUser, smtpSettings.SmtpPass);
            await smtp.SendAsync(email);
            await smtp.DisconnectAsync(true);
        }
    }
}