﻿namespace IdentityServer.Data
{
    public class MongoConfigurationOptions
    {
        public string Connection { get; set; }
        public string DatabaseName { get; set; }
    }
}
