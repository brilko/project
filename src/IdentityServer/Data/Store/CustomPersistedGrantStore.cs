﻿using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Data.Store
{
    /// <summary>
    /// Handle consent decisions, authorization codes, refresh and reference tokens
    /// </summary>
    public class CustomPersistedGrantStore : IPersistedGrantStore
    {
        protected IRepository _dbRepository;

        public CustomPersistedGrantStore(IRepository repository)
        {
            _dbRepository = repository;
        }

        public Task<IEnumerable<PersistedGrant>> GetAllAsync(string subjectId)
        {
            var result = _dbRepository.Where<PersistedGrant>(i => i.SubjectId == subjectId);
            return Task.FromResult(result.AsEnumerable());
        }

        public Task<IEnumerable<PersistedGrant>> GetAllAsync(PersistedGrantFilter filter)
        {
            filter.Validate();
            var result = _dbRepository.Where<PersistedGrant>(i =>
            (!string.IsNullOrEmpty(filter.SubjectId)) && (i.SubjectId == filter.SubjectId) &&
            (!string.IsNullOrEmpty(filter.SessionId)) && (i.SessionId == filter.SessionId) &&
            (!string.IsNullOrEmpty(filter.ClientId)) && (i.ClientId == filter.ClientId) &&
            (!string.IsNullOrEmpty(filter.Type)) && (i.Type == filter.Type));

            return Task.FromResult(result.AsEnumerable());
        }

        public Task<PersistedGrant> GetAsync(string key)
        {
            var result = _dbRepository.Single<PersistedGrant>(i => i.Key == key);
            return Task.FromResult(result);
        }

        public Task RemoveAllAsync(string subjectId, string clientId)
        {
            _dbRepository.Delete<PersistedGrant>(i => i.SubjectId == subjectId && i.ClientId == clientId);
            return Task.FromResult(0);
        }

        public Task RemoveAllAsync(string subjectId, string clientId, string type)
        {
            _dbRepository.Delete<PersistedGrant>(i => i.SubjectId == subjectId && i.ClientId == clientId && i.Type == type);
            return Task.FromResult(0);
        }

        public Task RemoveAllAsync(PersistedGrantFilter filter)
        {
            _dbRepository.Delete<PersistedGrant>(i =>
           (!string.IsNullOrEmpty(filter.SubjectId)) && (i.SubjectId == filter.SubjectId) &&
           (!string.IsNullOrEmpty(filter.SessionId)) && (i.SessionId == filter.SessionId) &&
           (!string.IsNullOrEmpty(filter.ClientId)) && (i.ClientId == filter.ClientId) &&
           (!string.IsNullOrEmpty(filter.Type)) && (i.Type == filter.Type));
            return Task.FromResult(0);
        }

        public Task RemoveAsync(string key)
        {
            _dbRepository.Delete<PersistedGrant>(i => i.Key == key);
            return Task.FromResult(0);
        }

        public Task StoreAsync(PersistedGrant grant)
        {
            _dbRepository.Add<PersistedGrant>(grant);
            return Task.FromResult(0);
        }
    }
}
