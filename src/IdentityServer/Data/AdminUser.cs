﻿using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace IdentityServer.Data
{
    public class AdminUser
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public interface IAdminUserCreator
    {
        Task Create();
    }
    public class AdminUserCreator : IAdminUserCreator
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly AdminUser _user;

        public AdminUserCreator(IOptions<AdminUser> appSettings,
             UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _user = appSettings.Value;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Create()
        {
            if (_roleManager == null)
            {
                throw new ArgumentException("Null", nameof(_roleManager));
            }

            bool adminRoleExists = await _roleManager.RoleExistsAsync("Admin");

            if (!adminRoleExists)
            {
                await _roleManager.CreateAsync(new ApplicationRole() { Name = "Admin" });
            }

            if (_userManager == null)
            {
                throw new ArgumentException("Null", nameof(_userManager));
            }

            ApplicationUser appUser = new()
            {
                UserName = _user.Email,
                Email = _user.Email,
                EmailConfirmed = true
            };
            var adminUser = await _userManager.FindByEmailAsync(_user.Email);
            if (adminUser == null)
            {
                IdentityResult result = await _userManager.CreateAsync(appUser, _user.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(appUser, "Admin");
                }
            }
        }
    }
}
