﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using EmailSender;
using IdentityServer4.Services;
using IdentityServerHost.Quickstart.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace IdentityServerHost.Services
{
    [SecurityHeaders]
    [AllowAnonymous]
    public class EmailController : Controller
    {
        private readonly IEmailService _mailService;
        private readonly ILogger _logger;

        public EmailController(
            IEmailService mailService,
            IEventService events, ILogger<EmailController> logger)
        {
            _mailService = mailService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> SendEmail(string to, string subject, string html, string from = null)
        {
            try
            {
                await _mailService.Send(to, subject, html, from);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest(ex.ToString());
            }
        }
    }
}