import { Grid, Box, Tooltip, Typography, Container } from '@mui/material';
import IconButton from '@mui/material/Button';
import ArrowBack from '@mui/icons-material/ArrowBack';
import { Link } from "react-router-dom";

type Props = {
    children?: React.ReactNode;
    name: string;    
};


export default function FormContaner(prop: Props) {

    return (<>
        <Container component="main" maxWidth="xs">
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Grid container spacing={2}>
                    <Grid item xs={1}>
                        <Tooltip title="На главную страницу">
                            <Link to="/">
                                <IconButton size="small" >
                                    <ArrowBack />
                                </IconButton>
                            </Link>
                        </Tooltip>
                    </Grid>
                    <Grid item xs={11}>
                        <Typography component="h1" align="center" variant="h5">
                            {prop.name}
                        </Typography>
                    </Grid>
                </Grid>
                {prop.children}
            </Box>
        </Container>
    </>);
}