
import { Alert } from '@mui/material';

type ErrorProps = {
    error: string;
    show: boolean;
};

export default function ErrorAlert(props: ErrorProps) {
    return (
        <>
            {props.show === true &&
                <>
                    <Alert variant="outlined" severity="error">
                        {props.error}
                    </Alert>
                </>
            }
        </>
    );
}