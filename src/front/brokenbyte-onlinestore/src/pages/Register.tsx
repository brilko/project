import React, { useState, useEffect } from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { register, logout, selectCreateNewUser, selectUserActionError } from '../userSlice';
import FormContaner from './components/FormContainer'
import UserTextField from './components/UserTextField';

export default function Register() {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const actionError = useAppSelector(selectUserActionError);
    const userStatus = useAppSelector(selectCreateNewUser);

    const [password2Error, setPassword2Error] = useState(false);
    const [userNameError, setUserNameError] = useState(false);
    const [doRegister, setDoRegister] = useState(false);

    const [inputName, setInputName] = useState('');
    const [inputPassword, setInputPassword] = useState('');
    const [inputPassword2, setInputPassword2] = useState('');


    useEffect(() => {
        if (userStatus === true) {
            setDoRegister(true);
            dispatch(logout());
        }
        if (actionError !== '') {
            setUserNameError(true);
        }
    }, [actionError, userStatus, dispatch]);


    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (inputPassword !== inputPassword2) {
            setPassword2Error(true);
        }
        else {
            dispatch(register({ name: inputName, password: inputPassword }));
        }
    }

    const setName = (text: string) => {
        setInputName(text);
        setUserNameError(false);
    }
    const setPassword = (text: string) => {
        setInputPassword(text);
        setPassword2Error(false);
    }

    const setPassword2 = (text: string) => {
        setInputPassword2(text);
        setPassword2Error(false);
    }

    return (
        <>
            <FormContaner name='Регистрация' >

                {doRegister === false &&
                    <Box
                        component="form"
                        onSubmit={submitForm}
                        sx={{ mt: 1 }}
                    >

                        <UserTextField
                            error={userNameError}
                            inputText={inputName}
                            setInputText={setName}
                            label="Имя пользователя"
                            type='text'
                            errorText="Такой пользователь уже существует"
                        />

                        <UserTextField
                            error={false}
                            inputText={inputPassword}
                            setInputText={setPassword}
                            label="Пароль"
                            type='password'
                        />

                        <UserTextField
                            error={password2Error}
                            inputText={inputPassword2}
                            setInputText={setPassword2}
                            label="Повторите пароль"
                            type='password'
                            errorText="Пароли не совпадают"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            size="large"
                            sx={{ mt: 2, mb: 2 }}
                        >
                            Зарегистрироваться
                        </Button>
                        <Link to="/login"  >
                            Войти?
                        </Link>
                    </Box>
                }
                {doRegister === true &&
                    <>
                        <Typography variant="h6" sx={{ mt: 2 }} align="center" color="text.secondary" paragraph>
                            Вы успешно зарегистированы!
                        </Typography>
                        <Button type="submit"
                            fullWidth
                            variant="contained"
                            size="large"
                            sx={{ mt: 2, mb: 2 }}
                            onClick={() => { navigate("/login"); }}
                        >Вход</Button>

                    </>
                }
            </FormContaner>
        </>
    );
}
