import { Outlet } from "react-router-dom";
import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Header from '../components/Header'
import Footer from "../components/Footer";

export default function Root() {
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                minHeight: '100vh',
            }}
        >
            <CssBaseline />
            <Header />
            <Container component="main" sx={{ mt: 8, mb: 2 }} maxWidth="sm">

                <div id="detail">
                    <Outlet />
                </div>
            </Container>

            <Footer />

        </Box>
    );
}