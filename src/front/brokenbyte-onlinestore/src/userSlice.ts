import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './app/store';


export interface User {
    name: string;
    password: string;
}

export interface UserState {
    loginUser: User;
    error: string;
    createNewUser:Boolean;
    users: Array<User>;
}


const initialState: UserState = {
    loginUser: { name: '', password: '' },
    error: '',
    createNewUser: false,
    users: [{ name: '111', password: '111' }, { name: 'bob', password: 'bob' }, { name: 'alice', password: 'alice' }]
};


export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        login: (state, action: PayloadAction<User>) => {
            console.log('login');
            if (state.users.findIndex(x => (x.name === action.payload.name) && (x.password === action.payload.password)) !== -1) {
                state.loginUser = action.payload;
                state.error = '';
                state.createNewUser=false;
                
            }
            else {
                state.error = 'Неправильные имя пользователя или пароль';
                state.loginUser = { name: '', password: '' };
                state.createNewUser=false;
            }
        },

        logout: (state) => {
            console.log('logout');
            state.createNewUser=false;
            state.error = '';
            state.loginUser = { name: '', password: '' };
        },
       
        register: (state, action: PayloadAction<User>) => {
            if (state.users.findIndex(x => (x.name === action.payload.name)) !== -1) {
                state.error = 'Пользователь с таким именем уже зарегистрирован';
                state.createNewUser=false;
            }
            else {
                state.error = '';
                state.users.push(action.payload);
                state.createNewUser=true;
            }
        },
    }
});

export const { login, logout, register } = userSlice.actions;
export const selectUser = (state: RootState) => state.user.loginUser;
export const selectUserActionError = (state: RootState) => state.user.error;
export const selectCreateNewUser = (state: RootState) => state.user.createNewUser;
export default userSlice.reducer;
