import CartWidget from './CartWidget';
import { AppBar, Box, Toolbar, Typography, Button, IconButton, Menu, MenuItem, Tooltip } from '@mui/material';
import AccountCircle from '@mui/icons-material/AccountCircle';
import StoreIcon from '@mui/icons-material/Store';
import {
    useNavigate
} from "react-router-dom";
import { useAppSelector, useAppDispatch } from '../app/hooks';
import { logout, selectUser } from '../userSlice';
import * as React from 'react';

export default function Header() {
    const navigate = useNavigate();
    const user = useAppSelector(selectUser);
    const dispatch = useAppDispatch();

    const logoutClick = () => {
        setAnchorEl(null);
        dispatch(logout());
    };

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const profileClick = () => {
        setAnchorEl(null);
        navigate('/profile');
    };
    return (

        <AppBar position="relative">
            <Toolbar>
                <Tooltip title="На главную страницу">
                    <IconButton sx={{ mr: 2 }}
                        size="large"
                        edge="end"
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={() => { navigate('/'); }}
                        color="inherit"

                    >
                        <StoreIcon />
                    </IconButton>

                </Tooltip>
                <Typography variant="h6" color="inherit" noWrap>
                    Интернет-магазин
                </Typography>

                <Box sx={{ flexGrow: 1 }} />
                <Box sx={{ display: { xs: 'flex', md: 'flex' } }}>

                    <CartWidget productsCount={1} />
                    {user.name === '' &&
                        <Tooltip title="Войти">
                            <Button color="inherit" onClick={() => { navigate('/login'); }}>Вход</Button>
                        </Tooltip>}
                    {user.name !== '' &&
                        <>
                            <Tooltip title={user.name}>
                                <IconButton
                                    size="large"
                                    edge="end"
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    color="inherit"
                                >
                                    <AccountCircle />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={profileClick}>Профиль</MenuItem>
                                <MenuItem onClick={logoutClick}>Выйти</MenuItem>
                            </Menu></>}
                </Box>


            </Toolbar>
        </AppBar>


    );
}