import { useEffect, useState } from 'react'
import axios from 'axios';
import { useParams, useNavigate } from 'react-router-dom';
import {
  Button, Container, Typography, Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Box, CardActionArea
} from '@mui/material';

export type Product = {
  id: string;
  article: string;
  shortName: string;
  fullName: string;
  visible: boolean;
  image: string;
  manufacturerId: string;
  categoryId: string;
  avialableCount: string;
  price: string;
}

export default function ProductList() {

  const qweryParams = useParams();
  const [error, setError] = useState(false);
  const [products, setProducts] = useState<Product[]>([]);
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      axios.post(`${process.env.REACT_APP_BACK_URL}api/v1/Product/list`,
        {
          categoryId: qweryParams.id,
          itemsPerPage: 10,
          page: 1
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
        .then(function (response) {
          setProducts(response.data);
          console.log(response);
        })
        .catch(function (error) {
          setError(error);
          console.log(error);
        })
        .finally(function () {
          // выполняется всегда
        });
    }

    fetchData();
  }, [qweryParams])

  // eslint-disable-next-line react-hooks/exhaustive-deps


  const addToCart = (product: Product): void => {
    ///TODO добавление в корзину
  }

  return (
    <Box
      sx={{
        bgcolor: 'background.paper',
        pt: 0,
        pb: 1,
      }}
    >
      <Typography
        component="h2"
        variant="h3"
        align="center"
        color="text.primary"
        gutterBottom
      >
        Товары
      </Typography>
      <Container sx={{ py: 2 }} maxWidth="md">
        <Grid container spacing={4}>
          {error === false &&
            products.map((product: Product) => (
              <Grid item key={product.id} xs={12} sm={3} md={6}>

                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }} >
                  <CardActionArea onClick={() => { navigate('/products/' + product.id); }}>
                    
                      <CardMedia
                        component="div"
                        sx={{
                          // 16:9
                          pt: '56.25%',
                          pb: '56.25%',
                        }}
                        image={`/images/coat.jpg`}
                      />
                    <CardContent sx={{ flexGrow: 1 }}>
                      <Typography gutterBottom variant="h5" component="h2">
                        {product.price} &#8381;
                      </Typography>
                      <Typography  >
                        {product.shortName}
                      </Typography>
                    </CardContent>

                    <CardActions>
                      <Button size="small" onClick={() => { addToCart(product) }}>В корзину</Button>
                    </CardActions>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
        </Grid>
      </Container>
    </Box>
  );
}

/*<section className={classes.productslistPage}>
      <h1>Товары</h1>
      <div className={classes.container}>
        {error===false && products.map((product: Product) => (
            <div className={classes.category} key={product.id}>
              <img src={'images/' + product.image} id='img' alt={product.fullName}/>
              <h3>{product.shortName}</h3>             
            <p>Цена: {product.price}</p>
            <button  onClick={() => addToCart(product)}>В корзину</button>
            </div>
          ))}      
      </div>

    </section> */