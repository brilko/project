import { ButtonBase, Grid, Toolbar, Typography, Button, IconButton, Menu, MenuItem, Tooltip } from '@mui/material';
import { useParams, useNavigate } from 'react-router-dom';
import { Product } from './ProductList';
import { useEffect, useState } from 'react'
import axios from 'axios';
import { styled } from '@mui/material/styles';


export default function ProductCard() {
    const qweryParams = useParams();
    const [product, setProduct] = useState<Product>();
    const navigate = useNavigate();

    const Img = styled('img')({
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    });

    useEffect(() => {
        async function fetchData() {
            axios.get(`${process.env.REACT_APP_BACK_URL}api/v1/Product/${qweryParams.id}`,
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
                .then(function (response) {
                    setProduct(response.data);
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                    // navigate('/does-not-exist');
                })
                .finally(function () {
                    // выполняется всегда
                });
        }

        fetchData();
    }, [qweryParams, navigate])

    return (
        <Grid container spacing={4} direction="row">
            <Grid item xs={6}>
                <Img alt="complex" src={`/images/coat.jpg`} />
            </Grid>
            <Grid item xs={6}>
                <Typography component="h2" variant="h5">
                    {product?.shortName}
                </Typography>
                <Typography variant="subtitle1" color="text.secondary">
                    Арт. {product?.article}
                </Typography>
                <Typography variant="subtitle1" paragraph>
                    {product?.fullName}
                </Typography>
                <Typography variant="h4" component="h1" gutterBottom>
                    {product?.price} &#8381;
                </Typography>
                <Button variant="contained">В корзину</Button>
            </Grid>
        </Grid>


    );
}