import { useEffect, useState } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

export type Category = {
  id: string;
  name: string;
  image: string;
  description: string;
}

export default function CategoriesList() {

  const navigate = useNavigate();
  const [error, setError] = useState(false);
  const [categories, setCategories] = useState<Category[]>([]);

  useEffect(() => {
    fetchData(`${process.env.REACT_APP_BACK_URL}api/v1/Category/list`);
  }, [])

  async function fetchData(url: string) {

    axios.post(url,
      {
        itemsPerPage: 10,
        page: 1
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
      .then(function (response) {
        setCategories(response.data);
        console.log(response);
      })
      .catch(function (error) {
        setError(error);
        console.log(error);
      })
      .finally(function () {
        // выполняется всегда
      });
  }

  const LinkToProducts = (category: Category): void => {
    navigate('categories/' + category.id);
  }


  return (
    <Box
      sx={{
        bgcolor: 'background.paper',
        pt: 0,
        pb: 1,
      }}
    >
      <Typography
        component="h2" variant="h4"
        align="center"
        color="text.primary"
        gutterBottom
      >
        Категории
      </Typography>
      <Container sx={{ py: 8 }} maxWidth="md">
        <Grid container spacing={4}>
          {error === false &&
            categories.map(category => (
              <Grid item key={category.id} xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column', maxWidth: 345  }}
                  onClick={() => LinkToProducts(category)}                >
                  <CardMedia
                    component="div"
                    sx={{
                      // 16:9
                      pt: '56.25%',
                      pb:'56.25%',
                    }}
                    image={'images/' + category.image}
                  />
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2" >
                      {category.name}
                    </Typography>
                    {category.description!=='' &&
                    <Typography>
                      {category.description}
                    </Typography>}
                  </CardContent>
                </Card>
              </Grid>
            ))}


        </Grid>
      </Container>
    </Box>
  );
}