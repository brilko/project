import {
    useNavigate 
} from "react-router-dom";
import {Badge, IconButton} from '@mui/material';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

interface ProductCountProps {
    productsCount: number
  }
export default function CartWidget(props:ProductCountProps) {
    const navigate = useNavigate();

    return (
        <IconButton aria-label="cart"  size="large" color="inherit" onClick={()=>{navigate('/shoppingcart');}}>
        <Badge badgeContent={props.productsCount} color="warning">
            <ShoppingCartIcon />
        </Badge>
    </IconButton>     
    );
}