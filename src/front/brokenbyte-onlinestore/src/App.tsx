import React from 'react';
import './App.css';
import {
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
} from "react-router-dom";

import Root from './pages/Root';
import LoginPage from './pages/LoginPage';
import Register from './pages/Register';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Page404 from './components/Page404';

import CategoriesList from "./components/CategoriesList";
import ProductList from "./components/ProductList";
import ProductCard from './components/ProductCard';


export default App;
function App() {
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route
        path="/"
        element={<Root />}
        errorElement={<Page404 />}             >
        <Route index element={<CategoriesList />} />
        <Route errorElement={<Page404 />}>
          <Route path="categories/:id" element={<ProductList />} />
          <Route path="products/:id" element={<ProductCard />} />
          <Route
            path="login"
            element={<LoginPage />}
          />
          <Route
            path="register"
            element={<Register />}
          />
        </Route>
      </Route>
    )
  );
  const theme = createTheme();

  return (
    <ThemeProvider theme={theme}>
      <RouterProvider router={router} />
    </ThemeProvider>
  );
}


