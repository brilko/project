import React from 'react';
import './App.css';
import {
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
} from "react-router-dom";

import Root from './components/Root';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Page404 from './components/Page404';

import CategoriesList from "./components/CategoriesList";
import CategoryEdit from "./components/CategoryEdit";
import ProductEdit from './components/ProductEdit';
import MainView from "./components/MainView";
import ProductList from './components/ProductList';

export default App;
function App() {
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route
        path="/"
        element={<Root />}
        errorElement={<Page404 />}             >
        <Route index element={<MainView />} />
        <Route errorElement={<Page404 />}>
          <Route path="categories" element={ <CategoriesList />} />
          <Route path="categories/:id" element={<CategoryEdit />} /> 
          <Route path="products" element={<ProductList />} />   
          <Route path="products/:id" element={<ProductEdit />} />          
        </Route>
      </Route>
    )
  );
  const theme = createTheme();

  return (
    <ThemeProvider theme={theme}>
      <RouterProvider router={router} />
    </ThemeProvider>
  );
}


