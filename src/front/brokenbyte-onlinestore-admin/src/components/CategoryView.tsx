import { Card, CardContent, CardMedia, Typography } from '@mui/material';
import { Category } from './CategoriesList';

export default function CategoryView(props: Category) {
  return (
    <Card
      sx={{ height: '100%', display: 'flex', flexDirection: 'column', maxWidth: 345 }}                           >
      <CardMedia
        component="div"
        sx={{
          // 16:9
          pt: '56.25%',
          pb: '56.25%',
        }}
        image={ props.image}
      />
      <CardContent sx={{ flexGrow: 1 }}>
        <Typography gutterBottom variant="h5" component="h2" >
          {props.name}
        </Typography>
        {props.description !== '' &&
          <Typography>
            {props.description}
          </Typography>}
      </CardContent>
    </Card>);
}