import { useEffect, useState } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Title from './Title'
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
export type Category = {
  id: string;
  name: string;
  image: string;
  description: string;
}

export default function CategoriesList() {

  const navigate = useNavigate();
  const [error, setError] = useState(false);
  const [categories, setCategories] = useState<Category[]>([]);
  const [selected, setSelected] = useState<string>();

  useEffect(() => {
    fetchData(`${process.env.REACT_APP_BACK_URL}api/v1/Category/list`);
  }, [])

  async function fetchData(url: string) {

    axios.post(url,
      {
        itemsPerPage: 10,
        page: 1
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
      .then(function (response) {
        setCategories(response.data);
        console.log(response);
      })
      .catch(function (error) {
        setError(error);
        console.log(error);
      })
      .finally(function () {
        // выполняется всегда
      });
  }

  return (
    <Box
      sx={{
        bgcolor: 'background.paper',
        pt: 0,
        pb: 1,
      }}
    >
      <Title>Категории</Title>
      <Button variant="contained">+</Button>
      {error === false &&
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Наименование</TableCell>
              <TableCell>Ссылка на картинку</TableCell>
              <TableCell>Описание</TableCell>
              <TableCell >Действия</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {categories.map((category) => {
              const isItemSelected = (selected === category.id);
              return (
                <TableRow key={category.id}
                  hover
                  onClick={() => setSelected(category.id)}
                  role="checkbox"
                  aria-checked={isItemSelected}
                  tabIndex={-1}
                  selected={isItemSelected}
                  sx={{ cursor: 'pointer' }}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      color="primary"
                      checked={isItemSelected}
                      inputProps={{
                        'aria-labelledby': category.id,
                      }}
                    />
                  </TableCell>
                  <TableCell>{category.name}</TableCell>
                  <TableCell>{category.image}</TableCell>
                  <TableCell>{category.description}</TableCell>

                  <TableCell>
                    <Stack direction="row" spacing={1} >
                      <IconButton edge="end" aria-label="delete"
                        onClick={() => navigate('/categories/' + category.id)}
                      >
                        <EditIcon />
                      </IconButton>

                      <IconButton edge="end" aria-label="delete">
                        <DeleteIcon />
                      </IconButton>
                    </Stack>
                  </TableCell>
                </TableRow>)
            }
            )}
          </TableBody>
        </Table>
      }

    </Box>
  );
}
