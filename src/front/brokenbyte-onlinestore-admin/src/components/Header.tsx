
import { AppBar, Box, Toolbar, Typography, Button, IconButton, Menu, MenuItem, Tooltip } from '@mui/material';
import StoreIcon from '@mui/icons-material/Store';
import {
    useNavigate
} from "react-router-dom";
import * as React from 'react';

export default function Header() {
    const navigate = useNavigate();

    return (

        <AppBar position="relative">
            <Toolbar>
                <Tooltip title="На главную страницу">
                    <IconButton sx={{ mr: 2 }}
                        size="large"
                        edge="end"
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={() => { navigate('/'); }}
                        color="inherit"

                    >
                        <StoreIcon />
                    </IconButton>

                </Tooltip>
                <Typography variant="h6" color="inherit" noWrap>
                    Интернет-магазин
                </Typography>
            </Toolbar>
        </AppBar>
    );
}