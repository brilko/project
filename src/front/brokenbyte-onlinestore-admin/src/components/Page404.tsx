
const Page404 = () => {

    const css = {
        margin: '10px',
        padding:'10px',
    };

    return <div style={css}>404</div>
};

export default Page404;