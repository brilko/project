import './CategoryEdit.css';
import { useState, useEffect } from 'react'
import Title from './Title'
import { TextField, Button, Stack } from '@mui/material';

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import CategoryView from './CategoryView';

export default function CategoryEdit() {

    const qweryParams = useParams();

    useEffect(() => {
        fetchData(`${process.env.REACT_APP_BACK_URL}api/v1/Category/${qweryParams.id}`);
    }, [qweryParams]);


    async function fetchData(url: string) {
        await axios.get(url, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {

                setInputName(response.data.name);
                setInputDesc(response.data.description);
                setInputImage(response.data.image);
                setShowImage(`http://localhost:3000/images/${response.data.image}`);
                setErrorText('');

            })
            .catch(function (error) {
                setErrorText(error);
            })
            .finally(function () {
                // выполняется всегда
            });
    }

    const [saveSuccess, setSaveSuccess] = useState(false);
    const [errorText, setErrorText] = useState("");
    const [inputName, setInputName] = useState("");
    const [inputDesc, setInputDesc] = useState("");
    const [inputImage, setInputImage] = useState("");
    const [showImage, setShowImage] = useState('');


    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

            async function fetchData() {
              axios.put(`${process.env.REACT_APP_BACK_URL}api/v1/Category/${qweryParams.id}`,
                {
                    name: inputName,
                    description: inputDesc,
                    image: inputImage,
                },
                {
                  headers: {
                    'Content-Type': 'application/json'
                  }
                }
              )
                
            .then((response) => {
                setSaveSuccess(true);
                console.log(response.data);
            })
            .catch((error) => {
                setSaveSuccess(false);
                console.error(error);
            });  
            fetchData();
          }
        }
    return (
        <>
            <Title>Редактирование категории</Title>
            <div className="row">
                <div className="column">
                    <Stack spacing={2} component="form"
                        onSubmit={submitForm}>
                        <TextField
                            label="Наименование категории"
                            value={inputName}
                            required
                            onChange={(e) => {
                                setInputName(e.target.value); setErrorText("");
                                setSaveSuccess(false);
                            }}
                            type='text'
                        />
                        <TextField
                            label="Описание"
                            multiline
                            onChange={(e) => {
                                setInputDesc(e.target.value); setErrorText("");
                                setSaveSuccess(false);
                            }}
                            value={inputDesc}
                        />
                        <TextField
                            label="Путь к картинке"
                            onChange={(e) => {
                                setInputImage(e.target.value);
                                setShowImage(`http://localhost:3000/images/${e.target.value}`);
                                setErrorText("");
                                setSaveSuccess(false);
                            }}
                            value={inputImage}
                        />
                        {showImage}

                        <Button
                            type="submit"

                            variant="contained"
                            size="large"
                        >
                            Сохранить
                        </Button>


                        {errorText !== "" &&

                            <Alert severity="error">
                                <AlertTitle>Error</AlertTitle>
                                {errorText}
                            </Alert>
                        }
                        {saveSuccess === true &&
                            <Alert severity="success">
                                <AlertTitle>Success</AlertTitle>
                                Данные успешно сохранены
                            </Alert>
                        }
                    </Stack>

                </div>
                <div className="column">
                    <CategoryView                    
                        name={inputName}
                        description={inputDesc}
                        image={showImage}
                        id="0"
                    />
                </div>
            </div>




        </>
    );
}