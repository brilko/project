import { useEffect, useState } from 'react'
import axios from 'axios';
import { useParams, useNavigate } from 'react-router-dom';
import {
  Button, Container, Typography,
  Grid,
  Checkbox,
  IconButton,
  Table, TableBody, TableCell, TableHead, TableRow, Stack
} from '@mui/material';

import Title from './Title'
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

export type Product = {
  id: string;
  article: string;
  shortName: string;
  fullName: string;
  visible: boolean;
  image: string;
  manufacturerId: string;
  manufacturer: string;
  category: string;
  categoryId: string;
  avialableCount: string;
  price: string;
}

export default function ProductList() {

  const qweryParams = useParams();
  const [error, setError] = useState(false);
  const [products, setProducts] = useState<Product[]>([]);
  const [selected, setSelected] = useState<string>();

  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      axios.post(`${process.env.REACT_APP_BACK_URL}api/v1/Product/list`,
        {
          categoryId: qweryParams.id,
          itemsPerPage: 10,
          page: 1
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
        .then(function (response) {
          setProducts(response.data);
          console.log(response);
        })
        .catch(function (error) {
          setError(error);
          console.log(error);
        })
        .finally(function () {
          // выполняется всегда
        });
    }
    fetchData();
  }, [qweryParams])


  return (
    <>
      <Title>Товары</Title>

      {error === false &&
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Наименование</TableCell>
              <TableCell>Артикул</TableCell>              
              <TableCell >Действия</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {error === false &&    
              products.map((product: Product) => {
                const isItemSelected = (selected === product.id);
                return (
                  <TableRow key={product.id}
                    hover
                    onClick={() => setSelected(product.id)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    selected={isItemSelected}
                    sx={{ cursor: 'pointer' }}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        color="primary"
                        checked={isItemSelected}
                        inputProps={{
                          'aria-labelledby': product.id,
                        }}
                      />
                    </TableCell>             
                    <TableCell>{product.shortName}</TableCell> 
                    <TableCell>{product.article}</TableCell> 
                   
                    <TableCell>
                      <Stack direction="row" spacing={1} >
                        <IconButton edge="end" aria-label="delete"
                          onClick={() => navigate('/products/' + product.id)}
                        >
                          <EditIcon />
                        </IconButton>

                        <IconButton edge="end" aria-label="delete">
                          <DeleteIcon />
                        </IconButton>
                      </Stack>
                    </TableCell>
                  </TableRow>)
              }
              )}
          </TableBody>
        </Table>
      }
    </>
  );
}
