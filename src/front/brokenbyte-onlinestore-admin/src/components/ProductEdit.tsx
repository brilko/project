import './CategoryEdit.css';
import { useState, useEffect } from 'react'
import Title from './Title'
import { TextField, Button, Stack } from '@mui/material';

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import ProductCard from './ProductCard';
import { Product } from './ProductList';

export default function ProductEdit() {

    const qweryParams = useParams();
    const [product, setProduct] = useState<Product>();

    useEffect(() => {
        fetchData(`${process.env.REACT_APP_BACK_URL}api/v1/Product/${qweryParams.id}`);
    }, [qweryParams]);


    async function fetchData(url: string) {
        await axios.get(url, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {

                setProduct(response.data);
                setInputName(response.data.shortName);
                setInputDesc(response.data.fullName);
                setInputImage(response.data.image);
                setInputArticle(response.data.article);
                setInputPrice(response.data.price);
                setShowImage(`http://localhost:3000/images/${response.data.image}`);
                setErrorText('');

            })
            .catch(function (error) {
                setErrorText(error);
            })
            .finally(function () {
                // выполняется всегда
            });
    }

    const [saveSuccess, setSaveSuccess] = useState(false);
    const [errorText, setErrorText] = useState(""); 

    const [inputName, setInputName] = useState(""); 
    const [inputArticle, setInputArticle] = useState("");
    const [inputDesc, setInputDesc] = useState("");
    const [inputImage, setInputImage] = useState("");
    const [showImage, setShowImage] = useState('');
    const [inputPrice, setInputPrice] = useState('');

    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

            async function fetchData() {
              axios.put(`${process.env.REACT_APP_BACK_URL}api/v1/Category/${qweryParams.id}`,
                {
                    
                },
                {
                  headers: {
                    'Content-Type': 'application/json'
                  }
                }
              )
                
            .then((response) => {
                setSaveSuccess(true);
                console.log(response.data);
            })
            .catch((error) => {
                setSaveSuccess(false);
                console.error(error);
            });  
            fetchData();
          }
        }
    return (
        <>
            <Title>Редактирование товара</Title>
            <div className="row">
                <div className="column">
                    <Stack spacing={2} component="form"
                        onSubmit={submitForm}>
                        <TextField
                            label="Наименование товара"
                            value={inputName}
                            required     
                            type='text'
                            onChange={(e) => {
                                setInputName(e.target.value);
                                 setErrorText("");
                                setSaveSuccess(false);
                            }}
                          
                        />
                        <TextField
                            label="Полное наименование"
                            value={inputDesc}
                           
                            type='text'
                            onChange={(e) => {
                                setInputName(e.target.value);
                                 setErrorText("");
                                setSaveSuccess(false);
                            }}
                          
                        />
                        <TextField
                            label="Артикул"
                            value={inputArticle}
                            onChange={(e) => {
                                setInputArticle(e.target.value);
                                 setErrorText("");
                                setSaveSuccess(false);
                            }}
                        />
                        <TextField
                            label="Путь к картинке"                           
                            value={inputImage}
                            onChange={(e) => {
                                setInputImage(e.target.value);
                                 setErrorText("");
                                setSaveSuccess(false);
                            }}
                        />
                        <TextField
                            label="Цена"                           
                            value={inputPrice}
                            onChange={(e) => {
                                setInputPrice(e.target.value);
                                 setErrorText("");
                                setSaveSuccess(false);
                            }}
                        />
                       
                        <Button
                            type="submit"

                            variant="contained"
                            size="large"
                        >
                            Сохранить
                        </Button>


                        {errorText !== "" &&

                            <Alert severity="error">
                                <AlertTitle>Error</AlertTitle>
                                {errorText}
                            </Alert>
                        }
                        {saveSuccess === true &&
                            <Alert severity="success">
                                <AlertTitle>Success</AlertTitle>
                                Данные успешно сохранены
                            </Alert>
                        }
                    </Stack>

                </div>
                <div className="column">
                   <ProductCard />
                </div>
            </div>




        </>
    );
}