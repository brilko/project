﻿using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Core.Repositories;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Linq.Expressions;

namespace BrokenByte.OnlineStore.Logistics.DataAccess
{
    public class MongoDbRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly IMongoCollection<T> _collection;

        public MongoDbRepository(IMongoCollection<T> collection)
        {
            _collection = collection;
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);

        }

        public async Task DeleteAsync(long id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null) throw new ArgumentException("Элемент для удаления не найден");
            entity.Deleted = true;
            await UpdateAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _collection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<T> GetByIdAsync(long id)
        {
            return await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _collection.FindAsync(predicate).Result.FirstAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _collection.FindAsync(predicate).Result.ToListAsync();
        }

        public async Task<ResponseByPage<T>> GetByPage(QueryByPage query)
        {
            var filter = Builders<T>.Filter.Eq(x => x.Deleted, false);
            var countFacet = AggregateFacet.Create("count", PipelineDefinition<T, AggregateCountResult>.Create(new[]    {
                PipelineStageDefinitionBuilder.Count<T>()
            }));
            var dataFacet = AggregateFacet.Create("data",
            PipelineDefinition<T, T>.Create(new[]
            {
                PipelineStageDefinitionBuilder.Skip<T>((query.PageIndex - 1) * query.ItemsPerPage),
                PipelineStageDefinitionBuilder.Limit<T>(query.ItemsPerPage),
            }));


            var aggregation = await _collection.Aggregate()
                .Match(filter)
                .Facet(countFacet, dataFacet)
                .ToListAsync();

            var count = aggregation.First()
                .Facets.First(x => x.Name == "count")
                .Output<AggregateCountResult>()
                ?.FirstOrDefault()
                ?.Count ?? 0;
            var totalPages = (int)Math.Ceiling((double)count / query.ItemsPerPage);

            var data = aggregation.First()
                .Facets.First(x => x.Name == "data")
                .Output<T>();

            return new ResponseByPage<T>()
            {
                TotalPages = totalPages,
                Items = data
            };
        }
        public async Task<ResponseByPage<T>> GetByPage(QueryByPage query, Expression<Func<T, bool>> predicate)
        {

            var countFacet = AggregateFacet.Create("count", PipelineDefinition<T, AggregateCountResult>.Create(new[]    {
                PipelineStageDefinitionBuilder.Count<T>()
            }));
            var dataFacet = AggregateFacet.Create("data",
            PipelineDefinition<T, T>.Create(new[]
            {

                PipelineStageDefinitionBuilder.Skip<T>((query.PageIndex - 1) * query.ItemsPerPage),
                PipelineStageDefinitionBuilder.Limit<T>(query.ItemsPerPage),
            }));


            var aggregation = await _collection.Aggregate()
                .Match(predicate)
                .Facet(countFacet, dataFacet)
                .ToListAsync();

            var count = aggregation.First()
                .Facets.First(x => x.Name == "count")
                .Output<AggregateCountResult>()
                ?.FirstOrDefault()
                ?.Count ?? 0;
            var totalPages = (int)Math.Ceiling((double)count / query.ItemsPerPage);

            var data = aggregation.First()
                .Facets.First(x => x.Name == "data")
                .Output<T>();

            return new ResponseByPage<T>()
            {
                TotalPages = totalPages,
                Items = data
            };
        }
    }
}
