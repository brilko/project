﻿using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using MongoDB.Driver;

namespace BrokenByte.OnlineStore.Logistics.DataAccess.Repositories
{
    public class MongoDbInitializer
         : IDbInitializer
    {
        private readonly IRepository<Order> _orderCollection;
        private readonly IRepository<Product> _productCollection;

        private readonly IMongoDatabase _database;

        public MongoDbInitializer(IMongoDatabase database,
            IRepository<Order> orderCollection,
            IRepository<Product> productCollection
            )
        {
            _database = database;
            _productCollection = productCollection;
            _orderCollection = orderCollection;

        }
        private void DeleteCollections()
        {
            var collections = _database.ListCollectionNames().ToList();
            foreach (string c in collections)
            {
                _database.DropCollection(c);
            }
        }

        public void InitializeDb()
        {
            DeleteCollections();
            foreach (var item in FakeDataFactory.Products)
            {
                _productCollection.AddAsync(item);
            }

            //foreach (var item in FakeDataFactory.Manufacturers)
            //{
            //	_manufacturerCollection.AddAsync(item);
            //}		

        }
    }

}
