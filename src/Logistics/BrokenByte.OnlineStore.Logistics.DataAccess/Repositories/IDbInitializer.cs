﻿namespace BrokenByte.OnlineStore.Logistics.DataAccess.Repositories
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
