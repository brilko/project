﻿using BrokenByte.OnlineStore.Logistics.Core.Domain;

namespace BrokenByte.OnlineStore.Logistics.DataAccess.Repositories
{
    public static class FakeDataFactory
    {
        public static List<Category> Categories => new()
        {
            new  () {
            Id = 1,
            Name = "Куртки",
            Image = "http://tailtail.net/otus/brockenbyte/coat.jpg"
        },
          new()
        {
            Id =  2,
            Name = "Ботинки",
            Image = "http://tailtail.net/otus/brockenbyte/boots.jpg"
        },
           new()  {
            Id =  3,
            Name = "Жилеты",
            Image = "http://tailtail.net/otus/brockenbyte/waistcoat.jpg"
        },

            new Category()
            {
                Id = 4,
                Name = "Футболки",
                Image= "http://tailtail.net/otus/brockenbyte/tshirt.jpg"
            },
            new Category()
            {
                Id = 5,
                Name = "Брюки",
                Image= "http://tailtail.net/otus/brockenbyte/trousers.jpg"
            },
            new Category()
            {
                Id =  6,
                Name = "Толстовки",
                Image= "jumper.jpg"
            }
        };

        private static List<Product> CreateFakeCoats()
        {
            Random rnd = new();
            List<Product> list = [];
            for (int i = 0; i < 100; i++)
            {
                int categoryId = rnd.Next(1, Categories.Count);
                list.Add(new Product()
                {
                    Id = i + 1,
                    Name = $"Товар #{i} из категории {Categories[categoryId].Name} ",
                    Article = Guid.NewGuid().ToString(),
                    Deleted = false,
                    Quantity = rnd.Next(1, 100),
                    Reserved = 0,
                    Size = (i == 0) ? "48-50" : "52-54"
                });
            }
            return list;
        }
        public static List<Product> Products => CreateFakeCoats();



    }
}
