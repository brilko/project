﻿namespace BrokenByte.OnlineStore.Logistics.WebHost
{
    public class MongoConfigurationOptions
    {
        public required string ConnectionStrings { get; set; }
        public required string DatabaseName { get; set; }

    }
}
