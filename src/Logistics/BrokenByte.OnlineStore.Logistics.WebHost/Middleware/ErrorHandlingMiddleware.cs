﻿using Microsoft.AspNetCore.Http.Extensions;

using System.Net;
using System.Text.Json;

namespace BrokenByte.OnlineStore.WebApi.Middleware
{
    /// <summary>
    /// Error handling middleware
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="next"></param>
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="webHostEnvironment"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, ILogger<ErrorHandlingMiddleware> logger, IWebHostEnvironment webHostEnvironment)
        {
            try
            {
                context.Request.EnableBuffering();
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex, logger, webHostEnvironment);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex, ILogger<ErrorHandlingMiddleware> logger, IWebHostEnvironment webHostEnvironment)
        {
            var result = JsonSerializer.Serialize(new { error = ex.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            logger.LogError("Request {@RequestValue} Invoke error {@result}", context.Request.GetEncodedUrl(), ex.Message);
            return context.Response.WriteAsync(result);
        }
    }
}
