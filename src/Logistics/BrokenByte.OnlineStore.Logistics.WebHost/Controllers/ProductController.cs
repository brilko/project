﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Core.Repositories;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.WebHost.Models;
using BrokenByte.OnlineStore.Logistics.WebHost.Models.Products;
using Microsoft.AspNetCore.Mvc;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IRepository<Product> _repository;
        private readonly INotifyProductGateway _notify;

        private readonly IMapper _mapper;
#pragma warning disable IDE0052 // Удалить непрочитанные закрытые члены
        private readonly ILogger<ProductController> _logger;
#pragma warning restore IDE0052 // Удалить непрочитанные закрытые членыd

        /// <summary>
        /// конструктор
        /// </summary>
        public ProductController(IRepository<Product> repository,
            INotifyProductGateway notify,
            ILogger<ProductController> logger,
            IMapper mapper)
        {
            _repository = repository;
            _notify = notify;
            _logger = logger;
            _mapper = mapper;
        }
        /// <summary>
        /// Получить все данные
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var res = await _repository.GetAllAsync();
            if (res == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<ICollection<Product>, ICollection<ProductResponse>>(res.ToList()));
        }

        /// <summary>
        /// Получить данные товара по id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var entity = await _repository.GetByIdAsync(id);
            if (entity == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<Product, ProductResponse>(entity));
        }

        /// Создать новый продукт
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(ProductCreate newValue)
        {
            Product item = _mapper.Map<Product>(newValue);
            item.Id = newValue.id;
            await _repository.AddAsync(item);
            return Ok(item.Id);
        }


        /// <summary>
        ///  Обновить данные на единицу товара с id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, ProductUpdate editedValue)
        {
            var entity = await _repository.GetByIdAsync(id);
            if (entity == null)
            {
                return BadRequest("Не найден товар");
            }

            bool doNotify = ((editedValue.Quantity != null) && (entity.Quantity != (int)editedValue.Quantity));

            if (editedValue.Article != null) entity.Article = editedValue.Article;
            if (editedValue.Delivered != null) entity.Delivered = (int)editedValue.Delivered;
            if (editedValue.Reserved != null) entity.Reserved = (int)editedValue.Reserved;
            if (editedValue.Name != null) entity.Name = editedValue.Name;
            if (editedValue.Quantity != null) entity.Quantity = (int)editedValue.Quantity;
            if (editedValue.Size != null) entity.Size = editedValue.Size;
            await _repository.UpdateAsync(entity);

            if (doNotify) await _notify.SendNotificationAboutProductQuantityChange(id, entity.Quantity);
            return Ok();
        }


        /// <summary>
        /// Получить постраничный список
        /// </summary>       
        [HttpPost("list")]
        public async Task<IActionResult> GetList(QueryByPage byPage)
        {
            var data = await _repository.GetByPage(byPage);
            if (data.Items != null)
            {
                ICollection<Product> dataItems = data.Items.ToList();
                ICollection<ProductResponse> dtoItems = _mapper.Map<ICollection<Product>, ICollection<ProductResponse>>(dataItems);
                var response = new ItemsByPageResponse<ProductResponse>()
                {
                    Items = dtoItems.ToList(),
                    TotalPages = data.TotalPages
                };

                return Ok(response);
            }
            else
            {
                var response = new ItemsByPageResponse<ProductResponse>()
                {
                    Items = [],
                    TotalPages = 0
                };
                return Ok(response);
            }
        }
    }
}
