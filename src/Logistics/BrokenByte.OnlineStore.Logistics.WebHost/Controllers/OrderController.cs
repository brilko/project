﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.WebHost.Models;
using BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders;
using Microsoft.AspNetCore.Mvc;
using static MongoDB.Driver.WriteConcern;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Controllers
{
	[ApiController]
	[Route("api/v1/[controller]")]
	public class OrderController : ControllerBase
	{
		private readonly IRepository<Order> _repository;
		private readonly IOrderStatusService _service;
		private readonly IRepository<Product> _productRepository;

		private readonly IMapper _mapper;
#pragma warning disable IDE0052 // Удалить непрочитанные закрытые члены
		private readonly ILogger<OrderController> _logger;
#pragma warning restore IDE0052 // Удалить непрочитанные закрытые членыd

		/// <summary>
		/// конструктор
		/// </summary>
		public OrderController(IRepository<Order> repository,
			IOrderStatusService service,
			IRepository<Product> productRepository,
			ILogger<OrderController> logger,
			IMapper mapper)
		{
			_repository = repository;
			_service = service;
			_productRepository = productRepository;
			_logger = logger;
			_mapper = mapper;
		}
		/// <summary>
		/// Получить все данные
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			var res = await _repository.GetAllAsync();
			if (res == null)
			{
				return NotFound();
			}
			return Ok(_mapper.Map<ICollection<Order>, ICollection<OrderResponse>>(res.ToList()));
		}

		/// <summary>
		/// Получить постраничный список
		/// </summary>       
		[HttpPost("list")]
		public async Task<IActionResult> GetList(QueryOrderByPage byPage)
		{
			if (byPage == null) throw new ArgumentNullException(nameof(byPage));

			var data = await _repository.GetByPage(byPage.ByPage, o => o.Status.ToString() == byPage.Status.ToString());
			if (data.Items != null)
			{
				ICollection<Order> dataItems = data.Items.ToList();
				ICollection<OrderResponse> dtoItems = _mapper.Map<ICollection<Order>, ICollection<OrderResponse>>(dataItems);
				var response = new ItemsByPageResponse<OrderResponse>()
				{
					Items = dtoItems.ToList(),
					TotalPages = data.TotalPages
				};
				return Ok(response);
			}
			else
			{
				var response = new ItemsByPageResponse<OrderResponse>()
				{
					Items = [],
					TotalPages = 0
				};
				return Ok(response);
			}
		}


		/// <summary>
		/// Получить данные  по id
		/// </summary>
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(long id)
		{
			var entity = await _repository.GetByIdAsync(id);
			if (entity == null)
			{
				return NotFound();
			}
			return Ok(_mapper.Map<Order, OrderResponse>(entity));
		}

		/// <summary>
		///  Отмена заказа с id
		/// </summary>
		[HttpPut("{id}")]
		public async Task<IActionResult> Update(long id, OrderUpdateStatusRequest status)
		{
			var entity = await _repository.GetByIdAsync(id);
			if (entity.Status != OrderStatus.Canceled)
			{
				await _service.EditStatus(id, status.Status, null);
			}
			return Ok();
		}

		/// <summary>
		///  Отмена заказа с id
		/// </summary>
		[HttpPut("{id}Cancel")]
		public async Task<IActionResult> Cancel(long id)
		{
			var entity = await _repository.GetByIdAsync(id);
			if (entity.Status != OrderStatus.Canceled)
			{
				await _service.EditStatus(id, OrderStatus.Canceled, null);
			}
			return Ok();
		}

		/// <summary>
		///  Начать процедуру доставки заказа с id
		/// </summary>
		[HttpPut("{id}StartDelivery")]
		public async Task<IActionResult> StartDelivery(long id)
		{
			var entity = await _repository.GetByIdAsync(id);
			if (entity.Status != OrderStatus.Canceled)
			{
				await _service.EditStatus(id, OrderStatus.AssemblyStart, null);
			}
			return Ok();
		}

		private CreatingOrderDto OrderCreateRequestMapToCreatingOrderDto(OrderCreateRequest newValue)
		{
			CreatingOrderDto order = new()
			{
				Id = newValue.Id,
				CustomerId = newValue.CustomerId,
				CustomerName = newValue.CustomerName,
				DeliveryAddress = new DeliveryAddressDto()
				{
					Address = newValue.DeliveryAddress.Address
				},
				DeliveryServiceId = newValue.DeliveryServiceId,
				Items = new List<OrderItemDto>(),
				NumberInvoice = newValue?.NumberInvoice,
				Recipient = new RecipientDto()
				{
					FullName = newValue.Recipient.FullName
				},
				Status = Core.Domain.Entities.OrderStatus.Created,
				TotalPrice = (int)newValue.TotalPrice
			};

			foreach (var item in newValue.Items)
			{
				order.Items.Add(new OrderItemDto()
				{
					ProductId = item.ProductId,
					Quantity = item.Quantity,
					Deleted = false
				});
			}
			return order;
		}

		/// Создать новый заказ
		/// </summary>
		[HttpPost]
		public async Task<IActionResult> Create(OrderCreateRequest newValue)
		{
			if (newValue == null)
			{
				throw new ArgumentNullException(nameof(newValue));
			}

			var order = OrderCreateRequestMapToCreatingOrderDto(newValue);
			var result = await _service.CreateOrder(order);
			if (result != null)
			{
				OrderCreateResponse response = MapOrderCreateResponseFromDto(result);
				return Ok(response);
			}
			else
			{
				throw new AccessViolationException("Неправильно отрабатывает сервис IOrderStatusService");
			}
		}


		private static OrderCreateResponse MapOrderCreateResponseFromDto(CreateOrderResponseDto result)
		{
			OrderCreateResponse response = new OrderCreateResponse()
			{
				Id = result.Id,
				Created = result.Created,
				Items = null
			};

			if (result.Created)
			{
				return response;
			}

			if (result.Items != null)
			{
				response.Items = [];
				foreach (var item in result.Items)
				{
					response.Items.Add(new OrderItemCreateResponse()
					{
						ProductId = item.ProductId,
						AvailableQuantity = item.AvailableQuantity
					});

				}
			}

			return response;
		}
	}
}
