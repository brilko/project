﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;
using BrokenByte.OnlineStore.Logistics.WebHost.Models.Products;

namespace BrokenByte.OnlineStore.Logistics.WebHost
{
	public class MapperConfigurationSet
	{
		public static MapperConfiguration GetMapperConfiguration()
		{
			var configuration = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile<ProductMappingsProfile>();
				cfg.AddProfile<ProductServicesMappingsProfile>();
				cfg.AddProfile<OrderMappingsProfile>();
			});
			configuration.AssertConfigurationIsValid();
			return configuration;
		}
	}
}
