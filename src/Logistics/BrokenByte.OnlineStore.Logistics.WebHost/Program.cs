using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.DataAccess;
using BrokenByte.OnlineStore.Logistics.DataAccess.Repositories;
using BrokenByte.OnlineStore.Logistics.Integration.Implementations;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.Services.Implementations;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;
using BrokenByte.OnlineStore.Logistics.WebHost;
using BrokenByte.OnlineStore.Logistics.WebHost.Models.Products;
using MongoDB.Driver;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

var mongoDbSettings = builder.Configuration.GetSection("MongoDbSettings").Get<MongoConfigurationOptions>();
if (mongoDbSettings == null) throw new ArgumentNullException(nameof(mongoDbSettings));


builder.Services.AddSingleton<IMongoClient>(_ => new MongoClient(mongoDbSettings.ConnectionStrings))
.AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoClient>().GetDatabase(mongoDbSettings.DatabaseName))
.AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>().GetCollection<Product>(nameof(Product).ToLower()))
.AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>().GetCollection<Order>(nameof(Order).ToLower()))
.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IMongoClient>().StartSession());


builder.Services.AddScoped<IProductQuantityManipulateService, ProductQuantityManipulateService>();
builder.Services.AddScoped<IOrderStatusService, OrderStatusService>();
builder.Services.AddScoped<INotifyProductGateway, NotifyProductGateway>();
builder.Services.AddScoped<INotifyOrderGateway, NotifyOrderGateway>();



builder.Services.AddScoped<IRepository<Product>, MongoDbRepository<Product>>();
builder.Services.AddScoped<IRepository<Order>, MongoDbRepository<Order>>();
builder.Services.AddScoped<IDbInitializer, MongoDbInitializer>();

builder.Services.AddSingleton<IMapper>(new Mapper(MapperConfigurationSet.GetMapperConfiguration()));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseMiddleware(typeof(BrokenByte.OnlineStore.WebApi.Middleware.ErrorHandlingMiddleware));

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using (var scope = app.Services.CreateScope())
{
    var initial = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
    Configure(initial);
}

app.Run();


void Configure(IDbInitializer dbInitializer)
{

    dbInitializer.InitializeDb();
}

