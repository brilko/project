﻿namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
	public class OrderCreateResponse
	{
		public required long Id { get; set; }
		public bool Created { get; set; } = false;

		public List<OrderItemCreateResponse>? Items { get; set; } = [];
	}
}
