﻿namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class DeliveryAddressResponse
    {
        public string Address { get; set; } = string.Empty;
    }
}
