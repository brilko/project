﻿namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class OrderItemRequest
    {
     

        /// <summary>
        /// Количество в заказе
        /// </summary>
        public int Quantity { get; set; }


        public long ProductId { get; set; }


    }
}
