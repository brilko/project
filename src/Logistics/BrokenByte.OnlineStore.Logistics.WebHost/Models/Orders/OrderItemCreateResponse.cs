﻿namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
	public class OrderItemCreateResponse
	{
		
		public int AvailableQuantity { get; set; }

		public long ProductId { get; set; }
	}
}
