﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class OrderResponse
    {
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор службы доставки
        /// </summary>
        public long DeliveryServiceId { get; set; }

        /// <summary>
        /// Номер накладной
        /// </summary>
        public string? NumberInvoice { get; set; }

        /// <summary>
        /// Полная стоимость за все продукты (c учетом промокода)
        /// </summary>
        public int TotalPrice { get; set; }

        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public long CustomerId { get; set; }
        public string CustomerName { get; set; } = string.Empty;

        /// <summary>
        ///  статуса заказа
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        ///  адреса доставки
        /// </summary>
        public DeliveryAddressResponse DeliveryAddress { get; set; } = new();

        public RecipientResponse Recipient { get; set; } = new();

        public List<OrderItemResponse> Items { get; set; } = [];
        public List<OrderHistoryItemResponse> History { get; set; } = [];


    }
}
