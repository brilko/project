﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using BrokenByte.OnlineStore.Logistics.Core.Repositories;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class QueryOrderByPage
    {
        public required OrderStatus Status { get; set; }
        public required QueryByPage ByPage { get; set; }
    }
}
