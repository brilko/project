﻿

using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class OrderUpdateStatusRequest
    {
        public OrderStatus Status { get; set; }

    }
}
