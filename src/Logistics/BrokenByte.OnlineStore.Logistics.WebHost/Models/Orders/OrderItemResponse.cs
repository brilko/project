﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class OrderItemResponse
    {

        /// <summary>
        /// Количество в заказе
        /// </summary>
        public int Quantity { get; set; }


        public long ProductId { get; set; }

        [MaxLength(150)]
        public string ProductName { get; set; } = string.Empty;

        /// <summary>
        /// Артикул
        /// </summary>
        [MaxLength(50)]
        public string ProductArticle { get; set; } = string.Empty;

        public string ProductSize { get; set; } = string.Empty;
    }
}
