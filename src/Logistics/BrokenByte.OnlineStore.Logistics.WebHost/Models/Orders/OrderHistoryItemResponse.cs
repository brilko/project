﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class OrderHistoryItemResponse
    {
        public OrderStatus Status { get; set; }
        public DateTime Date { get; set; }
    }
}
