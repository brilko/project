﻿namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class RecipientResponse
    {
        public string FullName { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;
    }
}
