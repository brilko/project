﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Domain;


namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class OrderMappingsProfile : Profile
    {
        public OrderMappingsProfile()
        {
            CreateMap<Order, OrderItemResponse>()
                .ForMember(d => d.ProductArticle, map => map.Ignore())
                .ForMember(d => d.ProductSize, map => map.Ignore())
                .ForMember(d => d.ProductName, map => map.Ignore());

            CreateMap<OrderItemResponse, Order>()
                .ForMember(d => d.Deleted, map => map.Ignore());

            CreateMap<OrderResponse, Order>()
                .ForMember(d => d.Deleted, map => map.Ignore());

        }
    }
}
