﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders
{
    public class OrderCreateRequest
    {
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор службы доставки
        /// </summary>
        public long DeliveryServiceId { get; set; }

        /// <summary>
        /// Номер накладной
        /// </summary>
        public string? NumberInvoice { get; set; }

        /// <summary>
        /// Полная стоимость за все продукты (c учетом промокода)
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public long CustomerId { get; set; }
        public string CustomerName { get; set; } = string.Empty;

        /// <summary>
        ///  статуса заказа
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        ///  адреса доставки
        /// </summary>
        public DeliveryAddressResponse DeliveryAddress { get; set; } = new();

        public RecipientResponse Recipient { get; set; } = new();

        public List<OrderItemRequest> Items { get; set; } = [];

    }
}
