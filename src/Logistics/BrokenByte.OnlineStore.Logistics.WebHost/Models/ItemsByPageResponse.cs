﻿namespace BrokenByte.OnlineStore.Logistics.WebHost.Models
{
    public class ItemsByPageResponse<T>
    {
        /// <summary>
        /// Количество страниц
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Сущности с конкретной страницы
        /// </summary>
        public List<T>? Items { get; set; }
    }
}
