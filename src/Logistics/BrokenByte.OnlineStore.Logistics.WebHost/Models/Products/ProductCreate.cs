﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Products
{
    public class ProductCreate
    {
        public required long id { get; set; }

        [MaxLength(50)]
        public string Article { get; set; } = string.Empty;

        public string Size { get; set; } = string.Empty;

        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(150)]
        public string Name { get; set; } = string.Empty;



    }
}
