﻿namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Products
{
    public class ProductQuantityUpdate
    {
        /// <summary>
        /// Фактическое количество товаров на складе
        /// </summary>
        public int Quantity { get; set; }
    }
}
