﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Products
{
    public class ProductMappingsProfile : Profile
    {
        public ProductMappingsProfile()
        {
            CreateMap<ProductResponse, ProductDto>()
                .ForMember(d => d.Deleted, map => map.Ignore()).ReverseMap(); ;

            CreateMap<ProductResponse, Product>()
                .ForMember(d => d.Deleted, map => map.Ignore()).ReverseMap(); ;


            CreateMap<ProductCreate, Product>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Reserved, map => map.Ignore())
                .ForMember(d => d.Delivered, map => map.Ignore())
                .ForMember(d => d.Quantity, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore()).ReverseMap(); ;

            CreateMap<ProductUpdate, ProductDto>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore()).ReverseMap(); ;

        }
    }
}
