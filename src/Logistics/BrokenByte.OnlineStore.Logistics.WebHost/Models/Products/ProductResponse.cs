﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.WebHost.Models.Products
{
    public class ProductResponse
    {
        public long Id { get; set; }


        [MaxLength(150)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Артикул
        /// </summary>
        [MaxLength(50)]
        public string Article { get; set; } = string.Empty;

        public string Size { get; set; } = string.Empty;

        /// <summary>
        /// Фактическое количество товаров на складе
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Количество товара забронированное под заказы покупателей
        /// </summary>
        public int Reserved { get; set; }

        /// <summary>
        /// Количество доставляемых товаров
        /// </summary>
        public int Delivered { get; set; }



    }
}
