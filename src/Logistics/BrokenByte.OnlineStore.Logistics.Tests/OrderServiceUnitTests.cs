﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using BrokenByte.OnlineStore.Logistics.DataAccess;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.Services.Implementations;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;
using BrokenByte.OnlineStore.Logistics.WebHost;
using BrokenByte.OnlineStore.Logistics.WebHost.Controllers;
using BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders;
using Castle.Core.Resource;
using FluentAssertions;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Misc;
using Moq;
using Shouldly;
using Xunit;
using YamlDotNet.Core;
using YamlDotNet.Core.Tokens;

namespace BrokenByte.OnlineStore.Logistics.Tests
{
	public class TestNotifyOrderGateway : INotifyOrderGateway
	{
		public Task SendNotificationAboutOrderStatusChanged(long orderId, OrderStatus status, string? deteilInfo)
		{
			return Task.CompletedTask;
		}
	}


	public class TestNotifyProductGateway : INotifyProductGateway
	{
		public Task SendNotificationAboutProductQuantityChange(long productId, int quantity)
		{
			return Task.CompletedTask;
		}
	}
	public class OrderServiceUnitTests
	{

		private readonly Mock<IRepository<Product>> _productRepository;
		private readonly Mock<IRepository<Order>> _orderRepository;
		private readonly IProductQuantityManipulateService _productService;
		private readonly IOrderStatusService _orderService;

		private static readonly long _productId1 = 1;
		private static readonly long _orderId = 3;

		public OrderServiceUnitTests()
		{
			IMapper mapper = new Mapper(Configuration.GetMapperConfiguration());
			_productRepository = new();
			_orderRepository = new();
			_productService = new ProductQuantityManipulateService(mapper, _productRepository.Object, new TestNotifyProductGateway());
			_orderService = new OrderStatusService(mapper, _orderRepository.Object, new TestNotifyOrderGateway(), _productService);
		}

		private Product CreateProductInRepository(long id, int quantity = 200, int reserved = 100, int delivered = 10)
		{
			Product product = new ProductBuilder(id)
				.WithQuantity(quantity)
				.WithReservedQuantity(reserved)
				.WithDeliveredQuantity(delivered)
				.Build();
			_productRepository.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(product);
			return product;
		}
		private Order SetupOrderInRepository(long id)
		{
			Order order = new Order()
			{
				Id = id
			};
			_orderRepository.Setup(m => m.GetByIdAsync(order.Id)).ReturnsAsync(order);
			return order;
		}

		[Fact]
		public async void CreateOrder_WithValidOrder_OrderCreatedWithReservation()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = 10;
			CreatingOrderDto value = CreateOrderDto();
			value.Items.Add(new OrderItemDto()
			{
				ProductId = _productId1,
				Quantity = quantityInOrder
			});

			// Act
			var result = await _orderService.CreateOrder(value);

			// Assert
			product1.Quantity.Should().Be(quantity);
			product1.Reserved.Should().Be(reserved + quantityInOrder);
			product1.Delivered.Should().Be(delivered);

			result.Should().NotBeNull();
			result.Created.ShouldBe(true);

		}

		[Fact]
		public void CreateOrder_WithInvalidProductId_ThrowException()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = 10;
			CreatingOrderDto value = CreateOrderDto();
			value.Id = _orderId;
			value.Items.Add(new OrderItemDto()
			{
				ProductId = 100,
				Quantity = quantityInOrder
			});

			// Act				
			Should.Throw<ArgumentException>(async () => await _orderService.CreateOrder(value))
				.Message.ShouldBeEquivalentTo($"Продукт с идентификатором {100} не найден");

		}

		[Fact]
		public async void CreateOrder_WithValidOrderAndNotAvailableProductQuantity_ReturnResultNotCreated()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = 1000;
			CreatingOrderDto value = CreateOrderDto();
			value.Id = _orderId;
			value.Items.Add(new OrderItemDto()
			{
				ProductId = _productId1,
				Quantity = quantityInOrder
			});

			// Act				
			var result = await _orderService.CreateOrder(value);
			result.Should().NotBeNull();
			result.Created.ShouldBe(false);
			result.Items.Should().NotBeNull();

			CreateOrderItemResponseDto? item = result.Items?.First();
			item.Should().NotBeNull();
			if (item!=null)
			{
				item.AvailableQuantity.Should().Be(quantity- reserved- delivered);
				item.ProductId.Should().Be(_productId1);
			}

		}

		[Fact]
		public void CreateOrder_WithValidOrderAndProductQuantityLess0_ThrowException()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = -10;
			CreatingOrderDto value = CreateOrderDto();
			value.Id = _orderId;
			value.Items.Add(new OrderItemDto()
			{
				ProductId = _productId1,
				Quantity = quantityInOrder
			});

			// Act				
			Should.Throw<ArgumentException>(async () => await _orderService.CreateOrder(value))
				.Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.QuantityLessNullExceptionMessage); ;

		}


		private Order CrateOrderWithStatus(OrderStatus status)
		{
			Order order = new Order()
			{
				Id = _orderId,
				Status = status,
				CustomerId = 1,
				CustomerName = "TestCustomerName",
				DeliveryAddress = new()
				{
					Address = "AddressAddress"
				},
				DeliveryServiceId = 1,
				Items =
							[
								new()
								{
									Id = 1,
									ProductId = _productId1,
									Quantity = 1
								}
							],
				NumberInvoice = "numb111",
				Recipient = new()
				{
					FullName = "FullNameTest",
					Phone = "TestPhone"
				},
				TotalPrice = 10000
			};
			_orderRepository.Setup(m => m.GetByIdAsync(order.Id)).ReturnsAsync(order);
			return order;
		}

		private static CreatingOrderDto CreateOrderDto()
		{
			return new CreatingOrderDto()
			{
				CustomerId = 1,
				CustomerName = "TestCustomerName",
				DeliveryAddress = new DeliveryAddressDto()
				{
					Address = "AddressAddress"
				},
				DeliveryServiceId = 1,
				Id = _orderId,
				Items = new List<OrderItemDto>(),
				NumberInvoice = "numb111",
				Recipient = new()
				{
					FullName = "FullNameTest",
					Phone = "TestPhone"
				},
				Status = Core.Domain.Entities.OrderStatus.DeliveryInProcess,
				TotalPrice = 10000
			};
		}
		[Theory]
		[InlineData(OrderStatus.Created)]
		[InlineData(OrderStatus.Canceled)]
		[InlineData(OrderStatus.Delivered)]
		[InlineData(OrderStatus.DeliveryInProcess)]
		[InlineData(OrderStatus.AssemblyEndAndReadyForDelivery)]
		[InlineData(OrderStatus.AssemblyStart)]
		[InlineData(OrderStatus.TransferredToDeliveryService)]
		[InlineData(OrderStatus.Returned)]

		public void EditStatus_FromAnyToCreateState_ThrowException(OrderStatus status)
		{
			// Arrange
			Product product1 = CreateProductInRepository(_productId1);
			CrateOrderWithStatus(status);

			// Act, Assert
			Should.Throw<ArgumentException>(async () => await _orderService.EditStatus(_orderId, OrderStatus.Created, "!!!!!!!!"));
		}

		
	}
}
