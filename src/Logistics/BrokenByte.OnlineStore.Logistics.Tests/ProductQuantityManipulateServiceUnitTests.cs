﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.DataAccess.Repositories;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Products;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.Services.Implementations;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;
using FluentAssertions;
using FluentAssertions.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using YamlDotNet.Core.Tokens;

namespace BrokenByte.OnlineStore.Logistics.Tests
{
	public class ProductQuantityManipulateServiceUnitTests
	{

		private readonly Mock<IRepository<Product>> _productRepository;
		private readonly IProductQuantityManipulateService _productService;

		private static readonly long _productId1 = 1;
		private static readonly long _productId2 = 2;

		public ProductQuantityManipulateServiceUnitTests()
		{
			IMapper mapper = new Mapper(Configuration.GetMapperConfiguration());
			_productRepository = new();
			_productService = new ProductQuantityManipulateService(mapper, _productRepository.Object, new TestNotifyProductGateway());
		}
		private static ProductQuantityDto CreateProductQuantityDto(int quantityValue)
		{
			return new ProductQuantityDto()
			{
				AddQuantity = quantityValue,
				Id = _productId1
			};
		}

		private Product CreateProductInRepository(long id, int quantity = 200, int reserved = 100, int delivered = 10)
		{
			Product product = new ProductBuilder(id)
				.WithQuantity(quantity)
				.WithReservedQuantity(reserved)
				.WithDeliveredQuantity(delivered)
				.Build();
			_productRepository.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(product);
			return product;
		}

		

		[Theory]
		[InlineData(100, 0, 0, -20)]
		[InlineData(100, 0, 0, 0)]
		[InlineData(2000, 100, 10, 120)]
		[InlineData(1000, 150, 250, 500)]
		[InlineData(1000, 150, 250, 5000)]
		public async void SetQuantity_WithValueMoreThenReservedPlusDelivered_QuantityIsEqualValue(int quantity, int reserved, int delivered, int quantityDto)
		{
			// Arrange			
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.SetQuantity(addValues);

			// Assert
			product1.Quantity.ShouldBe(quantityDto);
			product2.Quantity.ShouldBe(quantity);
		}

		[Fact]
		public void SetQuantity_WithInvalidProductId_ThrowException()
		{
			// Arrange
			CreateProductInRepository(_productId1);
			ProductQuantityDto addValues = new ProductQuantityDto()
			{
				Id = 10,
				AddQuantity = 10
			};
			//Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.SetQuantity(addValues)).Message.ShouldBeEquivalentTo($"Продукт с идентификатором {addValues.Id} не найден");
		}


		[Theory]
		[InlineData(200, 100, 0, 10)]
		[InlineData(200, 0, 100, 10)]
		[InlineData(200, 15, 5, 10)]
		[InlineData(200, 15, 25, 25)]
		public void SetQuantity_WithValueLessThanReservedPlusDelivered_ThrowException(int quantity, int reserved, int delivered, int quantityDto)
		{
			// Arrange
			CreateProductInRepository(_productId1, quantity, reserved, delivered);
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			// Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.SetQuantity(addValues))
				.Message.ShouldContain(ProductQuantityManipulateService.ExceptionMessageSetCountQuantity);

		}



		[Theory]
		[InlineData(200, 100)]
		[InlineData(200, 1)]
		[InlineData(200, 15)]
		[InlineData(200, 150)]
		public async void AddQuantity_WithValueMoreThen0_QuantityEqualQuantityPlusValue(int quantity, int quantityDto)
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, quantity);
			Product product2 = CreateProductInRepository(_productId2, quantity);

			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.AddQuantity(addValues);

			//Arrange
			product1.Quantity.ShouldBe(quantity + quantityDto);
			product2.Quantity.ShouldBe(quantity);
		}

		[Fact]
		public void AddQuantity_WithInvalidProductId_ThrowException()
		{
			// Arrange
			CreateProductInRepository(_productId1);
			ProductQuantityDto addValues = new ProductQuantityDto()
			{
				Id = 10,
				AddQuantity = 10
			};
			//Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.AddQuantity(addValues)).Message.ShouldBeEquivalentTo($"Продукт с идентификатором {addValues.Id} не найден");
		}

		[Theory]
		[InlineData(200, 100, 0, -100)]
		[InlineData(200, 0, 100, -100)]
		[InlineData(200, 15, 5, -150)]
		[InlineData(200, 15, 25, -25)]
		public async void AddQuantity_WithValueLessThen0AndResQuantityMoreThanReservedPlusDelivered_QuantityEqualQuantityPlusValue(int quantity, int reserved, int delivered, int quantityDto)
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);

			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.AddQuantity(addValues);

			//Arrange
			product1.Quantity.ShouldBe(quantity + quantityDto);
			product2.Quantity.ShouldBe(quantity);
		}


		[Theory]
		[InlineData(200, 100, 0, -110)]
		[InlineData(200, 0, 100, -110)]
		[InlineData(200, 15, 5, -200)]
		[InlineData(200, 15, 25, -200)]
		public void AddQuantity_WithValueMoreLess0ButLessThanReservedPlusDelivered_ThrowException(int quantity, int reserved, int delivered, int quantityDto)
		{
			// Arrange
			CreateProductInRepository(_productId1, quantity, reserved, delivered);
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			// Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.AddQuantity(addValues))
				.Message.ShouldContain(ProductQuantityManipulateService.AddQuantityLessReservedExceptionMessage);

		}

		[Theory]
		[InlineData(100, 50, 10, 20)]
		[InlineData(500, 150, 30, 50)]
		public async void ReservedToDelivery_ValueMoreThen0AndLessThenReserved_SubReservedAndAddDelivered(int quantity, int reserved, int delivered, int quantityDto)
		{
			//Assert
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);


			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.ReservedToDelivery(new[] { addValues });

			product1.Quantity.ShouldBe(quantity);
			product1.Reserved.ShouldBe(reserved - quantityDto);
			product1.Delivered.ShouldBe(delivered + quantityDto);

			product2.Quantity.ShouldBe(quantity);
			product2.Reserved.ShouldBe(reserved);
			product2.Delivered.ShouldBe(delivered);
		}
		[Fact]
		public void ReservedToDelivery_WithInvalidProductId_ThrowException()
		{
			// Arrange
			CreateProductInRepository(_productId1);
			ProductQuantityDto addValues = new ProductQuantityDto()
			{
				Id = 10,
				AddQuantity = 10
			};
			//Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.ReservedToDelivery(new[] { addValues }))
				.Message.ShouldBeEquivalentTo($"Продукт с идентификатором {addValues.Id} не найден");
		}

		[Theory]
		[InlineData(200, 100, 0, 150)]
		[InlineData(200, 200, 100, 300)]
		public void ReservedToDelivery_WithValueLessThenReserved_ThrowException(int quantity, int reserved, int delivered, int quantityDto)
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.ReservedToDelivery(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.ToDeliveredQuantityMoreThenReservedExceptionMessage);

		}

		[Fact]
		public void ReservedToDelivery_WithValueLess0_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(-20);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.ReservedToDelivery(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.QuantityLessNullExceptionMessage);
		}


		[Theory]
		[InlineData(200, 100, 0, 10)]
		[InlineData(200, 20, 100, 20)]
		public async void AddToReserved_Valid_ValueMoreThen0AndLessQuantity(int quantity, int reserved, int delivered, int quantityDto)
		{
			//Assert			

			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.AddToReserved(new[] { addValues });

			product1.Quantity.ShouldBe(quantity);
			product1.Reserved.ShouldBe(reserved + quantityDto);
			product1.Delivered.ShouldBe(delivered);

			product2.Quantity.ShouldBe(quantity);
			product2.Reserved.ShouldBe(reserved);
			product2.Delivered.ShouldBe(delivered);
		}


		[Fact]
		public void AddToReserved_WithInvalidProductId_ThrowException()
		{
			// Arrange
			CreateProductInRepository(_productId1);
			ProductQuantityDto addValues = new ProductQuantityDto()
			{
				Id = 10,
				AddQuantity = 10
			};
			//Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.AddToReserved(new[] { addValues }))
				.Message.ShouldBeEquivalentTo($"Продукт с идентификатором {addValues.Id} не найден");
		}


		[Fact]
		public void AddToReserved_WithValueLess0_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(-20);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.AddToReserved(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.QuantityLessNullExceptionMessage);
		}

		[Theory]
		[InlineData(200, 0, 0, 500)]
		[InlineData(200, 200, 100, 100)]
		[InlineData(200, 0, 100, 200)]
		public async void AddToReserved_WithValueLessThenAvailable_ReturAnvailableQuantity(int quantity, int reserved, int delivered, int quantityDto)
		{
			//Assert		
			CreateProductInRepository(_productId1, quantity, reserved, delivered);
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			var result = await _productService.AddToReserved(new[] { addValues });
			result.ShouldNotBeNull();
			var resultList = result.ToList();

			//Arrange
			resultList.Count.ShouldBe(1);
			ProductAvailableQuantityDto resultItem = resultList[0];
			resultItem.ShouldNotBeNull();
			resultItem.AvailableQuantity.ShouldBe(quantity- reserved- delivered);
		}


		[Fact]
		public async void ApplyDelivered_WithValueMoreThen0AndLessThenDelivered_SubDeliveredSubQuantity()
		{
			//Assert
			int quantity = 200;
			int reserved = 100;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);

			int quantityDto = 10;
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.ApplyDelivered(new[] { addValues });

			product1.Quantity.ShouldBe(quantity - quantityDto);
			product1.Reserved.ShouldBe(reserved);
			product1.Delivered.ShouldBe(delivered - quantityDto);

			product2.Quantity.ShouldBe(quantity);
			product2.Reserved.ShouldBe(reserved);
			product2.Delivered.ShouldBe(delivered);
		}

		[Fact]
		public void ApplyDelivered_WithInvalidProductId_ThrowException()
		{
			// Arrange
			CreateProductInRepository(_productId1);
			ProductQuantityDto addValues = new ProductQuantityDto()
			{
				Id = 10,
				AddQuantity = 10
			};
			//Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.ApplyDelivered(new[] { addValues }))
				.Message.ShouldBeEquivalentTo($"Продукт с идентификатором {addValues.Id} не найден");
		}

		[Fact]
		public void ApplyDelivered_WithValueLessThen0_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(-20);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.ApplyDelivered(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.QuantityLessNullExceptionMessage);
		}

		[Fact]
		public void ApplyDelivered_WithValueLessThenQuantity_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, 200);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(220);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.ApplyDelivered(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.DeliveredMoreThenQuantityExceptionMessage);
		}

		[Fact]
		public void ApplyDelivered_WithValueLessThenDelivered_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, 200, 5, 50);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(100);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.ApplyDelivered(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.DeliveredApplyMoreThenDeliveredExceptionMessage);
		}

		[Fact]
		public async void CancelReserved_WithValueMoreThen0AndLessReserved_SubReserved()
		{
			//Assert
			int quantity = 200;
			int reserved = 100;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);


			int quantityDto = 10;
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.CancelReserved(new[] { addValues });

			product1.Quantity.ShouldBe(quantity);
			product1.Reserved.ShouldBe(reserved - quantityDto);
			product1.Delivered.ShouldBe(delivered);

			product2.Quantity.ShouldBe(quantity);
			product2.Reserved.ShouldBe(reserved);
			product2.Delivered.ShouldBe(delivered);
		}

		[Fact]
		public void CancelReserved_WithInvalidProductId_ThrowException()
		{
			// Arrange
			CreateProductInRepository(_productId1);
			ProductQuantityDto addValues = new ProductQuantityDto()
			{
				Id = 10,
				AddQuantity = 10
			};
			//Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.CancelReserved(new[] { addValues }))
				.Message.ShouldBeEquivalentTo($"Продукт с идентификатором {addValues.Id} не найден");
		}

		[Fact]
		public void CancelReserved_WithValueLess0_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, 200, 5, 50);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(-100);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.CancelReserved(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.QuantityLessNullExceptionMessage);
		}


		[Fact]
		public void CancelReserved_WithValueLessThenReserved_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, 200, 5, 50);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(100);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.CancelReserved(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.CancelReservedQuantityMoreThenReservedExceptionMessage);
		}



		[Fact]
		public async void CancelDelivery_WithValueMoreThen0AndLessDelivered_SubDelivered()
		{
			//Assert
			int quantity = 200;
			int reserved = 100;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Product product2 = CreateProductInRepository(_productId2, quantity, reserved, delivered);

			int quantityDto = 10;
			ProductQuantityDto addValues = CreateProductQuantityDto(quantityDto);

			//Act
			await _productService.CancelDelivery(new[] { addValues });

			product1.Quantity.ShouldBe(quantity);
			product1.Reserved.ShouldBe(reserved);
			product1.Delivered.ShouldBe(delivered - quantityDto);

			product2.Quantity.ShouldBe(quantity);
			product2.Reserved.ShouldBe(reserved);
			product2.Delivered.ShouldBe(delivered);
		}

		[Fact]
		public void CancelDelivery_WithInvalidProductId_ThrowException()
		{
			// Arrange
			CreateProductInRepository(_productId1);
			ProductQuantityDto addValues = new ProductQuantityDto()
			{
				Id = 10,
				AddQuantity = 10
			};
			//Act
			// Assert
			Should.Throw<ArgumentException>(async () => await _productService.CancelDelivery(new[] { addValues }))
				.Message.ShouldBeEquivalentTo($"Продукт с идентификатором {addValues.Id} не найден");
		}

		[Fact]
		public void CancelDelivery_WithValueLess0_ThrowException()
		{
			//Assert		
			Product product1 = CreateProductInRepository(_productId1, 200, 5, 50);
			Product product2 = CreateProductInRepository(_productId2);
			ProductQuantityDto addValues = CreateProductQuantityDto(100);

			//Act
			//Arrange
			Should.Throw<ArgumentException>(async () => await _productService.CancelDelivery(new[] { addValues })).
				Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.CancelDeliveryMoreThenDeliveredExceptionMessage);
		}
	}


}

