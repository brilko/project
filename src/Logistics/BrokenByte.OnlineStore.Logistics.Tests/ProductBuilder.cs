﻿using BrokenByte.OnlineStore.Logistics.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Core;

namespace BrokenByte.OnlineStore.Logistics.Tests
{
	internal class ProductBuilder
	{

		private long _productId = 1;
		private int _delivered = 0;
		private int _reserved = 0;
		private int _quantity = 100;

		public ProductBuilder()
		{
		}

		public ProductBuilder(long id)
		{
			WithCreatedId(id);
		}

		public ProductBuilder WithCreatedId(long id)
		{
			_productId = id;
			return this;
		}

		public ProductBuilder WithQuantity(int quantity)
		{
			_quantity = quantity;
			return this;
		}

		public ProductBuilder WithReservedQuantity(int reserved)
		{
			_reserved = reserved;
			return this;
		}

		public ProductBuilder WithDeliveredQuantity(int delivered)
		{
			_delivered = delivered;
			return this;
		}

		public Product Build()
		{
			return new Product()
			{
				Id = _productId,
				Name = $"Товар {_productId}",
				Deleted = false,
				Size = "48",
				Article = Guid.NewGuid().ToString(),
				Delivered = _delivered,
				Reserved = _reserved,
				Quantity = _quantity
			};
		}
	}
}
