﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.Services.Implementations;
using BrokenByte.OnlineStore.Logistics.WebHost.Controllers;
using BrokenByte.OnlineStore.Logistics.WebHost.Models.Orders;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrokenByte.OnlineStore.Logistics.Tests
{
	public class OrderControllerTestUnits
	{
		private readonly Mock<IRepository<Product>> _productRepository;
		private readonly Mock<IRepository<Order>> _orderRepository;
		private readonly IProductQuantityManipulateService _productService;
		private readonly IOrderStatusService _orderService;
		private readonly OrderController _orderController;

		private static readonly long _productId1 = 1;
		private static readonly long _productId2 = 2;
		private static readonly long _orderId = 3;

		public OrderControllerTestUnits()
		{
			IMapper mapper = new Mapper(Configuration.GetMapperConfiguration());
			_productRepository = new();
			_orderRepository = new();
			_productService = new ProductQuantityManipulateService(mapper, _productRepository.Object, new TestNotifyProductGateway());
			_orderService = new OrderStatusService(mapper, _orderRepository.Object, new TestNotifyOrderGateway(), _productService);
			var logger = new Mock<ILogger<OrderController>>();
			_orderController = new OrderController(_orderRepository.Object, _orderService, _productRepository.Object, logger.Object, mapper);		
		}

		private Product CreateProductInRepository(long id, int quantity = 200, int reserved = 100, int delivered = 10)
		{
			Product product = new ProductBuilder(id)
				.WithQuantity(quantity)
				.WithReservedQuantity(reserved)
				.WithDeliveredQuantity(delivered)
				.Build();
			_productRepository.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(product);
			return product;
		}
		private Order SetupOrderInRepository(long id)
		{
			Order order = new Order()
			{
				Id = id
			};
			_orderRepository.Setup(m => m.GetByIdAsync(order.Id)).ReturnsAsync(order);
			return order;
		}
		private static OrderCreateRequest CreateOrderDto()
		{
			return new OrderCreateRequest()
			{
				CustomerId = 1,
				CustomerName = "TestCustomerName",
				DeliveryAddress = new ()
				{
					Address = "AddressAddress"
				},
				DeliveryServiceId = 1,
				Id = _orderId,
				Items = new (),
				NumberInvoice = "numb111",
				Recipient = new()
				{
					FullName = "FullNameTest",
					Phone = "TestPhone"
				},
				Status = Core.Domain.Entities.OrderStatus.DeliveryInProcess,
				TotalPrice = 10000
			};
		}

		[Fact]
		public async void Create_WithValidOrder_OrderCreatedWithReservation()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = 10;
			OrderCreateRequest value = CreateOrderDto();
			value.Items.Add(new ()
			{
				ProductId = _productId1,
				Quantity = quantityInOrder
			});

			// Act
			IActionResult result = await _orderController.Create(value);

			// Assert
			product1.Quantity.Should().Be(quantity);
			product1.Reserved.Should().Be(reserved + quantityInOrder);
			product1.Delivered.Should().Be(delivered);

			result.Should().NotBeNull();
			if (result != null)
			{
				result.ShouldBeAssignableTo(typeof(Microsoft.AspNetCore.Mvc.OkObjectResult));

				OkObjectResult okResult = (OkObjectResult)result;
				okResult.Should().NotBeNull();
				okResult.Value.Should().NotBeNull();
				okResult.Value.ShouldBeAssignableTo(typeof(OrderCreateResponse));
				if (okResult != null && okResult.Value != null)
				{
					OrderCreateResponse? response = okResult.Value as OrderCreateResponse;
					response.Should().NotBeNull();
					if (response != null)
					{
						response.Created.Should().BeTrue();
						response.Items.Should().BeNull();
					}

				}
			}
		}

		[Fact]
		public void Create_WithInvalidProductId_ThrowException()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = 10;
			var value = CreateOrderDto();
			value.Id = _orderId;
			value.Items.Add(new ()
			{
				ProductId = 100,
				Quantity = quantityInOrder
			});

			// Act				
			Should.Throw<ArgumentException>(async () => await _orderController.Create(value))
				.Message.ShouldBeEquivalentTo($"Продукт с идентификатором {100} не найден");

		}

		[Fact]
		public async void Create_WithValidOrderAndNotAvailableProductQuantity_ReturnCreatedFalseWithAvailableList()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = 1000;
			var value = CreateOrderDto();
			value.Id = _orderId;
			value.Items.Add(new ()
			{
				ProductId = _productId1,
				Quantity = quantityInOrder
			});

			IActionResult result = await _orderController.Create(value);

			result.Should().NotBeNull();
			if (result!=null)
			{
				result.ShouldBeAssignableTo(typeof(Microsoft.AspNetCore.Mvc.OkObjectResult));

				OkObjectResult okResult = (OkObjectResult)result;
				okResult.Should().NotBeNull();
				okResult.Value.Should().NotBeNull();
				okResult.Value.ShouldBeAssignableTo(typeof(OrderCreateResponse));
				if (okResult!=null && okResult.Value != null)
				{
					OrderCreateResponse? response = okResult.Value as OrderCreateResponse;
					response.Should().NotBeNull();
					if (response!=null)
					{
						response.Created.Should().BeFalse();
						response.Items.Should().NotBeNull();
						response.Items?.Count.Should().Be(1);
						var available = response.Items?.First();
						available.Should().NotBeNull();
						available?.ProductId.Should().Be(_productId1);
						available?.AvailableQuantity.Should().Be(quantity- reserved-delivered);
					}

				}
			}			
		}

		[Fact]
		public void CreateOrder_WithValidOrderAndProductQuantityLess0_ThrowException()
		{
			// Arrange
			int quantity = 200;
			int reserved = 50;
			int delivered = 10;
			Product product1 = CreateProductInRepository(_productId1, quantity, reserved, delivered);
			Order order = SetupOrderInRepository(_orderId);

			int quantityInOrder = -10;
			var value = CreateOrderDto();
			value.Id = _orderId;
			value.Items.Add(new ()
			{
				ProductId = _productId1,
				Quantity = quantityInOrder
			});

			// Act				
			Should.Throw<ArgumentException>(async () => await _orderController.Create(value))
				.Message.ShouldBeEquivalentTo(ProductQuantityManipulateService.QuantityLessNullExceptionMessage); ;

		}


	}
}
