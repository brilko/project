﻿namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping
{
    public class ResponseByPageDto<T>
    {
        public int TotalPages { get; set; }

        public IEnumerable<T>? Items { get; set; }
    }
}
