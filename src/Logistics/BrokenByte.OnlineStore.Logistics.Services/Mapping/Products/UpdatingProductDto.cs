﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Services.Mapping.Products
{
    public class UpdatingProductDto
    {
        public long Id { get; set; }


        /// <summary>
        /// Артикул
        /// </summary>
        [MaxLength(50)]
        public string Article { get; set; } = string.Empty;

        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(150)]
        public string Name { get; set; } = string.Empty;

        public string Size { get; set; } = string.Empty;

    }
}
