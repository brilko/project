﻿namespace BrokenByte.OnlineStore.Logistics.Services.Mapping.Products
{
    public class ProductQuantityDto
    {
        public long Id { get; set; }
        public int AddQuantity { get; set; }
    }

}