﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping.Products
{
	public class ProductAvailableQuantityDto
	{
		public long Id { get; set; }
		public int AvailableQuantity { get; set; }

	}
}
