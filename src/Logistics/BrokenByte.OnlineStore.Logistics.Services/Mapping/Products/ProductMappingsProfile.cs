﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Domain;

namespace BrokenByte.OnlineStore.Logistics.Services.Mapping.Products
{
    public class ProductServicesMappingsProfile : Profile
    {
        public ProductServicesMappingsProfile()
        {
            CreateMap<ProductDto, Product>();

            CreateMap<Product, ProductDto>();

            CreateMap<CreatingProductDto, Product>()
               .ForMember(d => d.Quantity, map => map.Ignore())
               .ForMember(d => d.Reserved, map => map.Ignore())
               .ForMember(d => d.Delivered, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore());



        }
    }
}
