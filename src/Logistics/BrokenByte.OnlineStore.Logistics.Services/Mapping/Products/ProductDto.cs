﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Services.Mapping.Products
{
    /// <summary>
    /// Это непосредственно сама единица товара 
    /// </summary>
    public class ProductDto
    {
        public long Id { get; set; }

        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Артикул
        /// </summary>
        [MaxLength(50)]
        public string Article { get; set; } = string.Empty;

        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(150)]
        public string Name { get; set; } = string.Empty;

        public string Size { get; set; } = string.Empty;

        /// <summary>
        /// Фактическое количество товаров на складе
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Количество товара забронированное под заказы покупателей
        /// </summary>
        public int Reserved { get; set; }

        /// <summary>
        /// Количество доставляемое
        /// </summary>
        public int Delivered { get; set; }
    }
}
