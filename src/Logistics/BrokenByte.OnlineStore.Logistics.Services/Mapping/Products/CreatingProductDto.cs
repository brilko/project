﻿namespace BrokenByte.OnlineStore.Logistics.Services.Mapping.Products
{
    public class CreatingProductDto
    {
        public long Id { get; set; }

        public string Name { get; set; } = string.Empty;
        public string Article { get; set; } = string.Empty;

        public string Size { get; set; } = string.Empty;
    }
}
