﻿namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders
{
    /// <summary>
    /// Товар в заказе
    /// </summary>
    public class OrderItemDto
    {
     
        public bool Deleted { get; set; }

        public int Quantity { get; set; }

        public long ProductId { get; set; }
    }
}
