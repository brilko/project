﻿namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders
{
    public class RecipientDto
    {
        public string FullName { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;
    }
}
