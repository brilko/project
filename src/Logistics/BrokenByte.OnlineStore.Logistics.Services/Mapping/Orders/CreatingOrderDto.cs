﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders
{
    public class CreatingOrderDto
    {
        [Required]
        public long Id { get; set; }
        /// <summary>
        /// Идентификатор службы доставки
        /// </summary>
        public long DeliveryServiceId { get; set; }

        /// <summary>
        /// Номер накладной
        /// </summary>

        public string? NumberInvoice { get; set; }

        /// <summary>
        /// Полная стоимость за все продукты (c учетом промокода)
        /// </summary>
        public int TotalPrice { get; set; }

        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public long CustomerId { get; set; }

        public string CustomerName { get; set; } = string.Empty;

        public OrderStatus Status { get; set; }

        /// <summary>
        ///  адреса доставки
        /// </summary>
        public DeliveryAddressDto DeliveryAddress { get; set; } = new();

        public RecipientDto Recipient { get; set; } = new();

        public List<OrderItemDto> Items { get; set; } = [];


    }
}
