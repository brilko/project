﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders
{
	public class CreateOrderItemResponseDto
	{
		public int AvailableQuantity { get; set; }
		public long ProductId { get; set; }
	}
}
