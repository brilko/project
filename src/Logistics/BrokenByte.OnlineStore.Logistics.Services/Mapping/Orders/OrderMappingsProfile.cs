﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;

namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders
{
    public class OrderMappingsProfile : Profile
    {
        public OrderMappingsProfile()
        {
            CreateMap<DeliveryAddressDto, DeliveryAddress>().ReverseMap();
            CreateMap<RecipientDto, Recipient>().ReverseMap();

            CreateMap<OrderItem, OrderItemDto>().ReverseMap();

            CreateMap<OrderItem, ProductQuantityDto>()
                .ForMember(d => d.Id, m => m.MapFrom(b => b.ProductId))
                .ForMember(d => d.AddQuantity, m => m.MapFrom(b => b.Quantity));
        }
    }
}
