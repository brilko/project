﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders
{
	public class CreateOrderResponseDto
	{
		public required long Id { get; set; }
		public bool Created { get; set; } = false;
		public List<CreateOrderItemResponseDto>? Items { get; set; } = [];
	}
}
