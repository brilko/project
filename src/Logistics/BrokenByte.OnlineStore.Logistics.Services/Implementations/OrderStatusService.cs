﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;

namespace BrokenByte.OnlineStore.Logistics.Services.Implementations
{
    public class OrderStatusService : IOrderStatusService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Order> _repository;
        private readonly IProductQuantityManipulateService _productService;
        private readonly INotifyOrderGateway _notifyOrderService;
        public OrderStatusService(
            IMapper mapper,
            IRepository<Order> repository,
            INotifyOrderGateway notifyOrderService,
            IProductQuantityManipulateService productService
           )
        {
            _mapper = mapper;
            _productService = productService;
            _notifyOrderService = notifyOrderService;
            _repository = repository;
        }

		public const string MessageExceptionAlreadyExistOrder = "Заказ с таким ид уже существует";

		private async Task AcceptReturnedOrder(long id)
		{
			Order entity = await GetOrder(id);
			if (entity.Status == OrderStatus.DeliveryInProcess)
			{
				await _productService.CancelDelivery(_mapper.Map<ICollection<OrderItem>, ICollection<ProductQuantityDto>>(entity.Items));
				await ChangeOrderStatus(entity, OrderStatus.Canceled, null, null);
			}
			else
			{
				throw new ArgumentException("Статус заказа не позволяет выполнить операцию");
			}
		}

        private async Task CanceledOrder(long id, string? message)
        {
            Order entity = await GetOrder(id);
            if (entity.Status != OrderStatus.Canceled)
            {
                if (NeedToCancelReservation(entity.Status))
                {
                    await _productService.CancelReserved(_mapper.Map<ICollection<OrderItem>, ICollection<ProductQuantityDto>>(entity.Items));
                }
                await ChangeOrderStatus(entity, OrderStatus.Canceled, message, null);
            }
        }

        private static bool NeedToCancelReservation(OrderStatus status)
        {
            switch (status)
            {
                case OrderStatus.Canceled:
                case OrderStatus.Returned:
                case OrderStatus.Delivered:
                case OrderStatus.DeliveryInProcess:
                case OrderStatus.TransferredToDeliveryService:
                    return false;


                case OrderStatus.Created:
                case OrderStatus.AssemblyStart:
                case OrderStatus.AssemblyEndAndReadyForDelivery:
                    return true;

                default:
                    return false;
            }
        }

        public async Task CancelOrder(long orderId)
        {
            await CanceledOrder(orderId, "Отмена из вне");
        }

        private async Task ConfirmOrderDelivery(long id)
        {
            Order entity = await GetOrder(id);
            if (entity.Status == OrderStatus.DeliveryInProcess)
            {
                await _productService.ApplyDelivered(_mapper.Map<ICollection<OrderItem>, ICollection<ProductQuantityDto>>(entity.Items));
                await ChangeOrderStatus(entity, OrderStatus.Delivered, null, null);
            }
            else
            {
                throw new ArgumentException("Статус заказа не позволяет выполнить операцию");
            }
        }

        private Order MapOrderFromCreatingOrderDto(CreatingOrderDto order)
        {
			Order entity = new()
			{
				Id = order.Id,
				CustomerId = order.CustomerId,
				CustomerName = order.CustomerName,
				Deleted = false,
				DeliveryAddress = new DeliveryAddress()
				{
					Address = order.DeliveryAddress.Address
				},
				DeliveryServiceId = order.DeliveryServiceId,
				History = new List<OrderHistoryItem>(),
				Items = new List<OrderItem>(),
				NumberInvoice = order.NumberInvoice?? "", 
				Recipient = new Recipient()
				{
					FullName = order.Recipient.FullName,
					Phone = order.Recipient.Phone,
				},
				Status = OrderStatus.Created,
				TotalPrice = order.TotalPrice
			};
			foreach (var item in order.Items)
			{
				entity.Items.Add(new OrderItem()
				{
					Deleted = false,
					ProductId = item.ProductId,
					Quantity = item.Quantity
				});
			}
			return entity;
		}

        private List<ProductQuantityDto> MapCreatingOrderDtoItemsToProductQuantityDto(CreatingOrderDto order)
        {
			List<ProductQuantityDto> quantityDtos = new List<ProductQuantityDto>();
			foreach (var item in order.Items)
			{
				quantityDtos.Add(new ProductQuantityDto()
				{
					Id = item.ProductId,
					AddQuantity = item.Quantity
				});
				
			}
            return quantityDtos;
		}

		public async Task<CreateOrderResponseDto> CreateOrder(CreatingOrderDto order)
		{
            Order entity = MapOrderFromCreatingOrderDto(order);
            List<ProductQuantityDto> quantityDtos = MapCreatingOrderDtoItemsToProductQuantityDto(order);


            CreateOrderResponseDto response = new CreateOrderResponseDto()
            {
                Id = order.Id,
                Created = true
            };

			var result = await _productService.AddToReserved(quantityDtos);
            if (result.ToList().Count == 0)
            {
                await _repository.AddAsync(entity);
                
            }
            else
            {
                response.Created = false;
                response.Items = new List<CreateOrderItemResponseDto>();
                foreach(var item in result.ToList())
                {
                    response.Items.Add(new CreateOrderItemResponseDto()
                    {
                        AvailableQuantity = item.AvailableQuantity,
                        ProductId = item.Id
                    });
				}
			}
            return response;
		}

        private async Task EndAssemblyOrder(long id)
        {
            await ChangeOrderStatus(id, OrderStatus.AssemblyStart, OrderStatus.AssemblyEndAndReadyForDelivery, null, null);
        }

        private async Task EndTransferredOrderToDeliveryService(long id)
        {
            Order entity = await GetOrder(id);
            if (entity.Status == OrderStatus.TransferredToDeliveryService)
            {
                await _productService.ReservedToDelivery(_mapper.Map<ICollection<OrderItem>, ICollection<ProductQuantityDto>>(entity.Items));
                await ChangeOrderStatus(entity, OrderStatus.DeliveryInProcess, null, null);
            }
            else
            {
                throw new ArgumentException("Статус заказа не позволяет выполнить операцию");
            }
        }

        public async Task StartAssemblyOrder(long id)
        {
            await ChangeOrderStatus(id, OrderStatus.Created, OrderStatus.AssemblyStart, null, null);
        }

        private async Task StartTransferredOrderToDeliveryService(long id)
        {
            Order entity = await GetOrder(id);
            if (entity.Status == OrderStatus.AssemblyEndAndReadyForDelivery)
            {
                await ChangeOrderStatus(entity, OrderStatus.TransferredToDeliveryService, null, null);
            }
            else
            {
                throw new ArgumentException("Статус заказа не позволяет выполнить операцию");
            }
        }

        private async Task<Order> GetOrder(long id)
        {
            var entity = await _repository.GetByIdAsync(id)
                        ?? throw new ArgumentException($"Заказ с идентификатором {id} не найден");
            if (entity.Deleted) throw new ArgumentException($"Заказ с идентификатором {entity.Id} удален");
            return entity;
        }

        private async Task ChangeOrderStatus(long id, OrderStatus from, OrderStatus to, string? message, long? employeeId)
        {
            Order entity = await GetOrder(id);
            if (entity.Status == from)
            {
                await ChangeOrderStatus(entity, to, message, employeeId);
            }
            else
            {
                throw new ArgumentException("Статус заказа не позволяет выполнить операцию");
            }
        }

        private async Task ChangeOrderStatus(Order entity, OrderStatus newStatus, string? message, long? employeeId)
        {
            if (entity.Deleted) throw new ArgumentException($"Заказ с идентификатором {entity.Id} удален");

            entity.Status = newStatus;
            entity.History.Add(new Core.Domain.Entities.OrderHistoryItem()
            {
                Status = newStatus,
                Date = DateTime.Now,
                EmployeeId = employeeId
            });
            await _repository.UpdateAsync(entity);
            await _notifyOrderService.SendNotificationAboutOrderStatusChanged(entity.Id, newStatus, message);
        }

        public async Task EditStatus(long orderId, OrderStatus status, string? message)
        {
            switch (status)
            {
                case OrderStatus.Canceled:
                    await this.CanceledOrder(orderId, message);
                    break;


                case OrderStatus.Returned:
                    await this.AcceptReturnedOrder(orderId);
                    break;


                case OrderStatus.Delivered:
                    await this.ConfirmOrderDelivery(orderId);
                    break;
                case OrderStatus.DeliveryInProcess:
                    await this.EndTransferredOrderToDeliveryService(orderId);
                    break;
                case OrderStatus.TransferredToDeliveryService:
                    await this.StartTransferredOrderToDeliveryService(orderId);
                    break;


                case OrderStatus.Created:
                    throw new ArgumentException("Статус заказа Created только непосредственно при установке");

                case OrderStatus.AssemblyStart:
                    await this.StartAssemblyOrder(orderId);
                    break;

                case OrderStatus.AssemblyEndAndReadyForDelivery:
                    await this.EndAssemblyOrder(orderId);
                    break;
            }


        }

		
	}
}
