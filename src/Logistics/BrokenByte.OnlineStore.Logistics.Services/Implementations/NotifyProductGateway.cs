﻿using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using Microsoft.Extensions.Logging;

namespace BrokenByte.OnlineStore.Logistics.Integration.Implementations
{
    public class NotifyProductGateway : INotifyProductGateway
    {
        private readonly ILogger<NotifyProductGateway> _logger;
        public NotifyProductGateway(ILogger<NotifyProductGateway> logger)
        {
            _logger = logger;
        }
        public Task SendNotificationAboutProductQuantityChange(long productId, int quantity)
        {
            _logger.LogInformation("SendNotificationAboutProductQuantityChange");
            return Task.CompletedTask;

        }
    }
}
