﻿using AutoMapper;
using BrokenByte.OnlineStore.Logistics.Core.Abstractions;
using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Products;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;

namespace BrokenByte.OnlineStore.Logistics.Services.Implementations
{
    public class ProductQuantityManipulateService : IProductQuantityManipulateService
    {
        private readonly IMapper _mapper;

        private readonly IRepository<Product> _repository;
        private readonly INotifyProductGateway _notifyService;

        public ProductQuantityManipulateService(IMapper mapper, IRepository<Product> repository, INotifyProductGateway notifyService)
        {
            _mapper = mapper;
            _repository = repository;
            _notifyService = notifyService;
        }

		public async Task<IEnumerable<ProductAvailableQuantityDto>> AddToReserved(IEnumerable<ProductQuantityDto> values)
		{
			List<ProductAvailableQuantityDto> availableList = [];
			List<Product> list = [];
			foreach (ProductQuantityDto item in values)
			{
				var entity = await GetOneById(item.Id);
				if (item.AddQuantity < 0)
				{
					throw new ArgumentException(QuantityLessNullExceptionMessage);
				}

				if (entity.Quantity - entity.Delivered < entity.Reserved + item.AddQuantity)
				{
					availableList.Add(new ProductAvailableQuantityDto()
					{
						AvailableQuantity = entity.Quantity - entity.Delivered - entity.Reserved,
						Id = entity.Id
					});
					//throw new ArgumentException(ReservedQuantityMoreThenQuantityAndDeliveredExceptionMessage);
				}
				else
				{
					entity.Reserved += item.AddQuantity;
					list.Add(entity);
				}
			}

			if (availableList.Count == 0)
			{
				foreach (Product item in list)
				{
					await _repository.UpdateAsync(item);
				}
			}
			return availableList;
        }

		public async Task ApplyDelivered(IEnumerable<ProductQuantityDto> values)
		{
			List<Product> list = [];
			foreach (ProductQuantityDto item in values)
			{
				var entity = await GetOneById(item.Id);
				if (item.AddQuantity < 0)
				{
					throw new ArgumentException(QuantityLessNullExceptionMessage);
				}
				if (entity.Quantity < item.AddQuantity)
				{
					throw new ArgumentException(DeliveredMoreThenQuantityExceptionMessage);
				}
				if (entity.Delivered < item.AddQuantity)
				{
					throw new ArgumentException(DeliveredApplyMoreThenDeliveredExceptionMessage);
				}
				else
				{
					entity.Delivered -= item.AddQuantity;
					entity.Quantity -= item.AddQuantity;
				}
				list.Add(entity);
			}

            foreach (Product item in list)
            {
                await _repository.UpdateAsync(item);
                await _notifyService.SendNotificationAboutProductQuantityChange(item.Id, item.Quantity);
            }
        }

		public async Task CancelDelivery(IEnumerable<ProductQuantityDto> values)
		{
			List<Product> list = [];
			foreach (ProductQuantityDto item in values)
			{
				var entity = await GetOneById(item.Id);
				if (item.AddQuantity < 0)
				{
					throw new ArgumentException(QuantityLessNullExceptionMessage);
				}

				if (entity.Delivered < item.AddQuantity)
				{
					throw new ArgumentException(CancelDeliveryMoreThenDeliveredExceptionMessage);
				}
				else
				{
					entity.Delivered -= item.AddQuantity;
				}
				list.Add(entity);
			}

            foreach (Product item in list)
            {
                await _repository.UpdateAsync(item);
            }
        }

		public async Task CancelReserved(IEnumerable<ProductQuantityDto> values)
		{
			List<Product> list = [];
			foreach (ProductQuantityDto item in values)
			{
				var entity = await GetOneById(item.Id);
				if (item.AddQuantity < 0)
				{
					throw new ArgumentException(QuantityLessNullExceptionMessage);
				}
				if (entity.Reserved < item.AddQuantity)
				{
					throw new ArgumentException(CancelReservedQuantityMoreThenReservedExceptionMessage);
				}

				entity.Reserved -= item.AddQuantity;

				list.Add(entity);
			}

            foreach (Product item in list)
            {
                await _repository.UpdateAsync(item);
            }
        }

        public async Task AddQuantity(ProductQuantityDto addValues)
        {

			var entity = await GetOneById(addValues.Id);
			if (addValues.AddQuantity < 0)
			{
				if (entity.Quantity + addValues.AddQuantity < entity.Reserved + entity.Delivered)
				{
					throw new ArgumentException(AddQuantityLessReservedExceptionMessage);
				}
			}
			entity.Quantity += addValues.AddQuantity;
			await _repository.UpdateAsync(entity);
			await _notifyService.SendNotificationAboutProductQuantityChange(entity.Id, entity.Quantity);

        }

		public async Task SetQuantity(ProductQuantityDto addValues)
		{
			var entity = await GetOneById(addValues.Id);
			if (addValues.AddQuantity < 0)
			{
				if ((entity.Delivered == 0) && (entity.Reserved == 0))
				{
				}
				else throw new ArgumentException(ExceptionMessageSetCountQuantity);

			}
			else if (entity.Reserved + entity.Delivered > addValues.AddQuantity)
			{
				throw new ArgumentException(ExceptionMessageSetCountQuantity);
			}

			entity.Quantity = addValues.AddQuantity;
			await _repository.UpdateAsync(entity);
			await _notifyService.SendNotificationAboutProductQuantityChange(entity.Id, entity.Quantity);

        }

		public async Task ReservedToDelivery(IEnumerable<ProductQuantityDto> values)
		{
			List<Product> list = [];
			foreach (ProductQuantityDto item in values)
			{
				var entity = await GetOneById(item.Id);
				if (item.AddQuantity < 0)
				{
					throw new ArgumentException(QuantityLessNullExceptionMessage);
				}

				if (entity.Reserved < item.AddQuantity)
				{
					throw new ArgumentException(ToDeliveredQuantityMoreThenReservedExceptionMessage);
				}
				else
				{
					entity.Reserved -= item.AddQuantity;
					entity.Delivered += item.AddQuantity;
				}
				list.Add(entity);
			}

            foreach (Product item in list)
            {
                await _repository.UpdateAsync(item);
            }
        }

		private async Task<Product> GetOneById(long id)
		{
			var entity = await _repository.GetByIdAsync(id)
						?? throw new ArgumentException($"Продукт с идентификатором {id} не найден");
			if (entity.Deleted) throw new ArgumentException($"Продукт с идентификатором {entity.Id} удален");
			return entity;
		}

		public const string ExceptionMessageSetCountQuantity = "Зарезервировано и доставке в сумме меньше, чем задаваемое";
		public const string QuantityLessNullExceptionMessage = "Величина коррекции не должна быть отрицательной";
		public const string QuantityAddLessExceptionMessage = "Общее количество слишком мало для такой коррекции";
		public const string AddQuantityLessReservedExceptionMessage = "Зарезервировано и доставке в сумме меньше, чем суммарное плюс коррекция";
		public const string ToDeliveredQuantityMoreThenReservedExceptionMessage = "Запрошенное значение для передачи в доставку больше чем зарезервированное количество";
		public const string ReservedQuantityMoreThenQuantityAndDeliveredExceptionMessage = "Запрошенное значение для резервирования больше чем общее количество плюс доствки";
		public const string CancelReservedQuantityMoreThenReservedExceptionMessage = "Запрошенное значение для отмены больше чем всего зарезервировано";
		public const string DeliveredMoreThenQuantityExceptionMessage = "Запрошенное для получения количество больше общего количества товаров";
		public const string DeliveredApplyMoreThenDeliveredExceptionMessage = "Запрошенное для получения количество больше доставляемого количества";
		public const string CancelDeliveryMoreThenDeliveredExceptionMessage = "Запрошенное для отмены количество больше доставляемого количества";

    }
}
