﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using BrokenByte.OnlineStore.Logistics.Services.Abstractions;
using Microsoft.Extensions.Logging;

namespace BrokenByte.OnlineStore.Logistics.Integration.Implementations
{
    public class NotifyOrderGateway : INotifyOrderGateway
    {
        private readonly ILogger<NotifyProductGateway> _logger;
        public NotifyOrderGateway(ILogger<NotifyProductGateway> logger)
        {
            _logger = logger;
        }

        public Task SendNotificationAboutOrderStatusChanged(long orderId, OrderStatus status, string? deteilInfo)
        {
            _logger.LogInformation("SendNotificationAboutOrderStatusChanged");
            return Task.CompletedTask;
        }


    }

}
