﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;

namespace BrokenByte.OnlineStore.Logistics.Services.Abstractions
{
    public interface INotifyOrderGateway
    {
        Task SendNotificationAboutOrderStatusChanged(long orderId, OrderStatus status, string? deteilInfo);
    }
}
