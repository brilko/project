﻿using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Products;
using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;

namespace BrokenByte.OnlineStore.Logistics.Services.Abstractions
{
    public interface IProductQuantityManipulateService
    {

        /// <summary>
        /// Добавить количество товара
        /// </summary>
        /// <param name="id">ид товара</param>
        /// <param name="addedValue">на сколько увеличить/уменьшить количество товара</param>
        Task AddQuantity(ProductQuantityDto addValues);
        Task SetQuantity(ProductQuantityDto addValues);


        Task CancelReserved(IEnumerable<ProductQuantityDto> values);
        Task<IEnumerable<ProductAvailableQuantityDto>> AddToReserved(IEnumerable<ProductQuantityDto> values);

        Task ReservedToDelivery(IEnumerable<ProductQuantityDto> values);
        Task CancelDelivery(IEnumerable<ProductQuantityDto> values);
        Task ApplyDelivered(IEnumerable<ProductQuantityDto> values);
    }
}
