﻿using BrokenByte.OnlineStore.Logistics.Services.Mapping.Products;

namespace BrokenByte.OnlineStore.Logistics.Services.Abstractions
{
    public interface IGivingProductGateway
    {
        Task<long> Create(CreatingProductDto productDto);

        Task UpdateInfo(UpdatingProductDto productDto);

    }
}
