﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;

namespace BrokenByte.OnlineStore.Logistics.Services.Abstractions
{
    public interface IOrderStatusService
    {
        Task EditStatus(long orderId, OrderStatus status, string? message);

        Task<CreateOrderResponseDto> CreateOrder(CreatingOrderDto order);
    }
}
