﻿namespace BrokenByte.OnlineStore.Logistics.Services.Abstractions
{
    public interface INotifyProductGateway
    {
        Task SendNotificationAboutProductQuantityChange(long productId, int quantity);
    }
}
