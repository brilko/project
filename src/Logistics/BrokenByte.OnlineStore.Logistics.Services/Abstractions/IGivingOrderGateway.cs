﻿using BrokenByte.OnlineStore.Logistics.Integration.Mapping.Orders;

namespace BrokenByte.OnlineStore.Logistics.Services.Abstractions
{
    public interface IGivingOrderGateway
    {
        Task CreateOrder(CreatingOrderDto order);

        Task StartAssemblyOrder(long orderId);

        Task CancelOrder(long orderId);
    }
}
