﻿using BrokenByte.OnlineStore.Logistics.Core.Domain;

namespace BrokenByte.OnlineStore.Logistics.Core.Abstractions
{
    public class ResponseByPage<T> where T : BaseEntity
    {
        /// <summary>
        /// Количество страниц
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Сущности с конкретной страницы
        /// </summary>
        public IEnumerable<T>? Items { get; set; }
    }
}
