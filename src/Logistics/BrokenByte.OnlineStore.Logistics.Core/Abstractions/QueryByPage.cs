﻿namespace BrokenByte.OnlineStore.Logistics.Core.Repositories
{
    public class QueryByPage
    {
        public int PageIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }
}
