﻿using BrokenByte.OnlineStore.Logistics.Core.Domain;
using BrokenByte.OnlineStore.Logistics.Core.Repositories;
using System.Linq.Expressions;

namespace BrokenByte.OnlineStore.Logistics.Core.Abstractions
{
    public interface IRepository<T>
        where T : BaseEntity
    {

        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(long id);


        Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);
        Task<ResponseByPage<T>> GetByPage(QueryByPage query, Expression<Func<T, bool>> predicate);
        Task<ResponseByPage<T>> GetByPage(QueryByPage query);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(long id);
    }
}
