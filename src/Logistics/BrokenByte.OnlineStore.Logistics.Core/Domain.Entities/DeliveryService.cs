﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    /// <summary>
    /// Служба доставки
    /// </summary>
    public class DeliveryService : BaseEntity
    {
        /// <summary>
        /// Название транспортной компании
        /// </summary>
        [MaxLength(120)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(120)]
        public string? Description { get; set; }

        [MaxLength(120)]
        public string? Email { get; set; }

        [MaxLength(120)]
        public string? Phone { get; set; }

        [MaxLength(512)]
        public string? Address { get; set; }

    }
}
