﻿namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    /// <summary>
    /// Это непосредственно сама единица товара 
    /// </summary>
    public class Product : BaseEntity
    {

        public string Article { get; set; } = string.Empty;


        public string Name { get; set; } = string.Empty;

        public string Size { get; set; } = string.Empty;

        /// <summary>
        /// Фактическое количество товаров на складе
        /// </summary>
        public int Quantity { get; set; } = 0;

        /// <summary>
        /// Количество товара забронированное под заказы покупателей
        /// </summary>
        public int Reserved { get; set; } = 0;

        /// <summary>
        /// Количество доставляемое
        /// </summary>
        public int Delivered { get; set; } = 0;
    }
}
