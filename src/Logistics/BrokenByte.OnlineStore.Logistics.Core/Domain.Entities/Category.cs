﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    // <summary>
    /// Категория товаров
    /// </summary>
    public class Category : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(100)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Ссылка на картинку
        /// </summary>
        [MaxLength(1024)]
        public string Image { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(256)]
        public string? Description { get; set; }


    }
}
