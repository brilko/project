﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    /// <summary>
    /// Адрес доставки
    /// </summary>
    public class DeliveryAddress
    {
        /// <summary>
        /// Адрес
        /// </summary>
        [MaxLength(512)]
        public string Address { get; set; } = string.Empty;
    }
}
