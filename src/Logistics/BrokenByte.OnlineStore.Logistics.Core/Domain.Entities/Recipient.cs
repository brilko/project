﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    /// <summary>
    /// Получатель заказа
    /// </summary>
    public class Recipient
    {
        /// <summary>
        /// Имя получателя
        /// </summary>
        [MaxLength(120)]
        public string FullName { get; set; } = string.Empty;

        /// <summary>
        /// Номер телефона получателя
        /// </summary>
        [MaxLength(50)]
        public string Phone { get; set; } = string.Empty;

    }
}
