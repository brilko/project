﻿using BrokenByte.OnlineStore.Logistics.Core.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    /// <summary>
    /// Заказ
    /// </summary>
    public class Order : BaseEntity
    {

        /// <summary>
        /// Идентификатор службы доставки
        /// </summary>
        public long DeliveryServiceId { get; set; }

        /// <summary>
        /// Номер накладной
        /// </summary>
        [MaxLength(50)]
        public string NumberInvoice { get; set; } = string.Empty;

        /// <summary>
        /// Полная стоимость за все продукты (c учетом промокода)
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// Имя клиента
        /// </summary>
		public string CustomerName { get; set; } = string.Empty;


        /// <summary>
        ///  статуса заказа
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        ///  адреса доставки
        /// </summary>
        public DeliveryAddress DeliveryAddress { get; set; } = new();

        public Recipient Recipient { get; set; } = new();

        public List<OrderItem> Items { get; set; } = [];

        public List<OrderHistoryItem> History { get; set; } = [];

    }
}
