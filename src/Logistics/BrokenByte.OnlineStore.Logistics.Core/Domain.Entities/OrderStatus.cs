﻿namespace BrokenByte.OnlineStore.Logistics.Core.Domain.Entities
{
    /// <summary>
    /// Статус заказа
    /// </summary>
    public enum OrderStatus
    {
        Created,            //создан
        AssemblyStart,          //на сборке		
        AssemblyEndAndReadyForDelivery, //собран и готов к передаче в доставку
        TransferredToDeliveryService, //понесли в транспортную компанию
        DeliveryInProcess, //в процессе доставки транспортной компанией
        Returned,
        Delivered, //доставлен		
        Canceled  //отменен
    }
}
