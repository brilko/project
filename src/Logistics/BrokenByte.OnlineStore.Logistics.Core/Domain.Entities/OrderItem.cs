﻿
namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    /// <summary>
    /// Товар в заказе
    /// </summary>
    public class OrderItem : BaseEntity
    {
        /// <summary>
        /// Количество в заказе
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Идентификатор товара
        /// </summary>
        public long ProductId { get; set; }


    }
}
