﻿namespace BrokenByte.OnlineStore.Logistics.Core.Domain.Entities
{
    public class OrderHistoryItem
    {
        public OrderStatus Status { get; set; }
        public DateTime Date { get; set; }
        public long? EmployeeId { get; set; }
    }
}
