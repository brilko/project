﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BrokenByte.OnlineStore.Logistics.Core.Domain
{
    /// <summary>
    /// Абстрактный класс сущности с идентификатором.
    /// </summary>
    public abstract class BaseEntity
    {
		/// <summary>
		/// Идентификатор.
		/// </summary>
		[BsonId]
		public long Id { get; set; }

        public bool Deleted { get; set; } = false;
    }
}