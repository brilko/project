﻿namespace OrdersMicroserviceBusContracts
{
    public interface ILogisticBus
    {
        Task CancelDeliveryOrder(long orderDocumentId);
        Task<bool> CollectDeliveryOrder(OrderDeliveryBusDto orderDelivery);
        Task StartDelivery(long orderDocumentId);
    }
}

/**
 * По идее мне нужно, чтобы !данные! из микросервиса отправлялись в микросервис логистики
 * От микросервиса логистики мне нужны данные о том, что я могу собрать заказ
 * Мне нужно отправить сообщение, что нужно отменить заказ
 * Отправить сообщение, что нужно отправить заказ
 * 
 * Нужно понять, что я должен отправить и что получить
 * 
 */