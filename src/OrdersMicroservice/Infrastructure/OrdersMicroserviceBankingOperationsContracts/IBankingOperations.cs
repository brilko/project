﻿namespace OrdersMicroserviceBankingOperationsContracts
{
    public interface IBankingOperations
    {
        Task<Guid?> Pay(long userId, int totalSum);
    }
}
