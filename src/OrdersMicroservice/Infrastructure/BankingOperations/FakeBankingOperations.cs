﻿using OrdersMicroserviceBankingOperationsContracts;

namespace OrdersMicroserviceBankingOperations
{
    public class FakeBankingOperations : IBankingOperations
    {
        public Task<Guid?> Pay(long userId, int totalSum)
        {
            return Task.Run(() =>
            {
                Console.WriteLine($"Проведение оплаты для пользователя {userId} на сумму {totalSum}");
                //var isCorrectPay = someFunction();
                //if (!isCorrect)
                //{
                //  Console.Writeline("Произошла ошибка");
                //  return null; 
                //}
                Console.WriteLine("Оплата прошла успешно");
                return (Guid?)Guid.NewGuid();
            });
        }
    }
}
