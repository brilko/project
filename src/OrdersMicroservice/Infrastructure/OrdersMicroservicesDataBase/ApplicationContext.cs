﻿using BrokenByte.OnlineStore.Domain.Entities.Orders;
using Microsoft.EntityFrameworkCore;

namespace OrdersMicroservicesDataBase
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        public DbSet<OrderDocument> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderDocumentStatus> OrderStatuses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var statuses = new List<OrderDocumentStatus>();
            var countStatuses = Enum.GetNames<OrderDocumentStatusEnum>().Length + 1;
            for (int i = 1; i < countStatuses; i++)
                statuses.Add(new OrderDocumentStatus()
                {
                    Id = i,
                    Name = ((OrderDocumentStatusEnum)i).ToString(),
                });
            modelBuilder.Entity<OrderDocumentStatus>().HasData(statuses);
        }
    }
}
