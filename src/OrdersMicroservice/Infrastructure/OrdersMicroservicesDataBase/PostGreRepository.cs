﻿using BrokenByte.OnlineStore.Domain.Entities;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using OrdersMicroservicesDataBase;

namespace OrdersMicroservicesDataAccess
{
    public class PostGreRepository<T>(ApplicationContext context) : IRepository<T> where T : BaseEntity
    {
        private readonly ApplicationContext context = context;
        private readonly DbSet<T> set = context.Set<T>();

        public async Task<T> AddAsync(T entity)
        {
            var entry = await set.AddAsync(entity);
            var newEntity = entry.Entity;
            await context.SaveChangesAsync();
            return newEntity;
        }

        public Task AddRangeAsync(ICollection<T> entities)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> GetAll()
        {
            return set;
        }

        public Task<T?> GetAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> GetPageAsync(Page page)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsExistAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryDeleteAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryDeleteRangeAsync(ICollection<T> entities)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryUpdateAsync(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
