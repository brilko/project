﻿using OrdersMicroserviceBusContracts;

namespace OrdersMicroserviceBus
{
    public class LogisticBus : ILogisticBus
    {
        public Task CancelDeliveryOrder(long orderDocumentId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CollectDeliveryOrder(OrderDeliveryBusDto orderDelivery)
        {
            throw new NotImplementedException();
        }

        public Task StartDelivery(long orderDocumentId)
        {
            throw new NotImplementedException();
        }
    }
}
