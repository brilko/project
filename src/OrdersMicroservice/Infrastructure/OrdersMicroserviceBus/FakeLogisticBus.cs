﻿using OrdersMicroserviceBusContracts;

namespace OrdersMicroserviceBus
{
    public class FakeLogisticBus : ILogisticBus
    {
        public Task CancelDeliveryOrder(long orderDocumentId)
        {
            return Task.CompletedTask;
        }

        public Task<bool> CollectDeliveryOrder(OrderDeliveryBusDto orderDelivery)
        {
            return Task.FromResult(true);
        }

        public Task StartDelivery(long orderDocumentId)
        {
            return Task.CompletedTask;
        }
    }
}
