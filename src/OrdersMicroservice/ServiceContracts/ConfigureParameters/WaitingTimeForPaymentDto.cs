﻿namespace OrdersMicroserviceServiceContracts.ConfigureParameters
{
    public class WaitingTimeForPaymentDto(int waitingMinutes)
    {
        public TimeSpan WaitingTime { get; set; } = TimeSpan.FromMinutes(waitingMinutes);
    }
}
