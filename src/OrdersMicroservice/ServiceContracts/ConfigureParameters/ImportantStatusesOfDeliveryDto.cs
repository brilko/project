﻿namespace OrdersMicroserviceServiceContracts.ConfigureParameters
{
    public class ImportantStatusesOfDeliveryDto
    {
        public List<string> Collected { get; set; } = [];
        public List<string> Undelivered { get; set; } = [];
        public List<string> Delivered { get; set; } = [];
    }
}
