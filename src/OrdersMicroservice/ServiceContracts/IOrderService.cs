﻿using OrdersMicroserviceServiceContracts.DTOs;

namespace OrdersMicroserviceServiceContracts
{
    public interface IOrderService
    {
        Task<long> CreateOrder(CreateOrderDocumentDto createOrderDto);

        enum AnswersTryCancelOrder
        {
            NotFound,
            OrderDocumentNotInCollectedState,
            Ok
        }
        Task<AnswersTryCancelOrder> TryCancelOrder(long orderDocumentId);

        enum AnswersTryPayOrder
        {
            Ok,
            BankingOperationError,
            NotFound,
            OrderDocumentNotInCollectedState
        }
        Task<AnswersTryPayOrder> TryPayOrder(long orderId);

        enum AnswersTryDeliveryOrder
        {
            Ok,
            NotFound
        }

        Task<AnswersTryDeliveryOrder> ChangeDeliveryStatus(long orderId, string deliveryStatus);

        Task<GetLongOrderDocumentDto?> TryGetOrderByOrderId(long orderId);

        Task<List<GetShortOrderDocumentDto>> GetOrdersByUserId(long userId);
    }
}
