﻿namespace OrdersMicroserviceServiceContracts.DTOs
{
    public class GetOrderItemDto
    {
        public long Id { get; set; }

        public int Quantity { get; set; }

        public int PricePerOne { get; set; }

        public long ProductId { get; set; }
    }
}
