﻿namespace OrdersMicroserviceServiceContracts.DTOs
{
    public class CreateOrderItemDto
    {
        public int Quantity { get; set; }

        public int PricePerOne { get; set; }

        public long ProductId { get; set; }
    }
}
