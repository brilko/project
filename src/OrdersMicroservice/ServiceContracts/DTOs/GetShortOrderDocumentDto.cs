﻿namespace OrdersMicroserviceServiceContracts.DTOs
{
    public class GetShortOrderDocumentDto
    {
        public int Id { get; set; }
        public DateTime CreateDateTime { get; set; } = DateTime.MinValue;
        /// <summary>
        /// Номер накладной
        /// </summary>
        public string NumberInvoice { get; set; } = Guid.Empty.ToString();
        public string OrderStatus { get; set; } = string.Empty;
    }
}
