﻿namespace OrdersMicroserviceServiceContracts.DTOs
{
    public class CreateOrderDocumentDto
    {
        public DateTime CreateDateTime { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Номер накладной
        /// </summary>
        public string NumberInvoice { get; set; } = Guid.Empty.ToString();

        public string CustomerFullName { get; set; } = string.Empty;

        /// <summary>
        /// Это идентификатор в базе данных, где храняться данные о пользователе.
        /// Эта база данных находится в другом микросервисе.
        /// </summary>
        public long CustomerId { get; set; }

        public string AddresseeFullName { get; set; } = string.Empty;

        public string AdresseePhone { get; set; } = string.Empty;

        public string DeliveryAddress { get; set; } = string.Empty;

        public List<CreateOrderItemDto> OrderItems { get; set; } = [];

        /// <summary>
        /// Полная стоимость за все продукты
        /// </summary>
        public int TotalPrice { get; set; }
    }
}
