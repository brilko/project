﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using OrdersMicroserviceBankingOperationsContracts;
using OrdersMicroserviceServiceContracts;
using OrdersMicroserviceServiceContracts.ConfigureParameters;
using OrdersMicroserviceServiceContracts.DTOs;
using static OrdersMicroserviceServiceContracts.IOrderService;

using EmailSender;
using Microsoft.Extensions.Options;
using EmailLibrary;

namespace OrdersMicroserviceServices
{
    public class OrderService(
        IMapper mapper,
        IRepository<OrderDocument> documentRepository,
        IBankingOperations bankingOperations,
        ImportantStatusesOfDeliveryDto importantStatuses,
        WaitingTimeForPaymentDto waitingTimeForPayment) : IOrderService
    {
        private readonly IMapper mapper = mapper;
        private readonly IRepository<OrderDocument> documentRepository = documentRepository;
        private readonly IBankingOperations bankingOperations = bankingOperations;
        private readonly ImportantStatusesOfDeliveryDto importantStatuses = importantStatuses;
        private readonly WaitingTimeForPaymentDto waitingTimeForPayment = waitingTimeForPayment;
        private readonly Dictionary<long, CancellationTokenSource> createdNotPaidOrders = [];

        public async Task<long> CreateOrder(CreateOrderDocumentDto createOrderDto)
        {
            var emailService = new EmailService();
            await emailService.Send("Test", "Test2");

            return 1;












            // Нужно создать заказ со статусом Collecting
            // Сохранить его в бд 
            // Отправить сообщение микросервису логистики, что нужно собрать заказ
            // Если логистика ответил, что собрал,
            //      то обновить статус заказа на Collected, обновить в бд,
            //      сообщить пользователю, что заказ собран
            // Если логистика ответил, что заказ не собран,
            //      то обновить статус на Uncolleted, обновить в бд,
            //      сообщить пользователю, что заказ не собран
            // Если после истечения времени ожидания оплаты принудительно отменить заказ
            // Вернуть ид созданного документа
            var orderDocument = mapper.Map<OrderDocument>(createOrderDto);
            orderDocument.OrderItems!.ForEach(i => i.OrderId = orderDocument.Id);
            orderDocument.OrderStatusId = (long)OrderDocumentStatusEnum.Collecting;
            var savedOrderDocument = await documentRepository.AddAsync(orderDocument);

            // Должна быть отправка данных в логистику, обновить данные на основе этих данных

            //Task.Run(async () => 
            //{
            //    Thread.Sleep(waitingTimeForPayment.WaitingTime);

            //    savedOrderDocument.OrderStatusId = (long)OrderDocumentStatusEnum.Cancelled;
            //    await 
            //},).Start();

            throw new NotImplementedException();


            return savedOrderDocument.Id;
        }


        private enum AnswersCancelationOrderDocument
        {
            orderDocumentNotInCollectedState
        }
        private async Task<AnswersCancelationOrderDocument> CancelOrder(OrderDocument orderDocument)
        {
            // Проверить, что документ собран. Если нет, то вывести ошибку.
            if (orderDocument.OrderStatusId != (long)OrderDocumentStatusEnum.Collected)
                return AnswersCancelationOrderDocument.orderDocumentNotInCollectedState;

            // Обновить состояние документа
            orderDocument.OrderStatusId = (long)OrderDocumentStatusEnum.Cancelled;
            await documentRepository.TryUpdateAsync(orderDocument);

            // Сообщить логистике, что нужно отменить заказ

            // Отправить сообщение пользователю, что заказ отменён
            throw new NotImplementedException();

        }

        public async Task<AnswersTryCancelOrder> TryCancelOrder(long orderDocumentId)
        {
            // Найти в бд документ. Если нет, то сообщить, что не найден
            // Проверить статус заказа. Он должен быть Collected, иначе сообщить, что не в том состоянии
            // Сообщить в логистику, что заказ должен быть отменён. 
            // Получить сообщение от логистики, что заказ успешно отменён. Иначе сообщить пользователю
            //      об ошибке.
            // Обновить статус заказа, обновить в бд
            // Сообщить пользователю, что заказ отменён


            // Поиск в базе данных документа заказа. Если его нет, то ошибка
            var orderDocumentEntity = await documentRepository.GetAsync(orderDocumentId);
            if (orderDocumentEntity == null)
                return AnswersTryCancelOrder.NotFound;

            // При попытке отменить заказ, находящийся не в состоянии оплачен, ошибка
            if (orderDocumentEntity.OrderStatusId != (long)OrderDocumentStatusEnum.Collected)
                return AnswersTryCancelOrder.OrderDocumentNotInCollectedState;

            // Должна быть отправка сообщения в логистику, что нужно отменить заказ 

            throw new NotImplementedException();


            return AnswersTryCancelOrder.Ok;
        }

        public async Task<AnswersTryPayOrder> TryPayOrder(long orderDocumentId)
        {
            // Поиск в базе данных документа заказа. Если его нет, то ошибка
            var orderDocumentEntity = await documentRepository.GetAsync(orderDocumentId);
            if (orderDocumentEntity == null)
                return AnswersTryPayOrder.NotFound;

            // При попытке оплатить заказ, находящийся не в состоянии собран, ошибка
            if (orderDocumentEntity.OrderStatusId != (long)OrderDocumentStatusEnum.Uncollected)
                return AnswersTryPayOrder.OrderDocumentNotInCollectedState;

            // Произвести оплату. Вывести ошибку в случае ошибки оплаты
            var paymentDocumentNumber = await bankingOperations.Pay(
                orderDocumentEntity.CustomerId, orderDocumentEntity.TotalPrice);
            if (paymentDocumentNumber == null)
                return AnswersTryPayOrder.BankingOperationError;

            // Обновить состояние документа заказа
            orderDocumentEntity.PaymentDocumentNumber = paymentDocumentNumber.ToString()!;
            orderDocumentEntity.PaymentDateTime = DateTime.UtcNow;
            orderDocumentEntity.AllSumTransaction = orderDocumentEntity.TotalPrice;
            orderDocumentEntity.OrderStatusId = (long)OrderDocumentStatusEnum.Paid;
            await documentRepository.TryUpdateAsync(new OrderDocument());

            throw new NotImplementedException();

            return AnswersTryPayOrder.Ok;
        }

        public async Task<AnswersTryDeliveryOrder> ChangeDeliveryStatus(
            long orderId, string deliveryStatus)
        {
            // Найти документ по ид. В Случае, если его нет, то сообщить об этом
            var orderDocumentEntity = await documentRepository.GetAsync(orderId);
            if (orderDocumentEntity == null)
                return AnswersTryDeliveryOrder.NotFound;

            // Обновить состояние доставки
            orderDocumentEntity.DeliveryState = deliveryStatus;
            orderDocumentEntity.DeliveryDateTime = DateTime.UtcNow;

            // Если состояние доставки создано, то заказ собран
            if (importantStatuses.Collected.Contains(deliveryStatus))
            {
                orderDocumentEntity.OrderStatusId = (long)OrderDocumentStatusEnum.Collected;
            }
            else if (importantStatuses.Undelivered.Contains(deliveryStatus))
            {
                orderDocumentEntity.OrderStatusId = (long)OrderDocumentStatusEnum.Undelivered;

            }
            else if (importantStatuses.Delivered.Contains(deliveryStatus))
                orderDocumentEntity.OrderStatusId = (long)OrderDocumentStatusEnum.Delivered;


            throw new NotImplementedException();
        }

        public Task<GetLongOrderDocumentDto?> TryGetOrderByOrderId(long orderId)
        {
            return Task.Run(() =>
            {
                var orderDocumentEntity = documentRepository
                    .GetAll()
                    .Where(od => od.Id == orderId)
                    .Include(od => od.OrderItems)
                    .FirstOrDefault();
                return orderDocumentEntity == null ?
                    null :
                    mapper.Map<GetLongOrderDocumentDto>(orderDocumentEntity);
            });
        }

        public Task<List<GetShortOrderDocumentDto>> GetOrdersByUserId(long customerId)
        {
            return Task.Run(() =>
            {
                var orderDocumentEntities = documentRepository
                    .GetAll()
                    .Where(od => od.CustomerId == customerId)
                    .Include(od => od.OrderStatus)
                    .ToList();
                return mapper.Map<List<GetShortOrderDocumentDto>>(orderDocumentEntities);
            });
        }
    }
}
