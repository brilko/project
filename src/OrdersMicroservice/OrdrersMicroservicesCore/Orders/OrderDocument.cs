﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Orders
{
    public class OrderDocument : BaseEntity
    {
        public DateTime CreateDateTime { get; set; } = DateTime.MinValue;

        public long OrderStatusId { get; set; }
        public OrderDocumentStatus? OrderStatus { get; set; }

        /// <summary>
        /// Номер накладной
        /// </summary>
        public string NumberInvoice { get; set; } = Guid.Empty.ToString();

        [MaxLength(50)]
        public string CustomerFullName { get; set; } = string.Empty;

        /// <summary>
        /// Это идентификатор в базе данных, где храняться данные о пользователе.
        /// Эта база данных находится в другом микросервисе.
        /// </summary>
        public long CustomerId { get; set; }

        public string AddresseeFullName { get; set; } = string.Empty;

        public string AdresseePhone { get; set; } = string.Empty;

        [MaxLength(300)]
        public string DeliveryAddress { get; set; } = string.Empty;


        /// <summary>
        /// Навигация по таблице связи многие-ко-многим для товаров в заказе
        /// </summary>
        public List<OrderItem>? OrderItems { get; set; }

        /// <summary>
        /// Полная стоимость за все продукты
        /// </summary>
        public int TotalPrice { get; set; }

        public DateTime? PaymentDateTime { get; set; }

        public string PaymentDocumentNumber { get; set; } = Guid.Empty.ToString();

        /// <summary>
        /// Оплаченная сумма
        /// </summary>
        public int AllSumTransaction { get; set; }

        public string DeliveryState { get; set; } = string.Empty;

        public DateTime? DeliveryDateTime { get; set; }
    }
}
