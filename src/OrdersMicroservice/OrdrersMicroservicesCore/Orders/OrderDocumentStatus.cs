﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Orders
{
    /// <summary>
    /// Статус заказа
    /// </summary>
    public class OrderDocumentStatus : BaseEntity
    {
        [MaxLength(70)]
        public string Name { get; set; } = string.Empty;

        public List<OrderDocument>? Orders { get; set; }
    }
}
