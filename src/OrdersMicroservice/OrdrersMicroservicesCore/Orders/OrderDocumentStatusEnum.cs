﻿namespace BrokenByte.OnlineStore.Domain.Entities.Orders
{
    public enum OrderDocumentStatusEnum
    {
        Collecting = 1,
        Uncollected = 2,
        Collected = 3,
        Cancelled = 4,
        Paid = 5,
        Delivered = 6,
        Undelivered = 7
    }
}