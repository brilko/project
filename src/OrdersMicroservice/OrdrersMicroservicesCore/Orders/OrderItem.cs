﻿namespace BrokenByte.OnlineStore.Domain.Entities.Orders
{
    /// <summary>
    /// Товар в заказе
    /// </summary>
    public class OrderItem : BaseEntity
    {
        /// <summary>
        /// Количество в заказе
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Цена на единицу товара на момент заказа
        /// </summary>
        public int PricePerOne { get; set; }

        /// <summary>
        /// Цена за весь товар 
        /// </summary>
        public int TotalPrice { get; set; }

        public long ProductId { get; set; }

        public long OrderId { get; set; }
        public OrderDocument? Order { get; set; }
    }
}
