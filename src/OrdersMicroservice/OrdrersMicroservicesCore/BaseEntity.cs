﻿namespace BrokenByte.OnlineStore.Domain.Entities
{
    public abstract class BaseEntity
    {
        public long Id { get; set; }

        /// <summary>
        /// Вместо удаления из бд, сущность помечается, как удалённая. 
        /// Периодически база данных очищается от сущностей помеченных на удаление.
        /// Такой подход позволяет повысить производительность СУБД.
        /// </summary>
        public bool Deleted { get; set; } = false;
    }
}