﻿namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions
{
    public class Page
    {
        public int PageIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }
}
