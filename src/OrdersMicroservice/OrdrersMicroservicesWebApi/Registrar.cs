﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using OrdersMicroserviceBankingOperations;
using OrdersMicroserviceBankingOperationsContracts;
using OrdersMicroservicesDataAccess;
using OrdersMicroservicesDataBase;
using OrdersMicroserviceServiceContracts;
using OrdersMicroserviceServiceContracts.ConfigureParameters;
using OrdersMicroserviceServices;
using OrdrersMicroservicesWebApi.MapperProfiles;

namespace OrdrersMicroservicesWebApi
{
    public static class Registrar
    {
        public static IServiceCollection AddMappersExtension(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProfileOrderDocument>();
                cfg.AddProfile<ProfileOrderItem>();
                cfg.AddProfile<ProfileOrderDocumentStatus>();
            });

            configuration.AssertConfigurationIsValid();

            services.AddSingleton<IMapper>(new Mapper(configuration));

            return services;
        }

        public static IServiceCollection AddBusinessServicesExtension(
            this IServiceCollection services)
        {
            return services
                .AddScoped<IOrderService, OrderService>();
        }

        private static T GetExtension<T>(
            this ConfigurationManager configuration,
            string sectionName)
        {
            IConfigurationSection section = configuration.GetSection(sectionName)
                ?? throw new Exception();
            T value = section.Get<T>()
                ?? throw new Exception();
            return value!;
        }

        public static IServiceCollection AddConfigureParameters(
            this IServiceCollection services, ConfigurationManager configuration)
        {
            ImportantStatusesOfDeliveryDto statuses = configuration
                .GetExtension<ImportantStatusesOfDeliveryDto>("ImportantStatesOfDelivery");
            WaitingTimeForPaymentDto payWaitingMinutes = new (configuration
                .GetExtension<int>("WaitingTimeForPayment"));
            return services
                .AddScoped(_ => statuses)
                .AddScoped(_ => payWaitingMinutes);
        }

        public static IServiceCollection AddDataBaseExtension(
            this IServiceCollection services, ConfigurationManager configuration)
        {
            var connectionString = configuration.GetConnectionString("PostGreSQL") ??
                    throw new KeyNotFoundException("Нужна база данных и строка подключения, " +
                        "которая обязательно должна быть в пользовательских секретах. Например," +
                        "Host=localhost;Port=5433;Database=ClothesStoreOrdersMicroservices;Username=postgres;Password=admin");

            return services
                .AddDbContext<ApplicationContext>(opt => opt.UseNpgsql(connectionString))
                .AddScoped(typeof(IRepository<>), typeof(PostGreRepository<>));
        }

        public static IServiceCollection AddInfrastructureExtension(
            this IServiceCollection services)
        {
            return services
                .AddScoped<IBankingOperations, FakeBankingOperations>();
        }

        public static void RecreateDataBaseExtension(this WebApplication app)
        {
            using var scope = app.Services.CreateScope();
            var db = scope.ServiceProvider.GetRequiredService<ApplicationContext>();
            db!.Database.EnsureDeleted();
            db!.Database.EnsureCreated();
        }
    }
}
