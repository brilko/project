﻿namespace OrdrersMicroservicesWebApi.Models
{
    public class GetLongOrderDocumentModel
    {
        public long Id { get; set; }

        public DateTime CreateDateTime { get; set; } = DateTime.MinValue;

        public string OrderStatus { get; set; } = string.Empty;

        /// <summary>
        /// Номер накладной
        /// </summary>
        public string NumberInvoice { get; set; } = Guid.Empty.ToString();

        public string CustomerFullName { get; set; } = string.Empty;

        /// <summary>
        /// Это идентификатор в базе данных, где храняться данные о пользователе.
        /// Эта база данных находится в другом микросервисе.
        /// </summary>
        public long CustomerId { get; set; }

        public string AddresseeFullName { get; set; } = string.Empty;

        public string AdresseePhone { get; set; } = string.Empty;

        public string DeliveryAddress { get; set; } = string.Empty;

        public List<GetOrderItemModel> OrderItems { get; set; } = [];

        public int TotalPrice { get; set; }

        public DateTime? PaymentDateTime { get; set; }

        public string PaymentDocumentNumber { get; set; } = Guid.Empty.ToString();

        /// <summary>
        /// Оплаченная сумма
        /// </summary>
        public int AllSumTransaction { get; set; }

        public DateTime? DeliveryDateTime { get; set; }
    }
}
