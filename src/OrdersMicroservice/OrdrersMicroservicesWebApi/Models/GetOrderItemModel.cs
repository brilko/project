﻿namespace OrdrersMicroservicesWebApi.Models
{
    public class GetOrderItemModel
    {
        public int Quantity { get; set; }

        public int PricePerOne { get; set; }

        public long ProductId { get; set; }
    }
}
