﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OrdersMicroserviceServiceContracts;
using OrdersMicroserviceServiceContracts.DTOs;
using OrdrersMicroservicesWebApi.Models;

namespace BrokenByte.OnlineStore.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrdersController(IOrderService orderService, IMapper mapper) : ControllerBase
    {
        private readonly IOrderService orderDocumentService = orderService;
        private readonly IMapper mapper = mapper;

        /// <summary>
        /// Создаёт новый заказ
        /// </summary>
        /// <returns>Id созданного заказа</returns>
        [HttpPost]
        public async Task<IActionResult> CreateOrder(CreateOrderDocumetModel createOrderModel)
        {
            var createOrderDto = mapper.Map<CreateOrderDocumentDto>(createOrderModel);
            var newId = await orderDocumentService.CreateOrder(createOrderDto);
            return Ok(newId);
        }

        /// <summary>
        /// Отменить доставку собранного заказа
        /// </summary>
        /// <param name="orderId">Id собранного заказа</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPut("cancel/{orderId}")]
        public async Task<IActionResult> CancelOrder(long orderId)
        {
            var answer = await orderDocumentService.TryCancelOrder(orderId);
            return answer switch
            {
                IOrderService.AnswersTryCancelOrder.NotFound => NotFound(),
                IOrderService.AnswersTryCancelOrder.OrderDocumentNotInCollectedState
                    => Conflict("Документ заказа не в состоянии собран"),
                IOrderService.AnswersTryCancelOrder.Ok => Ok(),
                _ => throw new NotImplementedException(),
            };
        }

        /// <summary>
        /// Устанавливает состояние сформированного заказа с заданным id оплачено.
        /// </summary>
        /// <param name="orderId">Id сформированного заказа</param>
        /// <returns></returns>
        [HttpPut("payments/{orderId}")]
        public async Task<IActionResult> PayOrder(long orderId)
        {
            var answer = await orderDocumentService.TryPayOrder(orderId);
            return answer switch
            {
                IOrderService.AnswersTryPayOrder.Ok => Ok(),
                IOrderService.AnswersTryPayOrder.BankingOperationError
                    => Conflict("Банковская ошибка"),
                IOrderService.AnswersTryPayOrder.OrderDocumentNotInCollectedState
                    => Conflict("Документ заказа не в состоянии собран"),
                IOrderService.AnswersTryPayOrder.NotFound => NotFound(),
                _ => throw new NotImplementedException(),
            };
        }

        /// <summary>
        /// Установить статус доставки оплаченного заказа.
        /// </summary>
        /// <param name="orderId">Id оплаченного документа</param>
        /// <param name="isDelivered"></param>
        /// <returns></returns>
        [HttpPut("deliveries/{orderId}/{deliveryStatus}")]
        public async Task<IActionResult> DeliveryOrder(long orderId, string deliveryStatus)
        {
            var answer = await orderDocumentService.ChangeDeliveryStatus(orderId, deliveryStatus);
            return answer switch
            {
                IOrderService.AnswersTryDeliveryOrder.Ok => Ok(),
                IOrderService.AnswersTryDeliveryOrder.NotFound => NotFound(),
                _ => throw new NotImplementedException(),
            };
        }

        /// <summary>
        /// Получить все данные о заказах по его Id.
        /// </summary>
        /// <param name="orderId">id заказа</param>
        /// <returns></returns>
        [HttpGet("{orderId}")]
        public async Task<IActionResult> GetOrderByOrderId(long orderId)
        {
            var orderDocumentDto = await orderDocumentService.TryGetOrderByOrderId(orderId);
            if (orderDocumentDto == null)
                return NotFound();
            var model = mapper.Map<GetLongOrderDocumentModel>(orderDocumentDto);
            return Ok(model);
        }

        /// <summary>
        /// Получить список заказов клиента
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("UserId/{userId}")]
        public async Task<IActionResult> GetOrdersByUserId(long userId)
        {
            var dtos = await orderDocumentService.GetOrdersByUserId(userId);
            var models = mapper.Map<List<GetShortOrderDocumentModel>>(dtos);
            return Ok(models);
        }
    }
}
