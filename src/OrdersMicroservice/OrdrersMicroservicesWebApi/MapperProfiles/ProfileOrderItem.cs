﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using OrdersMicroserviceServiceContracts.DTOs;
using OrdrersMicroservicesWebApi.Models;

namespace OrdrersMicroservicesWebApi.MapperProfiles
{
    public class ProfileOrderItem : Profile
    {
        public ProfileOrderItem()
        {
            #region ModelDto
            CreateMap<CreateOrderItemModel, CreateOrderItemDto>();
            CreateMap<GetOrderItemDto, GetOrderItemModel>();
            #endregion ModelDto

            #region DtoEntity
            CreateMap<CreateOrderItemDto, OrderItem>()
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.Deleted, opt => opt.MapFrom(dto => false))
                .ForMember(d => d.TotalPrice,
                    opt => opt.MapFrom(dto => dto.Quantity * dto.PricePerOne))
                .ForMember(d => d.OrderId, opt => opt.Ignore())
                .ForMember(d => d.Order, opt => opt.Ignore());

            CreateMap<OrderItem, GetOrderItemDto>();
            #endregion DtoEntity
        }
    }
}
