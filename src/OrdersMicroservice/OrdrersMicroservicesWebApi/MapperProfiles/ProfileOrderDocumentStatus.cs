﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using OrdersMicroserviceServiceContracts.DTOs;

namespace OrdrersMicroservicesWebApi.MapperProfiles
{
    public class ProfileOrderDocumentStatus : Profile
    {
        public ProfileOrderDocumentStatus()
        {
            CreateMap<OrderDocumentStatus, OrderDocumentStatusCasheDto>();
        }
    }
}
