﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using OrdersMicroserviceServiceContracts.DTOs;
using OrdrersMicroservicesWebApi.Models;

namespace OrdrersMicroservicesWebApi.MapperProfiles
{
    public class ProfileOrderDocument : Profile
    {
        public ProfileOrderDocument()
        {
            #region ModelDto
            CreateMap<CreateOrderDocumetModel, CreateOrderDocumentDto>();
            CreateMap<GetLongOrderDocumentDto, GetLongOrderDocumentModel>();
            CreateMap<GetShortOrderDocumentDto, GetShortOrderDocumentModel>();
            #endregion ModelDto

            #region DtoEntity
            CreateMap<CreateOrderDocumentDto, OrderDocument>()
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.Deleted, opt => opt.MapFrom(dto => false))
                .ForMember(d => d.OrderStatusId, opt => opt.Ignore())
                .ForMember(d => d.OrderStatus, opt => opt.Ignore())
                .ForMember(d => d.PaymentDateTime, opt => opt.Ignore())
                .ForMember(d => d.PaymentDocumentNumber, opt => opt.Ignore())
                .ForMember(d => d.AllSumTransaction, opt => opt.Ignore())
                .ForMember(d => d.DeliveryState, opt => opt.Ignore())
                .ForMember(d => d.DeliveryDateTime, opt => opt.Ignore());

            CreateMap<OrderDocument, GetLongOrderDocumentDto>();
            CreateMap<OrderDocument, GetShortOrderDocumentDto>()
                .ForMember(d => d.OrderStatus, opt => opt.MapFrom(od => od.OrderStatus!.Name));
            #endregion DtoEntity
        }
    }
}
