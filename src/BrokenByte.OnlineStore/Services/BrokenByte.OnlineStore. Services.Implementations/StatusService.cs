﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;

namespace BrokenByte.OnlineStore._Services.Implementations
{

    public class StatusService<TDto, T> : IStatusService<TDto> where T : BaseEntity
    {
        protected readonly IMapper _mapper;
        protected readonly IReadRepository<T> _repository;

        public StatusService(IMapper mapper, IReadRepository<T> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        Task<TDto> IStatusService<TDto>.GetById(long id)
        {
            var entity = _repository.GetAsync(id);
            return Task.FromResult(_mapper.Map<T?, TDto>(entity.Result));
        }

        public Task<ICollection<TDto>> GetAll()
        {
            ICollection<T> entities = _repository.GetAll().ToList();
            return Task.FromResult(_mapper.Map<ICollection<T>, ICollection<TDto>>(entities));
        }
    }
}
