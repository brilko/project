﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using BrokenByte.OnlineStore.Services.Contracts.Product;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;

namespace BrokenByte.OnlineStore._Services.Implementations
{
    public class ProductService : IProductService
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;
        private readonly IProductPriceRepository _productPriceRepository;
        private readonly IStorageItemRepository _storageItemsRepository;


        public ProductService(
            IMapper mapper,
            IProductRepository productRepository,
            IProductPriceRepository productPriceRepository,
            IStorageItemRepository storageItemsRepository
           )
        {
            _mapper = mapper;
            _productRepository = productRepository;
            _productPriceRepository = productPriceRepository;
            _storageItemsRepository = storageItemsRepository;
        }
        public async Task<long> Create(CreatingProductDto creatingDto)
        {
            var entity = _mapper.Map<CreatingProductDto, Product>(creatingDto);
            var createdEntity = await _productRepository.AddAsync(entity);
            await _productRepository.SaveChangesAsync();
            return createdEntity.Id;
        }

        public async Task Delete(long id)
        {
            var entity = await _productRepository.GetAsync(id)
                ?? throw new Exception();
            entity.Deleted = true;
            await _productRepository.SaveChangesAsync();

        }

        public async Task<ProductDto> GetById(long id)
        {
            var entity = await _productRepository.GetAsync(id)
                ?? throw new Exception("Не  найден товар");
            var value = _mapper.Map<Product?, ProductDto>(entity);
            var price = await _productPriceRepository.GetCurrentPrice(entity.Id)
                ?? throw new Exception("Не  задана цена");
            value.Price = price.Price;
            value.AvialableCount = GetAvailableCount(id);
            value.Manufacturer = entity.Manufacturer?.Name;
            value.Category = entity.Category?.Name;
            return value;
        }

        private int GetAvailableCount(long id)
        {
            int quantity = _storageItemsRepository
                .GetAll()
                .Where(c => !c.Deleted && c.ProductId == id)
                .Sum(p => p.Quantity);
            int reserved = _storageItemsRepository
                .GetAll()
                .Where(c => !c.Deleted && c.ProductId == id)
                .Sum(p => p.Reserved);

            return quantity - reserved;
        }

        public async Task<ICollection<ProductDto>> GetPaged(ProductFilterDto filterDto)
        {
            ICollection<Product> entities = await _productRepository.GetPagedAsync(filterDto);
            var list = _mapper.Map<ICollection<Product>, ICollection<ProductDto>>(entities);
            foreach (var item in list)
            {
                var price = await _productPriceRepository.GetCurrentPrice(item.Id)
                    ?? throw new Exception();
                item.Price = price.Price;
                item.AvialableCount = GetAvailableCount(item.Id);
            }
            return list;
        }

        public async Task Update(long id, UpdatingProductDto updatingDto)
        {
            _ = await _productRepository.GetAsync(id)
                ?? throw new Exception($"Сущность с идентфикатором {id} не найдена");
            var entity = _mapper.Map<UpdatingProductDto, Product>(updatingDto);
            await _productRepository.UpdateAsync(entity);
            await _productRepository.SaveChangesAsync();
        }
    }
}
