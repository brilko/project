﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.ProductInCart;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Customers;

namespace BrokenByte.OnlineStore._Services.Implementations
{
    public class CartService : ICartService
    {
        private readonly IMapper mapper;
        private readonly IProductInCartRepository productsInCartRepository;
        private readonly IRepository<Product> productsRepository;
        private readonly IRepository<Customer> customersRepository;

        public CartService(IMapper mapper,
                           IProductInCartRepository productsInCartRepository,
                           IRepository<Product> productsRepository,
                           IRepository<Customer> customersRepository)
        {
            this.mapper = mapper;
            this.productsInCartRepository = productsInCartRepository;
            this.productsRepository = productsRepository;
            this.customersRepository = customersRepository;
        }


        public async Task<bool> TryChangeProductCountInCartAsync(ProductInCartUpdateDto updateModel)
        {
            var productInCart = await productsInCartRepository
                .GetByProductIdAndCustomerId(updateModel.ProductId, updateModel.CustomerId);

            if (productInCart != null)
            {
                if (updateModel.Count == 0)
                {
                    await productsInCartRepository.TryDeleteAsync(productInCart);
                    return true;
                }

                productInCart.Count = updateModel.Count;
                await productsInCartRepository.UpdateAsync(productInCart);
                return true;
            }
            if (!await customersRepository.IsExist(updateModel.CustomerId) ||
                !await productsRepository.IsExist(updateModel.ProductId))
                return false;

            var productInCartEntity = mapper.Map<ProductInCart>(updateModel);

            await productsInCartRepository.AddAsync(productInCartEntity);
            return true;
        }

        public async Task<List<ProductInCartDto>> GetListbyIdAsync(long customerId)
        {
            var productsInUsersCart = await productsInCartRepository
                .GetProductsInCustomersCartIncludePrices(customerId);

            var productsInUsersCartModels = mapper.Map<List<ProductInCartDto>>(productsInUsersCart);

            return productsInUsersCartModels;
        }
    }
}
