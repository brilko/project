﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;

namespace BrokenByte.OnlineStore._Services.Implementations
{
    /// <summary>
    /// Cервис работы с производителями.
    /// </summary>
    public class ManufacturerService : IManufacturerService
    {
        private readonly IMapper _mapper;
        private readonly IManufacturerRepository _manufacturerRepository;

        public ManufacturerService(
            IMapper mapper,
            IManufacturerRepository manufacturerRepository
           )
        {
            _mapper = mapper;
            _manufacturerRepository = manufacturerRepository;
        }

        /// <summary>
        /// Получить производителя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО производителя. </returns>
        public async Task<ManufacturerDto> GetById(long id)
        {
            var entity = await _manufacturerRepository.GetAsync(id);
            return _mapper.Map<Manufacturer?, ManufacturerDto>(entity);
        }

        /// <summary>
        /// Создать производителя.
        /// </summary>
        /// <param name="creatingManufacturerDto"> ДТО производителя. </param>
        /// <returns> Идентификатор. </returns>
        public async Task<long> Create(CreatingManufacturerDto creatingManufacturerDto)
        {
            var manufacturer = _mapper.Map<CreatingManufacturerDto, Manufacturer>(creatingManufacturerDto);
            var createdManufactarer = await _manufacturerRepository.AddAsync(manufacturer);
            await _manufacturerRepository.SaveChangesAsync();
            return createdManufactarer.Id;
        }

        /// <summary>
        /// Изменить производителя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <param name="updatingDto"> ДТО редактируемого производителя. </param>
        public async Task Update(long id, UpdatingManufacturerDto updatingDto)
        {
            var entity = await _manufacturerRepository.GetAsync(id)
                ?? throw new Exception($"Сущность с идентфикатором {id} не найдена");
            entity.Name = updatingDto.Name;
            entity.Image = updatingDto.Image;
            await _manufacturerRepository.UpdateAsync(entity);
            await _manufacturerRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить производителя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        public async Task Delete(long id)
        {
            var entity = await _manufacturerRepository.GetAsync(id)
                ?? throw new Exception();
            entity.Deleted = true;
            await _manufacturerRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список производителей. </returns>
        public async Task<ICollection<ManufacturerDto>> GetPaged(ManufacturerFilterDto filterDto)
        {
            ICollection<Manufacturer> entities = await _manufacturerRepository.GetPagedAsync(filterDto);
            return _mapper.Map<ICollection<Manufacturer>, ICollection<ManufacturerDto>>(entities);
        }
    }
}
