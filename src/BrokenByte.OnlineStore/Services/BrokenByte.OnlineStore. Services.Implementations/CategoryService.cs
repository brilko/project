﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.Category;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;

namespace BrokenByte.OnlineStore._Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly IMapper _mapper;
        private readonly ICategoryRepository _repository;

        public CategoryService(
            IMapper mapper,
            ICategoryRepository repository
           )
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<long> Create(CreatingCategoryDto creatingCategoryDto)
        {
            var entity = _mapper.Map<CreatingCategoryDto, Category>(creatingCategoryDto);
            var createdEntity = await _repository.AddAsync(entity);
            await _repository.SaveChangesAsync();
            return createdEntity.Id;
        }

        public async Task Delete(long id)
        {
            var entity = await _repository.GetAsync(id)
                ?? throw new Exception();
            entity.Deleted = true;
            await _repository.SaveChangesAsync();
        }

        public async Task<CategoryDto> GetById(long id)
        {
            var entity = await _repository.GetAsync(id);
            return _mapper.Map<Category?, CategoryDto>(entity);
        }

        public async Task<ICollection<CategoryDto>> GetPaged(CategoryFilterDto filterDto)
        {
            ICollection<Category> entities = await _repository.GetPagedAsync(filterDto);
            return _mapper.Map<ICollection<Category>, ICollection<CategoryDto>>(entities);
        }

        public async Task Update(long id, UpdatingCategoryDto updatingCategoryDto)
        {
            _ = await _repository.GetAsync(id)
                ?? throw new Exception($"Сущность с идентфикатором {id} не найдена");
            var entity = _mapper.Map<UpdatingCategoryDto, Category>(updatingCategoryDto);
            await _repository.UpdateAsync(entity);
            await _repository.SaveChangesAsync();
        }
    }
}
