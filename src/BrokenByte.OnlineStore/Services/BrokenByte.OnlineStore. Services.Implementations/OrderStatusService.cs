﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.OrderStatus;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Order;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore._Services.Implementations
{
    public class OrderStatusService : StatusService<OrderStatusDto, OrderStatus>, IOrderStatusService
    {

        public OrderStatusService(IMapper mapper, IReadOrderStatusRepository repository) : base(mapper, repository)
        {

        }

        public async Task<List<OrderStatus>> GetFilteredAsync(OrderStatusFilterDto filter)
        {
            var entities = _repository.GetAll();
            if (filter.Id != 0)
                entities = entities.Where(e => e.Id == filter.Id);
            if (filter.Name != string.Empty)
                entities = entities.Where(e => e.Name == filter.Name);
            if (filter.Description != string.Empty)
                entities = entities.Where(e => e.Description == filter.Description);
            entities = entities
                .Skip(filter.PageIndex * filter.ItemsPerPage)
                .Take(filter.ItemsPerPage);

            var naturalized = await entities.ToListAsync();
            return naturalized;
        }
    }
}
