﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Services.Contracts.OrderStatus;

namespace BrokenByte.OnlineStore.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности статус продукта.
    /// </summary>
    public class OrderStatusMappingsProfile : Profile
    {
        public OrderStatusMappingsProfile()
        {
            CreateMap<OrderStatus, OrderStatusDto>();
        }

    }
}