﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.ProductPrice;

namespace BrokenByte.OnlineStore.Services.Implementations.Mapping
{

    /// <summary>
    /// Профиль автомаппера для сущности цена продукта.
    /// </summary>
    public class ProductPriceMappingsProfile : Profile
    {
        public ProductPriceMappingsProfile()
        {
            CreateMap<ProductPriceDto, ProductPrice>()
                .ForMember(d => d.Deleted, map => map.Ignore())
                .ForMember(d => d.Product, map => map.Ignore())
                .ForMember(d => d.ProductId, map => map.Ignore())
                .ReverseMap();

            CreateMap<CreatingProductPriceDto, ProductPrice>()
               .ForMember(d => d.Id, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore())
                .ForMember(d => d.Product, map => map.Ignore());
        }

    }
}
