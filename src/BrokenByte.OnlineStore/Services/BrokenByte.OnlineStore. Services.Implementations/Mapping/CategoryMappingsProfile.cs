﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.Category;

namespace BrokenByte.OnlineStore.Services.Implementations.Mapping
{
    public class CategoryMappingsProfile : Profile
    {
        public CategoryMappingsProfile()
        {
            CreateMap<CategoryDto, Category>()
               .ForMember(d => d.Products, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore()).ReverseMap();

            CreateMap<CreatingCategoryDto, Category>()
               .ForMember(d => d.Id, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore())
               .ForMember(d => d.Products, map => map.Ignore());

            CreateMap<UpdatingCategoryDto, Category>()
               .ForMember(d => d.Id, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore())
               .ForMember(d => d.Products, map => map.Ignore());
        }
    }
}
