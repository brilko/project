﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using BrokenByte.OnlineStore.Services.Contracts.Product;

namespace BrokenByte.OnlineStore.Services.Implementations.Mapping
{
    public class ProductMappingsProfile : Profile
    {
        public ProductMappingsProfile()
        {
            CreateMap<ProductDto, Product>()
                .ForMember(d => d.Prices, map => map.Ignore())
                .ForMember(d => d.StorageItems, map => map.Ignore())
                .ForMember(d => d.Manufacturer, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore())
                .ForMember(d => d.Category, map => map.Ignore())
                .ForMember(d => d.CustomersWithProductInCart, map => map.Ignore())
                .ForMember(d => d.CustomersWithProductInWishList, map => map.Ignore())
                .ForMember(d => d.OrderItems, map => map.Ignore())
                .ForMember(d => d.Orders, map => map.Ignore())
                .ForMember(d => d.Images, map => map.Ignore()).ReverseMap();

            CreateMap<CreatingProductDto, Product>()
               .ForMember(d => d.Id, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore())
               .ForMember(d => d.Prices, map => map.Ignore())
               .ForMember(d => d.StorageItems, map => map.Ignore())
               .ForMember(d => d.Images, map => map.Ignore())
               .ForMember(d => d.Manufacturer, map => map.Ignore())
               .ForMember(d => d.StorageItems, map => map.Ignore())
               .ForMember(d => d.Category, map => map.Ignore())
               .ForMember(d => d.CustomersWithProductInCart, map => map.Ignore())
               .ForMember(d => d.CustomersWithProductInWishList, map => map.Ignore())
               .ForMember(d => d.Orders, map => map.Ignore())
               .ForMember(d => d.OrderItems, map => map.Ignore());

            CreateMap<UpdatingProductDto, Product>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore())
                .ForMember(d => d.Prices, map => map.Ignore())
                .ForMember(d => d.StorageItems, map => map.Ignore())
                .ForMember(d => d.Images, map => map.Ignore())
                .ForMember(d => d.Manufacturer, map => map.Ignore())
                .ForMember(d => d.StorageItems, map => map.Ignore())
                .ForMember(d => d.Category, map => map.Ignore())
                .ForMember(d => d.CustomersWithProductInCart, map => map.Ignore())
                .ForMember(d => d.CustomersWithProductInWishList, map => map.Ignore())
                .ForMember(d => d.Orders, map => map.Ignore())
                .ForMember(d => d.OrderItems, map => map.Ignore());
        }
    }
}
