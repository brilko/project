﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;

namespace BrokenByte.OnlineStore.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности производителя.
    /// </summary>
    public class ManufacturerMappingsProfile : Profile
    {
        public ManufacturerMappingsProfile()
        {

            CreateMap<ManufacturerDto, Manufacturer>()
               .ForMember(d => d.Products, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore()).ReverseMap();

            CreateMap<CreatingManufacturerDto, Manufacturer>()
               .ForMember(d => d.Id, map => map.Ignore())
               .ForMember(d => d.Deleted, map => map.Ignore())
                .ForMember(d => d.Products, map => map.Ignore());

            CreateMap<UpdatingManufacturerDto, Manufacturer>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore())
                .ForMember(d => d.Products, map => map.Ignore());
        }

    }
}