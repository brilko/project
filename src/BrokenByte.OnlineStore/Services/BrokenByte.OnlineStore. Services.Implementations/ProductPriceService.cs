﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.ProductPrice;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;

namespace BrokenByte.OnlineStore._Services.Implementations
{
    public class ProductPriceService : IProductPriceService
    {
        private readonly IMapper _mapper;
        private readonly IProductPriceRepository _repository;

        public ProductPriceService(
            IMapper mapper,
            IProductPriceRepository repository
           )
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task Delete(long id)
        {
            var entity = await _repository.GetAsync(id)
                ?? throw new Exception();
            entity.Deleted = true;
            await _repository.SaveChangesAsync();
        }


        /// <summary>
        /// Создать цену. Новая цена автоматически отменяет все последующие по времени
        /// </summary>
        /// <param name="creatingDto"> ДТО создаваемой сущности. </param>
        public async Task<long> Create(CreatingProductPriceDto creatingDto)
        {
            if (creatingDto.DateBegin == null)
            {
                creatingDto.DateBegin = DateTime.Now;
            }
            var entity = _mapper.Map<CreatingProductPriceDto, ProductPrice>(creatingDto);

            //Удалить все с датой установленной раньше времени
            ICollection<ProductPrice> entities = await _repository.GetAllAsync(creatingDto.ProductId, creatingDto.DateBegin);
            await _repository.TryDeleteRangeAsync(entities);

            var createdEntity = await _repository.AddAsync(entity);

            await _repository.SaveChangesAsync();
            return createdEntity.Id;
        }

        public async Task<ICollection<ProductPriceDto>> GetPaged(ProductPriceFilterDto filterDto)
        {
            ICollection<ProductPrice> entities = await _repository.GetPagedAsync(filterDto);
            return _mapper.Map<ICollection<ProductPrice>, ICollection<ProductPriceDto>>(entities);

        }

        public async Task<ICollection<ProductPriceDto>> GetAllAsync(long productId)
        {
            ICollection<ProductPrice> entities = await _repository.GetAllAsync(productId, null);
            return _mapper.Map<ICollection<ProductPrice>, ICollection<ProductPriceDto>>(entities);
        }


        public async Task<ProductPriceDto?> Get(long id)
        {
            ProductPrice? entitie = await _repository.GetAsync(id);
            return _mapper.Map<ProductPrice?, ProductPriceDto>(entitie);
        }
    }
}
