﻿using BrokenByte.OnlineStore.Domain.Entities.Orders;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions.Order
{

    public interface IReadOrderStatusRepository : IReadRepository<OrderStatus>
    {
    }
}
