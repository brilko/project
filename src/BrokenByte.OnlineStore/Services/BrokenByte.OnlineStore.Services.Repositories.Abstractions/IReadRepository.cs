﻿using BrokenByte.OnlineStore.Domain.Entities;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions
{
    /// <summary>
    /// Интерфейс репозитория, предназначенного для чтения.
    /// </summary>
    /// <typeparam name="T"> Тип Entity для репозитория. </typeparam>
    public interface IReadRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Запросить все сущности в базе.
        /// </summary>
        /// <param name="noTracking"> Вызвать с AsNoTracking.</param>
        /// <returns> IQueryable массив сущностей.</returns>
        IQueryable<T> GetAll(bool noTracking = false);

        /// <summary>
        /// Запросить все сущности в базе.
        /// </summary>
        /// <param name="cancellationToken"> Токен отмены. </param>
        /// <param name="asNoTracking"> Вызвать с AsNoTracking. </param>
        /// <returns> Список сущностей. </returns>
        Task<List<T>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false);

        /// <summary>
        /// Получить сущность по Id.
        /// </summary>
        /// <param name="id"> Id сущности. </param>
        /// <returns> Cущность. </returns>
        Task<T?> GetAsync(long id);

        /// <summary>
        /// Запросить страницу из таблицы в базе данных
        /// </summary>
        /// <param name="page">Описание страницы. 
        /// Есть индекс страницы и количество сущностей на страницу</param>
        /// <returns> Список найденных сущностей. </returns>
        Task<List<T>> GetPageAsync(Page page);
    }
}
