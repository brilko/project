﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.ProductPrice;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products
{
    public interface IProductPriceRepository : IRepository<ProductPrice>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список . </returns>
        Task<List<ProductPrice>> GetPagedAsync(ProductPriceFilterDto filterDto);

        /// <summary>
        /// Получить все цены на товар, можно с датой применения с указанной
        /// </summary>
        /// <param name="productId">ид товара</param>
        /// <param name="dt">дата применения</param>
        /// <returns>Список цен</returns>
        Task<ICollection<ProductPrice>> GetAllAsync(long productId, DateTime? dt);

        /// <summary>
        /// Получить текущую цену на товар
        /// </summary>
        /// <param name="productId">ид товара</param>
        /// <returns>ткущая цена</returns>
        Task<ProductPrice?> GetCurrentPrice(long productId);
    }
}
