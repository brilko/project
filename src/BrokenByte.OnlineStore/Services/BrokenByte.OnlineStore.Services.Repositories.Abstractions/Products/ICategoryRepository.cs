﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.Category;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products
{
    public interface ICategoryRepository : IRepository<Category>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список. </returns>
        Task<List<Category>> GetPagedAsync(CategoryFilterDto filterDto);
    }
}
