﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.Product;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products
{
    public interface IProductRepository : IRepository<Product>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список. </returns>
        Task<List<Product>> GetPagedAsync(ProductFilterDto filterDto);
    }
}
