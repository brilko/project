﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products
{
    public interface IManufacturerRepository : IRepository<Manufacturer>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список производителей. </returns>
        Task<List<Manufacturer>> GetPagedAsync(ManufacturerFilterDto filterDto);
    }
}
