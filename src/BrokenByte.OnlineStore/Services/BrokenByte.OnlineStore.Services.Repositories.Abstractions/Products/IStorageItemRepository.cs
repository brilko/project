﻿using BrokenByte.OnlineStore.Domain.Entities.Storages;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products
{
    public interface IStorageItemRepository : IRepository<StorageItem>
    {

    }
}
