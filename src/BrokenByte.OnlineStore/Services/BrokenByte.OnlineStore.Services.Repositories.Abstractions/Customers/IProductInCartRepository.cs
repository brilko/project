﻿using BrokenByte.OnlineStore.Domain.Entities.Customers;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions.Customers
{
    public interface IProductInCartRepository : IRepository<ProductInCart>
    {
        /// <summary>
        /// Получить этот продукт из корзины этого пользователя. Если такого нет, то null.
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        Task<ProductInCart?> GetByProductIdAndCustomerId(long ProductId, long CustomerId);

        /// <summary>
        /// Получить список товаров в корзине пользователя.
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        Task<List<ProductInCart>> GetProductsInCustomersCartIncludePrices(long CustomerId);
    }
}
