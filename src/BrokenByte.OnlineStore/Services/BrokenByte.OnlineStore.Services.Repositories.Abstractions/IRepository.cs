﻿using BrokenByte.OnlineStore.Domain.Entities;

namespace BrokenByte.OnlineStore.Services.Repositories.Abstractions
{
    /// <summary>
    /// Описания общих методов для всех репозиториев.
    /// </summary>
    /// <typeparam name="T"> Тип Entity для репозитория. </typeparam>
    public interface IRepository<T> : IReadRepository<T>
        where T : BaseEntity
    {
        #region Delete
        /// <summary>
        /// Асинхронно удалить сущность.
        /// </summary>
        /// <param name="id"> Id удалённой сущности. </param>
        /// <returns> Была ли сущность удалена. </returns>
        Task<bool> TryDeleteAsync(long id);

        /// <summary>
        /// Асинхронно удалить сущность.
        /// </summary>
        /// <param name="entity"> Cущность для удаления. </param>
        /// <returns> Была ли сущность удалена. </returns>
        Task<bool> TryDeleteAsync(T entity);

        /// <summary>
        /// Асинхронно удалить сущности.
        /// </summary>
        /// <param name="entities"> Коллекция сущностей для удаления. </param>
        /// <returns> Была ли операция удаления успешна. </returns>
        Task<bool> TryDeleteRangeAsync(ICollection<T> entities);
        #endregion

        #region Update

        /// <summary>
        /// Для сущности асинхронно проставить состояние - что она изменена.
        /// </summary>
        /// <param name="entity"> Сущность для изменения. </param>
        Task<bool> UpdateAsync(T entity);
        #endregion

        #region Add
        /// <summary>
        /// Добавить в базу одну сущность.
        /// </summary>
        /// <param name="entity"> Сущность для добавления. </param>
        /// <returns> Добавленная сущность. </returns>
        Task<T> AddAsync(T entity);

        /// <summary>
        /// Добавить в базу массив сущностей.
        /// </summary>
        /// <param name="entities"> Массив сущностей. </param>
        Task AddRangeAsync(ICollection<T> entities);
        #endregion

        #region SaveChanges
        /// <summary>
        /// Сохранить изменения.
        /// </summary>
        Task SaveChangesAsync(CancellationToken cancellationToken = default);
        #endregion

        #region IsExist
        /// <summary>
        /// Проверка на существование 
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Возвращает true, если объект с этим id есть в базе данных, иначе false</returns>
        Task<bool> IsExist(long id);
        #endregion
    }
}