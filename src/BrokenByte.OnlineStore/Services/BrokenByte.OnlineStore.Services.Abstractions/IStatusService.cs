﻿namespace BrokenByte.OnlineStore.Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы о статусами
    /// </summary>
    public interface IStatusService<TDto>
    {
        // <summary>
        /// Получить статус.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО статуса. </returns>
        Task<TDto> GetById(long id);


        /// <summary>
        /// Получить все статусы
        /// </summary>
        /// <returns> Список статусов. </returns>
        Task<ICollection<TDto>> GetAll();
    }
}