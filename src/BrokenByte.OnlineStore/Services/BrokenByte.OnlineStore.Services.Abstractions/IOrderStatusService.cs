﻿using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Services.Contracts.OrderStatus;

namespace BrokenByte.OnlineStore.Services.Abstractions
{
    public interface IOrderStatusService : IStatusService<OrderStatusDto>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <returns> Список производителей. </returns>
        Task<List<OrderStatus>> GetFilteredAsync(OrderStatusFilterDto filter);
    }
}
