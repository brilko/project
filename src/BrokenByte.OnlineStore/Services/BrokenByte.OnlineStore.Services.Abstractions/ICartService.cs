﻿using BrokenByte.OnlineStore.Services.Contracts.ProductInCart;

namespace BrokenByte.OnlineStore.Services.Abstractions
{
    public interface ICartService
    {
        public Task<List<ProductInCartDto>> GetListbyIdAsync(long userId);
        public Task<bool> TryChangeProductCountInCartAsync(ProductInCartUpdateDto updateModel);
    }
}
