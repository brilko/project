﻿using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;

namespace BrokenByte.OnlineStore.Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с производителями.
    /// </summary>
    public interface IManufacturerService
    {
        // <summary>
        /// Получить производителя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО производителя. </returns>
        Task<ManufacturerDto> GetById(long id);

        /// <summary>
        /// Создать производителя.
        /// </summary>
        /// <param name="creatingManufacturerDto"> ДТО создаваемого производителя. </param>
        Task<long> Create(CreatingManufacturerDto creatingManufacturerDto);

        /// <summary>
        /// Изменить производителя.
        /// </summary>
        /// <param name="id">Идентификатор. </param>
        /// <param name="updatingManufacturerDto"> ДТО редактируемого производителя. </param>
        Task Update(long id, UpdatingManufacturerDto updatingManufacturerDto);

        /// <summary>
        /// Удалить производителя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        Task Delete(long id);

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <returns> Список производителей. </returns>
        Task<ICollection<ManufacturerDto>> GetPaged(ManufacturerFilterDto filterDto);
    }
}