﻿using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using BrokenByte.OnlineStore.Services.Contracts.Product;

namespace BrokenByte.OnlineStore.Services.Abstractions
{
    public interface IProductService
    {
        // <summary>
        /// Получить товар.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО товара. </returns>
        Task<ProductDto> GetById(long id);

        /// <summary>
        /// Создать товар.
        /// </summary>
        /// <param name="creatingDto"> ДТО создаваемой сущности. </param>
        Task<long> Create(CreatingProductDto creatingDto);

        /// <summary>
        /// Изменить товар.
        /// </summary>
        /// <param name="id">Идентификатор. </param>
        /// <param name="updatingDto"> ДТО редактируемой сущности. </param>
        Task Update(long id, UpdatingProductDto updatingDto);

        /// <summary>
        /// Удалить сущность.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        Task Delete(long id);

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <returns> Список товаров. </returns>
        Task<ICollection<ProductDto>> GetPaged(ProductFilterDto filterDto);
    }
}
