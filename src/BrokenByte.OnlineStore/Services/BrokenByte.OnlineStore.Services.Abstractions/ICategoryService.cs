﻿using BrokenByte.OnlineStore.Services.Contracts.Category;

namespace BrokenByte.OnlineStore.Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с категорями.
    /// </summary>
    public interface ICategoryService
    {
        // <summary>
        /// Получить категорями.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО категорями. </returns>
        Task<CategoryDto> GetById(long id);

        /// <summary>
        /// Создать категорию.
        /// </summary>
        /// <param name="creatingCategoryDto"> ДТО создаваемой категорями. </param>
        Task<long> Create(CreatingCategoryDto creatingCategoryDto);

        /// <summary>
        /// Изменить категорию.
        /// </summary>
        /// <param name="id">Идентификатор. </param>
        /// <param name="updatingCategoryDto"> ДТО редактируемой категории. </param>
        Task Update(long id, UpdatingCategoryDto updatingCategoryDto);

        /// <summary>
        /// Удалить производителя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        Task Delete(long id);

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <returns> Список категорий. </returns>
        Task<ICollection<CategoryDto>> GetPaged(CategoryFilterDto filterDto);
    }
}