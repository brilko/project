﻿using BrokenByte.OnlineStore.Services.Contracts.ProductPrice;

namespace BrokenByte.OnlineStore.Services.Abstractions
{
    public interface IProductPriceService
    {
        // <summary>
        /// Получить все цены на продукт.
        /// </summary>        
        Task<ICollection<ProductPriceDto>> GetAllAsync(long productId);


        // <summary>
        /// Получить цену.
        /// </summary>
        /// <param name="id">ид цены</param>
        /// <returns> цена на товар. </returns>
        Task<ProductPriceDto?> Get(long id);

        /// <summary>
        /// Создать цену. Новая цена автоматически отменяет все последующие по времени
        /// </summary>
        /// <param name="creatingDto"> ДТО создаваемой сущности. </param>
        Task<long> Create(CreatingProductPriceDto creatingDto);

        /// <summary>
        /// Удалить сущность.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        Task Delete(long id);

        /// <summary>
        /// Получить постраничный список  цен.
        /// </summary>
        /// <returns> Список. </returns>
        Task<ICollection<ProductPriceDto>> GetPaged(ProductPriceFilterDto filterDto);


    }
}
