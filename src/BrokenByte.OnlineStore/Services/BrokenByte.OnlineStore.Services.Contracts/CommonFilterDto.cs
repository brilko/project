﻿namespace BrokenByte.OnlineStore.Services.Contracts
{
    public class CommonFilterDto
    {
        public int ItemsPerPage { get; set; }

        public int PageIndex { get; set; }
    }
}
