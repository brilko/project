﻿namespace BrokenByte.OnlineStore.Services.Contracts.Product
{
    public class ProductFilterDto
    {
        public bool? Visible { get; set; }

        public long? CategoryId { get; set; }

        public long? ManufactoryId { get; set; }

        public bool? Available { get; set; }

        public decimal? PriceMin { get; set; }
        public decimal? PriceMax { get; set; }

        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}
