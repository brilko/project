﻿namespace BrokenByte.OnlineStore.Services.Contracts.Product
{
    public class UpdatingProductDto
    {

        /// <summary>
        /// Артикул
        /// </summary>     
        public string Article { get; set; } = string.Empty;

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string ShortName { get; set; } = string.Empty;

        /// <summary>
        /// Полное наименование
        /// </summary>
        public string? FullName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }


        /// <summary>
        /// Показывать в каталоге или нет
        /// </summary>
        public bool Visible { get; set; }


        /// <summary>
        /// Ссылка на изображение продукта в каьалоге
        /// </summary>
        public string? Image { get; set; }

        /// <summary>
        /// ИД производителя
        /// </summary>
        public long ManufacturerId { get; set; }

        /// <summary>
        /// ИД Категории
        /// </summary>
        public long CategoryId { get; set; }
    }
}
