﻿namespace BrokenByte.OnlineStore.Services.Contracts.ProductInCart
{
    public class ProductInCartDto
    {
        public long Id { get; set; }

        public long ProductId { get; set; }

        public long CustomerId { get; set; }

        public string ShortName { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public string? Image { get; set; }

        public int Count { get; set; }

        public string ManufacturerName { get; set; } = string.Empty;

        public string CategoryName { get; set; } = string.Empty;

        public decimal Price { get; set; }
    }
}
