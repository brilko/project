﻿namespace BrokenByte.OnlineStore.Services.Contracts.ProductInCart
{
    public class ProductInCartUpdateDto
    {
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        public int Count { get; set; }
    }
}
