﻿namespace BrokenByte.OnlineStore.Services.Contracts.ProductPrice
{
    public class ProductPriceFilterDto
    {
        /// <summary>
        /// Ид продукта
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Все начиная с даты. Если Сurrently == true, то игнориуется
        /// </summary>
        public DateTime? From { get; set; }

        public int ItemsPerPage { get; set; }

        public int Page { get; set; }

    }
}
