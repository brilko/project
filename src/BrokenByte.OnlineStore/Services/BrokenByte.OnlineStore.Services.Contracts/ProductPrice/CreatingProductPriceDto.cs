﻿namespace BrokenByte.OnlineStore.Services.Contracts.ProductPrice
{
    public class CreatingProductPriceDto
    {
        /// <summary>
        /// Цена на товар
        /// </summary>
        public decimal Price { get; set; }


        /// <summary>
        /// Время начала действия новой цены
        /// </summary>
        public DateTime? DateBegin { get; set; }


        /// <summary>
        /// Ид продукта
        /// </summary>
        public long ProductId { get; set; }
    }
}
