﻿namespace BrokenByte.OnlineStore.Services.Contracts.ProductPrice
{
    public class ProductPriceDto
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }


        /// <summary>
        /// Цена на товар
        /// </summary>
        public decimal Price { get; set; }


        /// <summary>
        /// Время начала действия новой цены
        /// </summary>
        public DateTime DateBegin { get; set; }

        /// <summary>
        /// Ид товара
        /// </summary>
        public long ProductId { get; set; }
    }
}
