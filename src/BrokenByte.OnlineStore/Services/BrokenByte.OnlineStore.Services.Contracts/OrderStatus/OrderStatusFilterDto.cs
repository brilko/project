﻿namespace BrokenByte.OnlineStore.Services.Contracts.OrderStatus
{
    public class OrderStatusFilterDto : CommonFilterDto
    {
        public long Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string? Description { get; set; }
    }
}
