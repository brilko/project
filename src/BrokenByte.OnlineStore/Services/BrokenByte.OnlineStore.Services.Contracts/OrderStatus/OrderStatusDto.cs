﻿namespace BrokenByte.OnlineStore.Services.Contracts.OrderStatus
{
    public class OrderStatusDto
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Наименования статуса
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание статуса
        /// </summary>
        public string? Description { get; set; }

    }
}
