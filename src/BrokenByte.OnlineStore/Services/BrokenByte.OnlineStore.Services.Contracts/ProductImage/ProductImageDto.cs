﻿namespace BrokenByte.OnlineStore.Services.Contracts.Product
{
    public class ProductImageDto
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Описание картинки
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Ссылка на картинку
        /// </summary>
        public string Image { get; set; } = string.Empty;

    }
}
