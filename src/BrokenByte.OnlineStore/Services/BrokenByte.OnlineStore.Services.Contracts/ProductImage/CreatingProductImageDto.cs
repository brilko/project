﻿namespace BrokenByte.OnlineStore.Services.Contracts.Manufacturer
{
    public class CreatingProductImageDto
    {
        /// <summary>
        /// Описание картинки
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Ссылка на картинку
        /// </summary>
        public string Image { get; set; } = string.Empty;

    }
}
