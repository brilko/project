﻿namespace BrokenByte.OnlineStore.Services.Contracts.Category
{
    public class CategoryFilterDto
    {
        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}
