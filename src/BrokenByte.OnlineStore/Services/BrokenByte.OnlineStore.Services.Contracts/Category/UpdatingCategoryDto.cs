﻿namespace BrokenByte.OnlineStore.Services.Contracts.Category
{
    public class UpdatingCategoryDto
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }

        public string? Image { get; set; }
    }
}
