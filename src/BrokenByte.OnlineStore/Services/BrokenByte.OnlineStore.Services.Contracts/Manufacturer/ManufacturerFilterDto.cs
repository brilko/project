﻿namespace BrokenByte.OnlineStore.Services.Contracts.Manufacturer
{
    public class ManufacturerFilterDto
    {

        /// <summary>
        /// Начальные буквы название, либо путо - весь список
        /// </summary>
        public string Name { get; set; } = string.Empty;

        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}
