﻿namespace BrokenByte.OnlineStore.Services.Contracts.Manufacturer
{
    /// <summary>
    /// ДТО производителя.
    /// </summary>
    public class ManufacturerDto
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>        
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Путь к изображению
        /// </summary>
        public string? Image { get; set; }
    }
}