﻿using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Orders
{
    /// <summary>
    /// Заказ
    /// </summary>
    public class Order : BaseEntity
    {
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Идентификатор службы доставки
        /// </summary>
        public long DeliveryServiceId { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(250)]
        public string? Description { get; set; }

        /// <summary>
        /// Номер накладной
        /// </summary>
        [MaxLength(50)]
        public string? NumberInvoice { get; set; }

        /// <summary>
        /// Полная стоимость за все продукты (c учетом промокода)
        /// </summary>
        public int TotalPrice { get; set; }

        /// <summary>
        /// Дата оплаты
        /// </summary>
        public DateTime? PaymentAt { get; set; }

        /// <summary>
        /// Детали платежа
        /// </summary>
        [MaxLength(512)]
        public string PaymentInfo { get; set; } = string.Empty;

        /// <summary>
        /// Оплаченная сумма
        /// </summary>
        public decimal AllSumTransaction { get; set; }

        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public long CustomerId { get; set; }
        /// <summary>
        /// Ссылочная навигация на клиента, создавшего заказ
        /// </summary>
        public Customer? Customer { get; set; }

        /// <summary>
        /// ИД статуса заказа
        /// </summary>
        public long OrderStatusId { get; set; }
        /// <summary>
        /// Ссылочная навигация на адрес доставки
        /// </summary>
        public OrderStatus? OrderStatus { get; set; }

        /// <summary>
        /// Идентификатор адреса доставки
        /// </summary>
        public long DeliveryAddressId { get; set; }
        /// <summary>
        /// Ссылочная навигация на адрес доставки
        /// </summary>
        public DeliveryAddress? DeliveryAddress { get; set; }

        /// <summary>
        /// Идентификатор получателя
        /// </summary>
        public long? RecipientId { get; set; }
        /// <summary>
        /// Ссылочная навигация на получателя
        /// </summary>
        public Recipient? Recipient { get; set; }

        /// <summary>
        /// Навигация по коллекции товары в заказе 
        /// </summary>
        public List<Product>? Products { get; set; }
        /// <summary>
        /// Навигация по таблице связи многие-ко-многим для товаров в заказе
        /// </summary>
        public List<OrderItem>? OrderItems { get; set; }

    }
}
