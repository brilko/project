﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Orders
{
    /// <summary>
    /// Статус заказа
    /// </summary>
    public class OrderStatus : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(70)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(255)]
        public string? Description { get; set; }

        /// <summary>
        /// Навигация по коллекции заказы
        /// </summary>
        public List<Order>? Orders { get; set; }
    }
}
