﻿using BrokenByte.OnlineStore.Domain.Entities.Products;

namespace BrokenByte.OnlineStore.Domain.Entities.Orders
{
    /// <summary>
    /// Товар в заказе
    /// </summary>
    public class OrderItem : BaseEntity
    {
        /// <summary>
        /// Количество в заказе
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Цена на единицу товара на момент заказа
        /// </summary>
        public decimal PricePerOne { get; set; }

        /// <summary>
        /// Цена за весь товар 
        /// </summary>
        public decimal TotalPrice { get; set; }

        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Идентификатор товара
        /// </summary>
        public long ProductId { get; set; }
        /// <summary>
        /// Ссылочная навигация на товар
        /// </summary>
        public Product? Product { get; set; }

        /// <summary>
        /// Идентификатор заказа
        /// </summary>
        public long OrderId { get; set; }
        /// <summary>
        /// Ссылочная навигация на заказ
        /// </summary>
        public Order? Order { get; set; }

    }
}
