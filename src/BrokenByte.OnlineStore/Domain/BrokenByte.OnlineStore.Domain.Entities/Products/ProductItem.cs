﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Products
{

    public class ProductItem : BaseEntity
    {
        /// <summary>
        /// Артикул
        /// </summary>
        [MaxLength(50)]
        public string Article { get; set; } = string.Empty;

        public string Size { get; set; } = string.Empty;

        /// <summary>
        /// Количество  на складе
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Навигация по коллекции стоимость товара
        /// </summary>
        public virtual List<ProductPrice>? Prices { get; set; }

        /// <summary>
        /// Ид продукта
        /// </summary>
        public long ProductId { get; set; }


        /// <summary>
        /// Справочная навигация на продукт
        /// </summary>
        public virtual Product Product { get; set; }

    }
}
