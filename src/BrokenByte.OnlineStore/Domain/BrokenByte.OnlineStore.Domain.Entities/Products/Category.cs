﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Products
{
    // <summary>
    /// Категория товаров
    /// </summary>
    public class Category : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(100)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(256)]
        public string? Description { get; set; }

        /// <summary>
        /// Ссылка на изображение
        /// </summary>
        [MaxLength(1024)]
        public string? Image { get; set; }


        /// <summary>
        /// ИД на родительскую директорию
        /// </summary>
        // public long? ParentId { get; set; }     


        /// <summary>
        /// Полный путь к текущей категории (ParentParentId,ParentId)
        /// </summary>
        // [MaxLength(512)]
        // public string? FullPath { get; set; }

        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Ссылочная навигация на родительскую категорию
        /// </summary>
        //public Category? Parent { get; set; }


        /// <summary>
        /// Навигация по коллекции товаров 
        /// </summary>
        public List<Product> Products { get; set; } = new();

    }
}
