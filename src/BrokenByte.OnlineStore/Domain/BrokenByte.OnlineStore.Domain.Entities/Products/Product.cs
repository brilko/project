﻿using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Domain.Entities.Storages;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Products
{
    /// <summary>
    /// Товар
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        /// Артикул
        /// </summary>
        [MaxLength(50)]
        public string Article { get; set; } = string.Empty;

        /// <summary>
        /// Краткое наименование
        /// </summary>
        [MaxLength(150)]
        public string ShortName { get; set; } = string.Empty;

        /// <summary>
        /// Полное наименование
        /// </summary>
        [MaxLength(200)]
        public string? FullName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(1024)]
        public string? Description { get; set; }

        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Показывать в каталоге или нет
        /// </summary>
        public bool Visible { get; set; } = true;


        /// <summary>
        /// Ссылка на изображение продукта в каталоге
        /// </summary>
        [MaxLength(1024)]
        public string? Image { get; set; }

        /// <summary>
        /// ИД производителя
        /// </summary>
        public long ManufacturerId { get; set; }

        /// <summary>
        /// ИД Категории
        /// </summary>
        public long CategoryId { get; set; }

        /// <summary>
        /// Навигация по коллекции стоимость товара
        /// </summary>
        public List<ProductPrice>? Prices { get; set; }

        /// <summary>
        /// Ссылочная навигация на статус товара
        /// </summary>
        // public ProductStatus ProductStatus { get; set; }

        /// <summary>
        /// Навигация по коллекции товаров на складах
        /// </summary>
        public List<StorageItem>? StorageItems { get; }

        /// <summary>
        /// Навигация по коллекции изображений в галлерее продукта
        /// </summary>
        public List<ProductImage>? Images { get; set; }


        /// <summary>
        /// Ссылочная навигация на производителя
        /// </summary>
        public Manufacturer? Manufacturer { get; set; }


        /// <summary>
        /// Ссылочная навигация на категорию товара
        /// </summary>
        public Category? Category { get; set; }

        /// <summary>
        /// Ссылочная навигация на корзины
        /// </summary>
        public List<Customer>? CustomersWithProductInCart { get; set; }

        /// <summary>
        /// Ссылочная навигация на вишлисты
        /// </summary>
        public List<Customer>? CustomersWithProductInWishList { get; set; }

        /// <summary>
        /// Ссылочная навигация товары в заказах
        /// </summary>
        public List<OrderItem>? OrderItems { get; set; }

        public List<Order>? Orders { get; set; }

    }
}
