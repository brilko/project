﻿namespace BrokenByte.OnlineStore.Domain.Entities.Products
{
    /// <summary>
    /// Производитель
    /// </summary>
    public class Manufacturer : BaseEntity
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Путь к изображению
        /// </summary>
        public string? Image { get; set; }

        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Навигация по коллекции товаров 
        /// </summary>
        public List<Product>? Products { get; }
    }
}
