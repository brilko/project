﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Products
{
    /// <summary>
    /// Изображение продукта
    /// </summary>
    public class ProductImage : BaseEntity
    {
        /// <summary>
        /// Описание картинки
        /// </summary>
        [MaxLength(256)]
        public string? Description { get; set; }

        /// <summary>
        /// Ссылка на картинку
        /// </summary>
        [MaxLength(1024)]
        public string Image { get; set; } = string.Empty;

        /// <summary>
        /// Ссфлка на товар
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Ссылочная навигация на товар
        /// </summary>
        public Product? Product { get; set; }

    }
}
