﻿namespace BrokenByte.OnlineStore.Domain.Entities.Products
{
    /// <summary>
    /// Стоимость товара
    /// Отдельная сущность потому что можно сделать цену в зависимости от роли покупателя и т.д.
    /// </summary>
    public class ProductPrice : BaseEntity
    {
        /// <summary>
        /// Цена на товар
        /// </summary>
        public decimal Price { get; set; }



        /// <summary>
        /// Время начала действия новой цены
        /// </summary>
        public DateTime? DateBegin { get; set; }

        /// <summary>
        /// Удалено
        /// </summary>
        public bool Deleted { get; set; } = false;


        /// <summary>
        /// Ид продукта
        /// </summary>
        public long ProductId { get; set; }


        /// <summary>
        /// Справочная навигация на продукт
        /// </summary>
        public Product? Product { get; set; }


    }
}
