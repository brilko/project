﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Employees
{
    public class EmployeeRole : BaseEntity
    {
        [MaxLength(70)]
        public string Name { get; set; } = string.Empty;

        [MaxLength(150)]
        public string Description { get; set; } = string.Empty;

        public List<Employee>? Employees { get; set; }
    }
}
