﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BrokenByte.OnlineStore.Domain.Entities.Employees
{
    /// <summary>
    /// Описание покупателя
    /// </summary>
    public class EmployeeDescription : BaseEntity
    {
        [MaxLength(512)]
        [Column(TypeName = "image")]
        public byte[]? Photo { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; } = string.Empty;

        /// <summary>
        /// Аккаунт в соц.сетиях
        /// </summary>
        public string? SocialMediaAccount { get; set; }

        /// <summary>
        /// Аккаунт в мессенджере
        /// </summary>
        public string? MessengerAccount { get; set; }

        public long EmployeeId { get; set; }

        public Employee? Employee { get; set; }
    }
}
