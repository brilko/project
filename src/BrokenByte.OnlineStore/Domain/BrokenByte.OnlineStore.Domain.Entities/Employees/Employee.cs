﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Employees
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee : BaseEntity
    {
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; } = string.Empty;

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// Фамилия
        /// </summary>       
        [MaxLength(50)]
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Имя
        /// </summary>
        [MaxLength(50)]
        public string FirstName { get; set; } = string.Empty;


        /// <summary>
        /// Отчество
        /// </summary>
        [MaxLength(50)]
        public string? MiddleName { get; set; }


        public long EmployeeRoleId { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [MaxLength(70)]
        public string Email { get; set; } = string.Empty;

        public EmployeeRole? Role { get; set; }


    }
}
