﻿namespace BrokenByte.OnlineStore.Domain.Entities
{
    /// <summary>
    /// Абстрактный клсаа сущности с идентификатором.
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id { get; set; }
    }
}