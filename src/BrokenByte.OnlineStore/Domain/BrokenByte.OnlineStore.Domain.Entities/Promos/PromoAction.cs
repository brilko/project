﻿using BrokenByte.OnlineStore.Domain.Entities.Employees;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Promos
{
    public class PromoAction : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(120)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(255)]
        public string? Description { get; set; }


        /// <summary>
        /// Ссылка на банер акции
        /// </summary>
        [MaxLength(1024)]
        public string? LinkToImage { get; set; }

        /// <summary>
        /// Промокод
        /// </summary>
        [MaxLength(50)]
        public string? PromoCode { get; set; }

        /// <summary>
        /// Скидка в %
        /// </summary>
        public decimal? DiscountPersent { get; set; }

        /// <summary>
        /// Скидка в рублях (при покупке товара ХХХ скидка 300 рублей)
        /// </summary>
        public decimal? Discount { get; set; }

        /// <summary>
        /// Дата начала дей1ствия акции
        /// </summary>
        public DateTime FromDate { get; set; }

        /// <summary>
        /// Дата окончания действия акции
        /// </summary>
        public DateTime ToDate { get; set; }

        /// <summary>
        /// Акция активна?
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Ссылка на сотрудника добавившего акцию
        /// </summary>
        public long EmployeeId { get; set; }

        /// <summary>
        /// Тип акции
        /// </summary>
        public long PromoActionTypeId { get; set; }


        /// <summary>
        /// Ссылочная навигация на сотрудника
        /// </summary>
        public Employee? Employee { get; set; }

        /// <summary>
        /// Ссылочная навигация на тип акции
        /// </summary>
        public PromoActionType? PromoActionType { get; set; }
    }
}
