﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Promos
{
    /// <summary>
    /// Тип промоакции
    /// </summary>
    public class PromoActionType : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(70)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(255)]
        public string? Description { get; set; }


        public List<PromoAction>? Actions { get; set; }
    }
}
