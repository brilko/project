﻿using BrokenByte.OnlineStore.Domain.Entities.Products;

namespace BrokenByte.OnlineStore.Domain.Entities.Promos
{
    /// <summary>
    /// Товары по акции
    /// </summary>
    public class PromoProduct : BaseEntity
    {
        /// <summary>
        /// Ид продукта
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Ид промоакции
        /// </summary>
        public long PromoActionId { get; set; }

        /// <summary>
        /// Ссылочная навигация на товар
        /// </summary>
        public Product? Product { get; set; }

        /// <summary>
        /// Ссылочная навигация на промоакцию
        /// </summary>
        public PromoAction? PromoAction { get; set; }


    }
}
