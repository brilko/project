﻿using BrokenByte.OnlineStore.Domain.Entities.Orders;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Customers
{
    /// <summary>
    /// Адрес доставки
    /// </summary>
    public class DeliveryAddress : BaseEntity
    {
        /// <summary>
        /// Адрес
        /// </summary>
        [MaxLength(512)]
        public string Address { get; set; } = string.Empty;


        /// <summary>
        /// Ссылка на клиента для которого этот получатель заведен
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// Ссылочная навигация на клиента
        /// </summary>
        public Customer? Customer { get; set; }

        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Навигация по коллекции заказы
        /// </summary>
        public List<Order>? Orders { get; set; }
    }
}
