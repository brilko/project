﻿using BrokenByte.OnlineStore.Domain.Entities.Orders;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Customers
{
    /// <summary>
    /// Получатель заказа
    /// </summary>
    public class Recipient : BaseEntity
    {
        /// <summary>
        /// Имя получателя
        /// </summary>
        [MaxLength(120)]
        public string FullName { get; set; } = string.Empty;

        /// <summary>
        /// Номер телефона получателя
        /// </summary>
        [MaxLength(50)]
        public string Phone { get; set; } = string.Empty;

        /// <summary>
        /// Ссылка на клиента для которого этот получатель заведен
        /// </summary>
        public long CustomerId { get; set; }

        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Ссылочная навигация на клиента
        /// </summary>
        public Customer? Customer { get; set; }

        /// <summary>
        /// Навигация по коллекции заказы
        /// </summary>
        public List<Order>? Orders { get; set; }
    }
}
