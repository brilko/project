﻿using BrokenByte.OnlineStore.Domain.Entities.Products;

namespace BrokenByte.OnlineStore.Domain.Entities.Customers
{
    /// <summary>
    /// Продукт в вишлисте. Для организации связи многие ко многим
    /// </summary>
    public class ProductInWishList : BaseEntity
    {
        /// <summary>
        /// Ид продукта
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Ид клиента
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// Ссылочная навигация на товар
        /// </summary>
        public Product? Product { get; set; }

        /// <summary>
        /// Ссылочная навигация на клиента
        /// </summary>
        public Customer? Customer { get; set; }
    }
}
