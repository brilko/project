﻿using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Customers
{
    /// <summary>
    /// Покупатель
    /// </summary>
    public class Customer : BaseEntity
    {
        /// <summary>
        /// Логин
        /// </summary>
        [MaxLength(50)]
        public string Login { get; set; } = string.Empty;

        /// <summary>
        /// Пароль
        /// </summary>
        [MaxLength(128)]
        public string Password { get; set; } = string.Empty;


        /// <summary>
        /// Имя
        /// </summary>
        [MaxLength(50)]
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Фамилия
        /// </summary>       
        [MaxLength(50)]
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Отчество
        /// </summary>
        [MaxLength(50)]
        public string? MiddleName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [MaxLength(70)]
        public string Email { get; set; } = string.Empty;


        public bool Deleted { get; set; } = false;

        /// <summary>
        /// Навигация по коллекции адреса доставки 
        /// </summary>
        public List<DeliveryAddress>? DeliveryAddresses { get; set; }

        /// <summary>
        /// Навигация по коллекции получатели 
        /// </summary>
        public List<Recipient>? Recipients { get; set; }

        /// <summary>
        /// Навигация по коллеции заказы 
        /// </summary>
        public List<Order>? Orders { get; set; }

        /// <summary>
        /// Навигация по коллеции товары в корзине 
        /// </summary>
        public List<Product>? Cart { get; set; }
        public List<ProductInCart>? ProductsInCart { get; set; }


        /// <summary>
        /// Навигация по коллеции отложенное (лист желаний)
        /// </summary>
        public List<Product>? WishList { get; set; }
        public List<ProductInWishList>? ProductsInWishList { get; set; }
    }
}
