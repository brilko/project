﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.Domain.Entities.Storages
{
    /// <summary>
    /// Склад  
    /// </summary>
    public class Storage : BaseEntity
    {
        /// <summary>
        /// Название склада
        /// </summary>
        [MaxLength(100)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Адрес
        /// </summary>
        [MaxLength(256)]
        public string? Address { get; set; }

        /// <summary>
        /// Навигация по коллекции Продукты на складе
        /// </summary>
        public List<StorageItem>? StorageItems { get; set; }
    }
}
