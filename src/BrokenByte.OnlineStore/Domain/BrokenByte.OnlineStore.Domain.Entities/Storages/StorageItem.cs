﻿using BrokenByte.OnlineStore.Domain.Entities.Products;

namespace BrokenByte.OnlineStore.Domain.Entities.Storages
{
    public class StorageItem : BaseEntity
    {
        /// <summary>
        /// Ид продукта
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Фактическое количество товаров на складе
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Количество товара забронированное под заказы покупателей
        /// </summary>
        public int Reserved { get; set; }

        /// <summary>
        /// Количество ожидаемого к поступлению товара - уже оплаченная доставка от поставщика
        /// </summary>
        public int Expected { get; set; }

        /// <summary>
        /// Удален продукт
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// ИД склада
        /// </summary>
        public long StorageId { get; set; }

        /// <summary>
        /// Ссылочная навигация на продукт
        /// </summary>
        public Product? Product { get; set; } = null!;

        /// <summary>
        /// Ссылочная навигация на склад
        /// </summary>
        public Storage? Storage { get; set; } = null!;


        /// Доступно для продажи — доступное для продажи количество = Остаток − Зарезервировано + Ожидается        
        /// Это пойдет в дто
        //public int Available { get; set; }   
    }
}
