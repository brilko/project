﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using Microsoft.AspNetCore.Mvc;

namespace BrokenByte.OnlineStore.WebApi.Controllers
{

    /// <summary>
    /// Производители
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ManufacturerController : ControllerBase
    {
        private readonly IManufacturerService _service;
        private readonly IMapper _mapper;
#pragma warning disable IDE0052 // Удалить непрочитанные закрытые члены
        private readonly ILogger<ManufacturerController> _logger;
#pragma warning restore IDE0052 // Удалить непрочитанные закрытые члены

        /// <summary>
        /// конструктор
        /// </summary>
        public ManufacturerController(IManufacturerService service, ILogger<ManufacturerController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные производителя по id
        /// </summary>
        [HttpGet("{id}")]
        [ResponseCache(
            Location = ResponseCacheLocation.Any,
            Duration = 120,
            VaryByQueryKeys = new string[] { "id" })]
        public async Task<IActionResult> Get(long id)
        {
            return Ok(_mapper.Map<ManufacturerModel>(await _service.GetById(id)));
            //return Ok(DateTime.Now.ToString());
        }


        /// <summary>
        /// Создать нового производителя
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(CreatingManufacturerModel model)
        {
            return Ok(await _service.Create(_mapper.Map<CreatingManufacturerDto>(model)));
        }

        /// <summary>
        ///  Обновить данные производителя с id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(long id, UpdatingManufacturerModel model)
        {
            await _service.Update(id, _mapper.Map<UpdatingManufacturerModel, UpdatingManufacturerDto>(model));
            return Ok();
        }

        /// <summary>
        /// Удалить производителя с id
        /// </summary>     
        /// <returns></returns>

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await _service.Delete(id);
            return Ok();
        }

        /// <summary>
        /// Получить постраничный список производителей с фильтром
        /// </summary>       
        [HttpPost("list")]
        public async Task<IActionResult> GetList(ManufacturerFilterModel filterModel)
        {
            var filterDto = _mapper.Map<ManufacturerFilterModel, ManufacturerFilterDto>(filterModel);
            return Ok(_mapper.Map<List<ManufacturerModel>>(await _service.GetPaged(filterDto)));
        }
    }
}
