﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using BrokenByte.OnlineStore.WebApi.Models.Order;
using Microsoft.AspNetCore.Mvc;

namespace BrokenByte.OnlineStore.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IRepository<Order> repository;
        private readonly IMapper mapper;

        public OrderController(IRepository<Order> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetById(long id)
        {
            var entity = await repository.GetAsync(id);
            var model = mapper.Map<LongOrderModel>(entity);
            return Ok(model);
        }

        [HttpGet("page")]
        public async Task<IActionResult> GetPage(int pageIndex, int ItemsPerPage)
        {
            var page = new Page()
            {
                PageIndex = pageIndex,
                ItemsPerPage = ItemsPerPage
            };
            var entities = await repository.GetPageAsync(page);
            var models = mapper.Map<List<ShortOrderModel>>(entities);
            return Ok(models);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(long id)
        {
            if (await repository.TryDeleteAsync(id))
                return Ok();
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateOrEditOrderModel model)
        {
            var entity = mapper.Map<Order>(model);
            var newId = await repository.AddAsync(entity);
            return Ok(newId);
        }

        [HttpPut]
        public async Task<IActionResult> Update(CreateOrEditOrderModel model)
        {
            var entity = mapper.Map<Order>(model);
            if (await repository.UpdateAsync(entity))
                return Ok();
            return NotFound();
        }
    }
}
