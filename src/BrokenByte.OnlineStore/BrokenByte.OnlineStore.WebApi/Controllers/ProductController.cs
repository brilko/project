﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using BrokenByte.OnlineStore.Services.Contracts.Product;
using BrokenByte.OnlineStore.Services.Contracts.Products;
using BrokenByte.OnlineStore.WebApi.Models.Products;
using Microsoft.AspNetCore.Mvc;

namespace BrokenByte.OnlineStore.WebApi.Controllers
{
    /// <summary>
    /// Производители
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;
        private readonly IMapper _mapper;
#pragma warning disable IDE0052 // Удалить непрочитанные закрытые члены
        private readonly ILogger<ProductController> _logger;
#pragma warning restore IDE0052 // Удалить непрочитанные закрытые члены

        /// <summary>
        /// конструктор
        /// </summary>
        public ProductController(IProductService service, ILogger<ProductController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные товара по id
        /// </summary>
        [HttpGet("{id}")]
        [ResponseCache(
            Location = ResponseCacheLocation.Any,
            Duration = 120,
            VaryByQueryKeys = new string[] { "id" })]
        public async Task<IActionResult> Get(long id)
        {
            return Ok(_mapper.Map<ProductModel>(await _service.GetById(id)));
        }


        /// <summary>
        /// Создать новый товар
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(CreatingProductModel model)
        {
            return Ok(await _service.Create(_mapper.Map<CreatingProductDto>(model)));
        }

        /// <summary>
        ///  Обновить данные товар с id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(long id, UpdatingProductModel model)
        {
            await _service.Update(id, _mapper.Map<UpdatingProductModel, UpdatingProductDto>(model));
            return Ok();
        }

        /// <summary>
        /// Удалить товар с id
        /// </summary>     
        /// <returns></returns>

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await _service.Delete(id);
            return Ok();
        }

        /// <summary>
        /// Получить постраничный список с фильтром
        /// </summary>       
        [HttpPost("list")]
        public async Task<IActionResult> GetList(ProductFilterModel filterModel)
        {
            var filterDto = _mapper.Map<ProductFilterModel, ProductFilterDto>(filterModel);
            return Ok(_mapper.Map<List<ProductModel>>(await _service.GetPaged(filterDto)));
        }
    }
}
