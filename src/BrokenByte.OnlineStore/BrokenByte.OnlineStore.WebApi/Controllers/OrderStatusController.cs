﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.OrderStatus;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using BrokenByte.OnlineStore.WebApi.Models.OrderStatus;
using Microsoft.AspNetCore.Mvc;

namespace BrokenByte.OnlineStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderStatusController : ControllerBase
    {
        private readonly IRepository<OrderStatus> orderStatusRepository;
        private readonly IMapper mapper;
        private readonly IOrderStatusService orderStatusService;

        public OrderStatusController(
            IRepository<OrderStatus> orderStatusRepository,
            IMapper mapper,
            IOrderStatusService orderStatusService)
        {
            this.orderStatusRepository = orderStatusRepository;
            this.mapper = mapper;
            this.orderStatusService = orderStatusService;
        }

        /// <summary>
        /// Получить данные статуса заказа по id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var entity = await orderStatusRepository.GetAsync(id);
            if (entity == null)
                return NotFound();
            var model = mapper.Map<ShortOrderStatusModel>(entity);
            return Ok(model);
        }


        /// <summary>
        /// Создать новый статус заказа 
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(CreateOrEditOrderStatusModel model)
        {
            var entity = mapper.Map<OrderStatus>(model);
            var addedEntity = await orderStatusRepository.AddAsync(entity);
            await orderStatusRepository.SaveChangesAsync();
            return Created(addedEntity.Id.ToString(), addedEntity);
        }

        /// <summary>
        /// Обновить данные статуса заказа с id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(long id, CreateOrEditOrderStatusModel model)
        {
            var entity = mapper.Map<OrderStatus>(model);
            entity.Id = id;
            if (!await orderStatusRepository.UpdateAsync(entity))
                return NotFound();
            await orderStatusRepository.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Удалить статус заказа с id
        /// </summary>     
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var isDeleted = await orderStatusRepository.TryDeleteAsync(id);
            if (isDeleted)
            {
                await orderStatusRepository.SaveChangesAsync();
                return Ok();
            }
            return NotFound();
        }

        /// <summary>
        /// Получить постраничный список статусов заказов с фильтром
        /// </summary>       
        [HttpPost("list")]
        public async Task<IActionResult> GetList(OrderStatusFilterDto filter)
        {
            var entities = await orderStatusService.GetFilteredAsync(filter);
            var models = mapper.Map<List<ShortOrderStatusModel>>(entities);
            return Ok(models);
        }
    }
}
