﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.ProductInCart;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using BrokenByte.OnlineStore.WebApi.Models.Cart;
using BrokenByte.OnlineStore.WebApi.ValidationAttributes;
using Microsoft.AspNetCore.Mvc;


//10D940EB-21D1-4E2E-BF78-EB166A0B721A
namespace BrokenByte.OnlineStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IRepository<ProductInCart> productsInCartRepository;
        private readonly ICartService cartService;

        public CartController(IMapper mapper, IRepository<ProductInCart> productsInCartRepository, ICartService cartService)
        {
            this.mapper = mapper;
            this.productsInCartRepository = productsInCartRepository;
            this.cartService = cartService;
        }

        /// <summary>
        /// Получить товары в корзине пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns>Список товаров в корзине пользователя</returns>
        [HttpGet("list/{userId}")]
        public async Task<IActionResult> GetListbyId([NonZeroIntLong] long userId)
        {
            var productsInCartDto = await cartService.GetListbyIdAsync(userId);
            var productsInCartModels = mapper.Map<ProductInCartModel[]>(productsInCartDto);
            return Ok(productsInCartModels);
        }

        /// <summary>
        /// Изменить количество товара в корзине
        /// </summary>
        /// <param name="updateModel">Содержит идентификатор пользователя,
        /// идентификатор продукта, количество продукта </param>
        /// <returns>В случае успеха возвращает идентификатор товара в корзине,
        /// иначе сообщение о неудаче</returns>
        [HttpPut()]
        public async Task<IActionResult> ChangeProductCountInCart(ProductInCartUpdateModel updateModel)
        {
            var updateDto = mapper.Map<ProductInCartUpdateDto>(updateModel);
            if (await cartService.TryChangeProductCountInCartAsync(updateDto))
                return Ok();
            return NotFound("Пользователь или продукт с указанным идентификатором не существуют.");
        }

        /// <summary>
        /// Удалить Продукт из корзины
        /// </summary>
        /// <param name="productInCartId">Идентификатор продукта в корзине</param>
        /// <returns>Сообщение об успешности</returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteById([NonZeroIntLong] long productInCartId)
        {
            if (await productsInCartRepository.TryDeleteAsync(productInCartId))
                return Ok();
            return NotFound();
        }
    }
}
