﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.Category;
using BrokenByte.OnlineStore.WebApi.Models.Categoies;
using Microsoft.AspNetCore.Mvc;

namespace BrokenByte.OnlineStore.WebApi.Controllers
{
    /// <summary>
    /// Категории
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _service;
        private readonly IMapper _mapper;
#pragma warning disable IDE0052 // Удалить непрочитанные закрытые члены
        private readonly ILogger<CategoryController> _logger;
#pragma warning restore IDE0052 // Удалить непрочитанные закрытые члены

        /// <summary>
        /// конструктор
        /// </summary>
        public CategoryController(ICategoryService service, ILogger<CategoryController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные категории по id
        /// </summary>
        [HttpGet("{id}")]
        [ResponseCache(
            Location = ResponseCacheLocation.Any,
            Duration = 120,
            VaryByQueryKeys = new string[] { "id" })]
        public async Task<IActionResult> Get(long id)
        {
            return Ok(_mapper.Map<CategoryModel>(await _service.GetById(id)));
        }


        /// <summary>
        /// Создать новую категорию
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(CreatingCategoryModel model)
        {
            return Ok(await _service.Create(_mapper.Map<CreatingCategoryDto>(model)));
        }

        /// <summary>
        ///  Обновить данные категории с id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(long id, UpdatingCategoryModel model)
        {
            await _service.Update(id, _mapper.Map<UpdatingCategoryModel, UpdatingCategoryDto>(model));
            return Ok();
        }

        /// <summary>
        /// Удалить категорию с id
        /// </summary>     
        /// <returns></returns>

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await _service.Delete(id);
            return Ok();
        }

        /// <summary>
        /// Получить постраничный список с фильтром
        /// </summary>       
        [HttpPost("list")]
        public async Task<IActionResult> GetList(CategoryFilterModel filterModel)
        {
            var filterDto = _mapper.Map<CategoryFilterModel, CategoryFilterDto>(filterModel);
            return Ok(_mapper.Map<List<CategoryModel>>(await _service.GetPaged(filterDto)));
        }
    }
}
