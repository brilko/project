﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.ProductPrice;
using BrokenByte.OnlineStore.WebApi.Models.ProductPrices;
using Microsoft.AspNetCore.Mvc;

namespace BrokenByte.OnlineStore.WebApi.Controllers
{

    /// <summary>
    /// Производители
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductPriceController : ControllerBase
    {
        private readonly IProductPriceService _service;
        private readonly IMapper _mapper;
#pragma warning disable IDE0052 // Удалить непрочитанные закрытые члены
        private readonly ILogger<ProductPriceController> _logger;
#pragma warning restore IDE0052 // Удалить непрочитанные закрытые члены

        /// <summary>
        /// конструктор
        /// </summary>
        public ProductPriceController(IProductPriceService service, ILogger<ProductPriceController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить цену товара по id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            return Ok(_mapper.Map<ProductPriceModel>(await _service.Get(id)));
        }


        /// <summary>
        /// Задать новую цену на товар
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(CreatingProductPriceModel model)
        {
            return Ok(await _service.Create(_mapper.Map<CreatingProductPriceDto>(model)));
        }

        /// <summary>
        /// Удалить цену на товар с id
        /// </summary>     
        /// <returns></returns>

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await _service.Delete(id);
            return Ok();
        }

        /// <summary>
        /// Получить постраничный список с фильтром
        /// </summary>       
        [HttpPost("list")]
        public async Task<IActionResult> GetList(ProductPriceFilterModel filterModel)
        {
            var filterDto = _mapper.Map<ProductPriceFilterModel, ProductPriceFilterDto>(filterModel);
            return Ok(_mapper.Map<List<ProductPriceModel>>(await _service.GetPaged(filterDto)));
        }
    }
}
