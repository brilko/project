﻿using System.ComponentModel.DataAnnotations;

namespace BrokenByte.OnlineStore.WebApi.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter)]
    public class NonZeroIntLongAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value == null)
                throw new ArgumentNullException();

            if (!long.TryParse(value.ToString(), out long number))
            {
                throw new ArgumentException();
            }

            return number >= 0;
        }
    }
}
