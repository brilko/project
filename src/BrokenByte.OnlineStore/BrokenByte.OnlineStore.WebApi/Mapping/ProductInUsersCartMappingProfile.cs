﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Services.Contracts.ProductInCart;
using BrokenByte.OnlineStore.WebApi.Models.Cart;

namespace BrokenByte.OnlineStore.WebApi.Mapping
{
    public class ProductInUsersCartMappingProfile : Profile
    {
        public ProductInUsersCartMappingProfile()
        {
            CreateMap<ProductInCart, ProductInCartDto>()
                .ForMember(dest => dest.ShortName, o => o.MapFrom(src => src.Product!.ShortName))
                .ForMember(dest => dest.Description, o => o.MapFrom(src => src.Product!.Description))
                .ForMember(dest => dest.Image, o => o.MapFrom(src => src.Product!.Image))
                .ForMember(dest => dest.ManufacturerName,
                    o => o.MapFrom(src => src.Product!.Manufacturer!.Name))
                .ForMember(dest => dest.CategoryName,
                    o => o.MapFrom(src => src.Product!.Category!.Name))
                .ForMember(dest => dest.Price,
                    o => o.MapFrom(src => src.Product!.Prices!
                        .Where(p => !p.Deleted)
                        .Where(p => p.DateBegin < DateTime.Now)
                        .MaxBy(p => p.DateBegin)!
                        .Price));


            CreateMap<ProductInCartDto, ProductInCartModel>();

            CreateMap<ProductInCartUpdateModel, ProductInCartUpdateDto>();

            CreateMap<ProductInCartUpdateDto, ProductInCart>()
                .ForMember(dest => dest.Id, o => o.Ignore())
                .ForMember(dest => dest.Product, o => o.Ignore())
                .ForMember(dest => dest.Customer, o => o.Ignore());
        }
    }
}