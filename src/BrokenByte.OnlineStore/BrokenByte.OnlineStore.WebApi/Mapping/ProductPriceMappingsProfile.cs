﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Contracts.ProductPrice;
using BrokenByte.OnlineStore.WebApi.Models.ProductPrices;

namespace BrokenByte.OnlineStore.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности цена товара.
    /// </summary>
    public class ProductPriceMappingsProfile : Profile
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ProductPriceMappingsProfile()
        {
            CreateMap<ProductPriceDto, ProductPriceModel>().ReverseMap();
            CreateMap<CreatingProductPriceModel, CreatingProductPriceDto>();
            CreateMap<ProductPriceFilterModel, ProductPriceFilterDto>();
        }
    }
}
