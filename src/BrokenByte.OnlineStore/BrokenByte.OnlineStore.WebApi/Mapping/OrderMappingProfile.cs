﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.WebApi.Models.Order;

namespace BrokenByte.OnlineStore.WebApi.Mapping
{
    public class OrderMappingProfile : Profile
    {
        private List<long> GetIds<T>(List<T>? entities)
            where T : BaseEntity
        {
            if (entities == null)
                return new List<long>();
            return entities.Select(e => e.Id).ToList();
        }

        public OrderMappingProfile()
        {
            CreateMap<CreateOrEditOrderModel, Order>()
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.Products, opt => opt.Ignore())
                .ForMember(d => d.OrderStatus, opt => opt.Ignore())
                .ForMember(d => d.OrderItems, opt => opt.Ignore())
                .ForMember(d => d.Customer, opt => opt.Ignore())
                .ForMember(d => d.DeliveryAddress, opt => opt.Ignore())
                .ForMember(d => d.Recipient, opt => opt.Ignore());
            CreateMap<Order, ShortOrderModel>();
            CreateMap<Order, LongOrderModel>()
                .ForMember(d => d.Products, opt => opt.MapFrom((o, lo) =>
                    lo.Products = GetIds(o.Products)))
                .ForMember(d => d.OrderItems, opt => opt.MapFrom((o, lo) =>
                    lo.OrderItems = GetIds(o.OrderItems)));
        }
    }
}
