﻿using AutoMapper;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Services.Contracts.OrderStatus;
using BrokenByte.OnlineStore.WebApi.Models.OrderStatus;

namespace BrokenByte.OnlineStore.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности производитель.
    /// </summary>
    public class OrderStatusMappingsProfile : Profile
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public OrderStatusMappingsProfile()
        {
            CreateMap<OrderStatusDto, ShortOrderStatusModel>().ReverseMap();
            CreateMap<OrderStatus, ShortOrderStatusModel>();
            CreateMap<CreateOrEditOrderStatusModel, OrderStatus>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Orders, opt => opt.Ignore());
        }
    }
}
