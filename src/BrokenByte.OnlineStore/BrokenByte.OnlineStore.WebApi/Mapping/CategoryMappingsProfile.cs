﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Contracts.Category;
using BrokenByte.OnlineStore.WebApi.Models.Categoies;

namespace BrokenByte.OnlineStore.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности категория товаров.
    /// </summary>
    public class CategoryMappingsProfile : Profile
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CategoryMappingsProfile()
        {
            CreateMap<CategoryDto, CategoryModel>();
            CreateMap<CategoryModel, CategoryDto>();
            CreateMap<CreatingCategoryModel, CreatingCategoryDto>();
            CreateMap<UpdatingCategoryModel, UpdatingCategoryDto>();
            CreateMap<CategoryFilterModel, CategoryFilterDto>();
        }
    }
}
