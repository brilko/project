﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using BrokenByte.OnlineStore.Services.Contracts.Product;
using BrokenByte.OnlineStore.Services.Contracts.Products;
using BrokenByte.OnlineStore.WebApi.Models.Products;

namespace BrokenByte.OnlineStore.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности товар.
    /// </summary>
    public class ProductMappingsProfile : Profile
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ProductMappingsProfile()
        {
            CreateMap<ProductDto, ProductModel>().ReverseMap();
            CreateMap<CreatingProductModel, CreatingProductDto>();
            CreateMap<UpdatingProductModel, UpdatingProductDto>();
            CreateMap<ProductFilterModel, ProductFilterDto>();
        }
    }
}
