﻿using AutoMapper;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;

namespace BrokenByte.OnlineStore.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности производитель.
    /// </summary>
    public class ManufacturerMappingsProfile : Profile
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ManufacturerMappingsProfile()
        {
            CreateMap<ManufacturerDto, ManufacturerModel>();
            CreateMap<ManufacturerModel, ManufacturerDto>();
            CreateMap<CreatingManufacturerModel, CreatingManufacturerDto>();
            CreateMap<UpdatingManufacturerModel, UpdatingManufacturerDto>();
            CreateMap<ManufacturerFilterModel, ManufacturerFilterDto>();
        }
    }
}
