﻿namespace BrokenByte.OnlineStore.Services.Contracts.Manufacturer
{
    /// <summary>
    /// Модель производителя.
    /// </summary>
    public class ManufacturerModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Путь к логотипу
        /// </summary>
        public string? Image { get; set; }

    }
}