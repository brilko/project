﻿namespace BrokenByte.OnlineStore.Services.Contracts.Manufacturer
{
    /// <summary>
    /// Модель для обновление производителя
    /// </summary>
    public class UpdatingManufacturerModel
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Путь к логотипу
        /// </summary>
        public string Image { get; set; } = string.Empty;
    }
}
