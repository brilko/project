﻿namespace BrokenByte.OnlineStore.Services.Contracts.Manufacturer
{
    /// <summary>
    /// Модель для фильтрации производителей
    /// </summary>
    public class ManufacturerFilterModel
    {
        /// <summary>
        /// Название производителя
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Количество производителей на странице
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int Page { get; set; }
    }
}
