﻿namespace BrokenByte.OnlineStore.WebApi.Models.Order
{
    public class LongOrderModel
    {
        public long Id { get; set; }

        public DateTime? CreatedAt { get; set; }

        public long DeliveryServiceId { get; set; }

        public string? Description { get; set; }

        public string? NumberInvoice { get; set; }

        public int TotalPrice { get; set; }

        public DateTime? PaymentAt { get; set; }

        public string PaymentInfo { get; set; } = string.Empty;

        public decimal AllSumTransaction { get; set; }

        public long CustomerId { get; set; }

        public long OrderStatusId { get; set; }

        public long DeliveryAddressId { get; set; }

        public long? RecipientId { get; set; }

        public List<long> Products { get; set; } = new List<long>();

        public List<long> OrderItems { get; set; } = new List<long>();
    }
}
