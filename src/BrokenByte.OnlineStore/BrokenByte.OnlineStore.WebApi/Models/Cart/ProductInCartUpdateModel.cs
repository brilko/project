﻿namespace BrokenByte.OnlineStore.WebApi.Models.Cart
{
    public class ProductInCartUpdateModel
    {
        //[NonZeroIntLong]
        public long CustomerId { get; set; }
        //[NonZeroIntLong]
        public long ProductId { get; set; }
        //[NonZeroIntLong]
        public int Count { get; set; }
    }
}
