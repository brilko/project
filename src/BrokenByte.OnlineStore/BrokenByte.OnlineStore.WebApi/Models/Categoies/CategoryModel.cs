﻿namespace BrokenByte.OnlineStore.WebApi.Models.Categoies
{
    public class CategoryModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }
        public string? Image { get; set; }
    }
}
