﻿namespace BrokenByte.OnlineStore.WebApi.Models.Categoies
{
    /// <summary>
    /// Модель для обновления категории
    ///</summary>
    public class UpdatingCategoryModel
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }

        public string? Image { get; set; }
    }
}
