﻿namespace BrokenByte.OnlineStore.WebApi.Models.Categoies
{
    public class CreatingCategoryModel
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }

        public string? Image { get; set; }
    }
}
