﻿namespace BrokenByte.OnlineStore.WebApi.Models.Categoies
{
    public class CategoryFilterModel
    {
        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}
