﻿namespace BrokenByte.OnlineStore.Services.Contracts.Manufacturer
{
    /// <summary>
    /// Модель для фильтра товаров
    /// </summary>
    public class ProductFilterModel
    {

        public bool? Visible { get; set; }

        public int? CategoryId { get; set; }

        public int? ManufactoryId { get; set; }

        public bool? Available { get; set; }

        public decimal? PriceMin { get; set; }
        public decimal? PriceMax { get; set; }

        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}
