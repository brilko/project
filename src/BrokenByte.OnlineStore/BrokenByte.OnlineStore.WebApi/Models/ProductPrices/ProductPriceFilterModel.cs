﻿namespace BrokenByte.OnlineStore.WebApi.Models.ProductPrices
{
    public class ProductPriceFilterModel
    {
        /// <summary>
        /// Ид продукта
        /// </summary>
        public int ProductId { get; set; }


        /// <summary>
        /// Все начиная с даты. 
        /// </summary>
        public DateTime? From { get; set; }

        /// <summary>
        /// Сколько на странице
        /// </summary>
        public int ItemsPerPage { get; set; } = 10;

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int Page { get; set; } = 1;
    }
}
