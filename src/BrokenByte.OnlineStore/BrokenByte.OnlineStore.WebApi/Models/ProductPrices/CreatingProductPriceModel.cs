﻿namespace BrokenByte.OnlineStore.WebApi.Models.ProductPrices
{
    public class CreatingProductPriceModel
    {
        /// <summary>
        /// Цена на товар
        /// </summary>
        public decimal Price { get; set; }


        /// <summary>
        /// Время начала действия новой цены
        /// </summary>
        public DateTime? DateBegin { get; set; }


        /// <summary>
        /// Ид продукта
        /// </summary>
        public int ProductId { get; set; }
    }
}
