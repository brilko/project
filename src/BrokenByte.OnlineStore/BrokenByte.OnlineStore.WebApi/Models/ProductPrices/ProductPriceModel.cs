﻿namespace BrokenByte.OnlineStore.WebApi.Models.ProductPrices
{
    public class ProductPriceModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Цена на товар
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Время начала действия новой цены
        /// </summary>
        public DateTime DateBegin { get; set; }

        /// <summary>
        /// Ид товара
        /// </summary>
        public int ProductId { get; set; } = 0;
    }
}
