﻿using BrokenByte.OnlineStore.Services.Contracts.Products;
using BrokenByte.OnlineStore.WebApi.Models.Categoies;
using FluentValidation;

namespace BrokenByte.OnlineStore.WebApi.Validators
{

    public class CreatingProductModelValidator : AbstractValidator<CreatingProductModel>
    {
        public CreatingProductModelValidator()
        {
            RuleFor(x => x.ShortName).NotNull().NotEmpty();
        }
    }


    public class CategoryModelValidator : AbstractValidator<CategoryModel>
    {
        public CategoryModelValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();

        }
    }

    public class UpdatingCategoryModelValidator : AbstractValidator<UpdatingCategoryModel>
    {
        public UpdatingCategoryModelValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty().Length(1, 100);
            RuleFor(x => x.Description).MaximumLength(250).When(y => !string.IsNullOrEmpty(y.Description));
        }
    }
}
