﻿using BrokenByte.OnlineStore.WebApi.Models.Cart;
using FluentValidation;

namespace BrokenByte.OnlineStore.WebApi.Validators
{
    public class ProductInCartUpdateModelValidator : AbstractValidator<ProductInCartUpdateModel>
    {
        public ProductInCartUpdateModelValidator()
        {
            RuleFor(pc => pc.ProductId)
                .GreaterThanOrEqualTo(0);

            RuleFor(pc => pc.CustomerId)
                .GreaterThanOrEqualTo(0);

            RuleFor(pc => pc.Count)
                .GreaterThanOrEqualTo(0);
        }
    }
}
