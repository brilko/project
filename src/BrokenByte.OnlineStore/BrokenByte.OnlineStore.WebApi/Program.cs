using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.WebApi;
using FluentValidation.AspNetCore;
using IdentityServer4.AccessTokenValidation;

using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
        .AddIdentityServerAuthentication(options =>
        {
            // base-address of your identityserver
            options.Authority = "https://localhost:5001";
            // name of the API resource
            options.ApiName = "broken-byte-online-store-identity-server-api";
        });

// Add services to the container.
builder.Services.AddControllers();

//Add cors
var allowSpecificOrigins = "_myAllowSpecificOrigins";
AddCors(allowSpecificOrigins, builder);

//������������ ����� SharpGrip.FluentValidation.AutoValidation �������� ������������� ��
//https://docs.fluentvalidation.net/en/latest/aspnet.html.
//���� �� ������ https://github.com/SharpGrip/FluentValidation.AutoValidation
builder.Services.AddFluentValidationAutoValidation();


//AddLogging
builder.Services.AddLogging();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();


builder.Services.AddSwaggerGen(c =>
{
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});



var dbConnectionString = builder.Configuration.GetConnectionString("SqlLiteDb") ??
    throw new Exception("ConnectionString Not found.");
builder.Services.AddCustomServices(dbConnectionString);

builder.Services.AddResponseCaching();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseMiddleware(typeof(BrokenByte.OnlineStore.WebApi.Middleware.ErrorHandlingMiddleware));

app.UseRouting();

app.UseResponseCaching();

app.UseCors(allowSpecificOrigins);

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

#if DEBUG
DeleteAndCreateDataBase(app);
#endif

app.Run();


static void DeleteAndCreateDataBase(WebApplication app)
{
    using var scope = app.Services.CreateScope();
    var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
    context.Database.EnsureDeleted();
    context.Database.EnsureCreated();
}

static void AddCors(string allowSpecificOrigins, WebApplicationBuilder builder)
{
    builder.Services.AddCors(options =>
    {
        options.AddPolicy(name: allowSpecificOrigins,
            policy =>
            {
                policy.WithOrigins(builder.Configuration.GetSection("CORS:Origins").Get<string[]>() ?? [])
                .WithHeaders(builder.Configuration.GetSection("CORS:Headers").Get<string[]>() ?? [])
                .WithMethods(builder.Configuration.GetSection("CORS:Methods").Get<string[]>() ?? []);
            });
    });
}