﻿using AutoMapper;
using BrokenByte.OnlineStore._Services.Implementations;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations;
using BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Customers;
using BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Products;
using BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Storage;
using BrokenByte.OnlineStore.Services.Abstractions;
using BrokenByte.OnlineStore.Services.Contracts.Products;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Customers;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Order;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;
using BrokenByte.OnlineStore.WebApi.Mapping;
using BrokenByte.OnlineStore.WebApi.Models.Cart;
using BrokenByte.OnlineStore.WebApi.Models.Categoies;
using BrokenByte.OnlineStore.WebApi.Validators;
using FluentValidation;

namespace BrokenByte.OnlineStore.WebApi
{
    /// <summary>
    /// Выполняет регистрацию БД, маппинг и включает докуметацию на сфаггер
    /// </summary>
    public static class Registrar
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services,
            string dbConnectionString)
        {


            return services
                .ConfigureContext(dbConnectionString)
                .InstallMappers()
                .InstallServices()
                .InstallRepositories()
                .InstallValidators();


        }

        /// <summary>
        /// Добавление валидаторов по https://docs.fluentvalidation.net/en/latest/di.html
        /// </summary>
        private static IServiceCollection InstallValidators(this IServiceCollection services)
        {
            return services
                .AddScoped<IValidator<ProductInCartUpdateModel>, ProductInCartUpdateModelValidator>()
                .AddScoped<IValidator<UpdatingCategoryModel>, UpdatingCategoryModelValidator>()
                .AddScoped<IValidator<CreatingProductModel>, CreatingProductModelValidator>();

        }
        private static IServiceCollection InstallMappers(this IServiceCollection services)
        {
            return services
                .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ManufacturerMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.ManufacturerMappingsProfile>();
                cfg.AddProfile<OrderStatusMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.OrderStatusMappingsProfile>();
                cfg.AddProfile<ProductMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.ProductMappingsProfile>();
                cfg.AddProfile<CategoryMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.CategoryMappingsProfile>();
                cfg.AddProfile<ProductPriceMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.ProductPriceMappingsProfile>();
                cfg.AddProfile<OrderMappingProfile>();
                cfg.AddProfile<ProductInUsersCartMappingProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }

        private static IServiceCollection InstallServices(this IServiceCollection services)
        {
            return services
                .AddTransient<IManufacturerService, ManufacturerService>()
                .AddTransient<ICategoryService, CategoryService>()
                .AddTransient<IOrderStatusService, OrderStatusService>()
                .AddTransient<IProductService, ProductService>()
                .AddTransient<IProductPriceService, ProductPriceService>()
                .AddTransient<ICartService, CartService>();
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection services)
        {
            return services
                .AddTransient<IManufacturerRepository, ManufacturerRepository>()
                .AddTransient<ICategoryRepository, CategoryRepository>()
                .AddTransient<IProductRepository, ProductRepository>()
                .AddTransient<IReadOrderStatusRepository, ReadOrderStatusRepository>()
                .AddTransient<IProductPriceRepository, ProductPriceRepository>()
                .AddTransient<IStorageItemRepository, StorageItemRepository>()
                .AddTransient<IProductInCartRepository, ProductInCartRepository>()
                .AddTransient(typeof(IRepository<>), typeof(Repository<>))
                .AddTransient(typeof(IReadRepository<>), typeof(ReadRepository<>));
        }
    }
}
