﻿using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Domain.Entities.Deliveries;
using BrokenByte.OnlineStore.Domain.Entities.Employees;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Domain.Entities.Promos;
using BrokenByte.OnlineStore.Domain.Entities.Storages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BrokenByte.OnlineStore.Infrastructure.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        #region Product
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }

        public DbSet<Category> Categories { get; set; }
        #endregion


        #region Customer
        public DbSet<Customer> Customers { get; set; }
        public DbSet<ProductInCart> ProductsInCarts { get; set; }
        public DbSet<ProductInCart> ProductsInWishLists { get; set; }
        public DbSet<DeliveryAddress> DeliveryAddresses { get; set; }
        public DbSet<Recipient> Recipients { get; set; }
        #endregion

        #region Storages

        public DbSet<Storage> Storages { get; set; }
        public DbSet<StorageItem> StorageItems { get; set; }

        #endregion


        #region Order
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<DeliveryService> DeliveryServices { get; set; }
        #endregion


        #region PromoActions
        public DbSet<PromoAction> PromoActions { get; set; }
        public DbSet<PromoProduct> PromoProducts { get; set; }
        public DbSet<PromoActionType> PromoActionTypes { get; set; }
        #endregion


        #region Employee
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeDescription> EmployeeDescriptions { get; set; }
        public DbSet<EmployeeRole> EmployeeRole { get; set; }
        #endregion



        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            //1 ко многим между складом и товарами на складе
            modelBuilder.Entity<StorageItem>()
                .HasOne(e => e.Storage)
                .WithMany(e => e.StorageItems);


            //1 ко многим между категорией и товарами            
            modelBuilder.Entity<Product>()
                .HasOne(e => e.Category)
                .WithMany(e => e.Products);

            //1 ко многим между товаром и товарами на складах
            modelBuilder.Entity<StorageItem>()
                .HasOne(e => e.Product)
                .WithMany(e => e.StorageItems);


            //1 ко многим между товаром и картинками
            modelBuilder.Entity<ProductImage>()
                .HasOne(e => e.Product)
                .WithMany(e => e.Images);

            //1 ко многим между товаром и ценами
            modelBuilder.Entity<ProductPrice>()
                .HasOne(e => e.Product)
                .WithMany(e => e.Prices);

            //1 ко многим между производителм и товарами
            modelBuilder.Entity<Product>()
                .HasOne(e => e.Manufacturer)
                .WithMany(e => e.Products);



            //многие ко многим между товарами и корзинами покупателей
            modelBuilder.Entity<Product>()
                  .HasMany(e => e.CustomersWithProductInCart)
                  .WithMany(e => e.Cart)
                  .UsingEntity<ProductInCart>();

            //многие ко многим между товарами и вишлистами покупателей
            modelBuilder.Entity<Product>()
                  .HasMany(e => e.CustomersWithProductInWishList)
                  .WithMany(e => e.WishList)
                  .UsingEntity<ProductInWishList>();


            //1 ко многим между покупателем и заказами 
            modelBuilder.Entity<Order>()
               .HasOne(e => e.Customer)
               .WithMany(e => e.Orders);

            //1 ко многим между покупателем и адресами доставки
            modelBuilder.Entity<DeliveryAddress>()
               .HasOne(e => e.Customer)
               .WithMany(e => e.DeliveryAddresses);

            //1 ко многим между покупателем и получателями
            modelBuilder.Entity<Recipient>()
               .HasOne(e => e.Customer)
               .WithMany(e => e.Recipients);


            //1 ко многим между статусом заказа и заказом
            modelBuilder.Entity<Order>()
               .HasOne(e => e.OrderStatus)
               .WithMany(e => e.Orders);


            //1 ко многим между адресом доставки и заказами
            modelBuilder.Entity<Order>()
               .HasOne(e => e.DeliveryAddress)
               .WithMany(e => e.Orders);

            //1 ко многим между адресом доставки и получателем
            modelBuilder.Entity<Order>()
               .HasOne(e => e.Recipient)
               .WithMany(e => e.Orders);

            //многие ко многим между товарами и заказми
            modelBuilder.Entity<Product>()
                  .HasMany(e => e.Orders)
                  .WithMany(e => e.Products)
                  .UsingEntity<OrderItem>();



            modelBuilder.HasDataExtension(FakeDataFactory.Customers)
                .HasDataExtension(FakeDataFactory.Manufacturers)
                .HasDataExtension(FakeDataFactory.Products)
                .HasDataExtension(FakeDataFactory.DeliveryAddresses)
                .HasDataExtension(FakeDataFactory.ProductInWishLists)
                .HasDataExtension(FakeDataFactory.Recipients)
                .HasDataExtension(FakeDataFactory.DeliveryServices)
                .HasDataExtension(FakeDataFactory.Employees)
                .HasDataExtension(FakeDataFactory.EmployeeDescriptions)
                .HasDataExtension(FakeDataFactory.EmployeeRoles)
                .HasDataExtension(FakeDataFactory.Orders)
                .HasDataExtension(FakeDataFactory.OrderItems)
                .HasDataExtension(FakeDataFactory.OrderStatuses)
                .HasDataExtension(FakeDataFactory.Categories)
                .HasDataExtension(FakeDataFactory.ProductPrices)
                .HasDataExtension(FakeDataFactory.PromoActions)
                .HasDataExtension(FakeDataFactory.PromoActionTypes)
                .HasDataExtension(FakeDataFactory.PromoProducts)
                .HasDataExtension(FakeDataFactory.Storages)
                .HasDataExtension(FakeDataFactory.StorageItems)
                .HasDataExtension(FakeDataFactory.ProductInCarts);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

    }
}