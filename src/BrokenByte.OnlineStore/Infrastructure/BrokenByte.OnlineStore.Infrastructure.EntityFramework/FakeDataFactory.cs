﻿using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Domain.Entities.Deliveries;
using BrokenByte.OnlineStore.Domain.Entities.Employees;
using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Domain.Entities.Promos;
using BrokenByte.OnlineStore.Domain.Entities.Storages;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.EntityFramework
{
    public static class FakeDataFactory
    {
        public static ModelBuilder HasDataExtension<T>(this ModelBuilder builder, List<T> data)
            where T : class
        {
            builder.Entity<T>().HasData(data);
            return builder;
        }

        #region CustomersGroup
        public static List<Customer> Customers => new()
        {
            customersElsaVendor,
            customerIvanBordof,
            new Customer()
            {
                Id = 3,
                Login = "ManyLetters",
                Password = "password",
                FirstName = "Eveniy",
                LastName = "Simonov",
                MiddleName = "Akakievich",
                Email = "TutVamNeTam@Zdes.vse"
            },
            new Customer()
            {
                Id = 4,
                Login = "Anyone",
                Password = "password",
                FirstName = "Roman",
                LastName = "Prohodilov",
                Email = "roman@empire.rim"
            }
        };
        private static readonly Customer customersElsaVendor = new()
        {
            Id = 1,
            Login = "ElVen",
            Password = "password",
            FirstName = "Elsa",
            LastName = "Vendor",
            MiddleName = null,
            Email = "email@cool.com",
        };
        private static readonly Customer customerIvanBordof = new()
        {
            Id = 2,
            Login = "DoHust",
            Password = "password",
            FirstName = "Ivan",
            LastName = "Bordof",
            MiddleName = "Alimkoszoed",
            Email = "yaTut@coin.money",
        };

        public static List<DeliveryAddress> DeliveryAddresses => new()
        {
            deliveryAddressElsaVendorWork,
            deliveryAddressElsaVendorHome,
            deliveryAdderssIvanBordof,
            new DeliveryAddress()
            {
                Id = 4,
                Address = "Противоположный переулок",
                CustomerId = customerIvanBordof.Id,
            }
        };
        private static readonly DeliveryAddress deliveryAddressElsaVendorWork = new()
        {
            Id = 1,
            Address = "проспект Новокузнецкий, строение 1, корпус 5",
            CustomerId = customersElsaVendor.Id,
        };
        private static readonly DeliveryAddress deliveryAddressElsaVendorHome = new()
        {
            Id = 2,
            Address = "Домик у озера",
            CustomerId = customersElsaVendor.Id,
        };
        private static readonly DeliveryAddress deliveryAdderssIvanBordof = new()
        {
            Id = 3,
            Address = "Новокузнецкий мост 8",
            CustomerId = customerIvanBordof.Id,
        };

        public static List<ProductInCart> ProductInCarts => new()
        {
            /*new ProductInCart()
            {
                Id = 1,
                CustomerId = customerIvanBordof.Id,
                ProductId = productRedJacket.Id,
                Count = 2
            },*/
            new ProductInCart()
            {
                Id = 2,
                CustomerId = customerIvanBordof.Id,
                ProductId = productBlueBoots.Id,
                Count = 1
            },
            /*new ProductInCart()
            {
                Id = 3,
                CustomerId = customersElsaVendor.Id,
                ProductId = productBlueJacket.Id,
                Count = 1
            },
            new ProductInCart()
            {
                Id = 4,
                CustomerId = customersElsaVendor.Id,
                ProductId = productRedJacket.Id,
                Count = 1
            }*/
        };
        public static List<ProductInWishList> ProductInWishLists => new()
        {
            /* new ProductInWishList()
             {
                 Id = 1,
                 CustomerId = customerIvanBordof.Id,
                 ProductId = productRedJacket.Id
             },
             new ProductInWishList()
             {
                 Id = 2,
                 CustomerId = customerIvanBordof.Id,
                 ProductId = productBlueJacket.Id
             },
             new ProductInWishList()
             {
                 Id = 3,
                 CustomerId = customerIvanBordof.Id,
                 ProductId = productBlueBoots.Id
             },
             new ProductInWishList()
             {
                 Id = 4,
                 CustomerId = customersElsaVendor.Id,
                 ProductId = productBlueJacket.Id
             }*/
        };
        public static List<Recipient> Recipients => new()
        {
            recipientEvgeniyViktorovichVolodin,
            recipientKartapolovViktor,
            recipientKudakinVasiliyIvanovich,
            new Recipient()
            {
                Id = 4,
                CustomerId = customerIvanBordof.Id,
                FullName = "Kristin Lagart",
                Phone = "+2-888-555-57-00"
            }
        };
        private static readonly Recipient recipientEvgeniyViktorovichVolodin = new()
        {
            Id = 1,
            FullName = "Evgeniy Viktorovich Volodin",
            CustomerId = customerIvanBordof.Id,
            Phone = "+7-999-888-77-66"
        };
        private static readonly Recipient recipientKartapolovViktor = new()
        {
            Id = 2,
            FullName = "Kartapolov Viktor",
            CustomerId = customerIvanBordof.Id,
            Phone = "+7-111-555-44-66"
        };
        private static readonly Recipient recipientKudakinVasiliyIvanovich = new()
        {
            Id = 3,
            FullName = "Kudakin Vasiliy Ivanovich",
            CustomerId = customersElsaVendor.Id,
            Phone = "+7-888-222-00-33",
        };
        #endregion

        #region DeliveriesGroup
        public static List<DeliveryService> DeliveryServices => new()
        {
            deliveryServiceFast,
            deliveryServiceHigh,
            new DeliveryService()
            {
                Id = 3,
                Address = "Ленина улица 4",
                Description = "Хороший сервис",
                Name = "GoodTravel",
                Phone = "+7-999-000-444-58-03"
            }
        };
        private static readonly DeliveryService deliveryServiceFast = new()
        {
            Id = 1,
            Address = "Вышеградская улица",
            Description = "Быстродел",
            Email = "Vike@groom.kz",
            Name = "Быстродел",
            Phone = "+8-999-888-55-33"
        };
        private static readonly DeliveryService deliveryServiceHigh = new()
        {
            Id = 2,
            Address = "Новодевичья гора, высота 300 тыс метров",
            Description = "Высоко забрадись",
            Email = "HighLoad@RDR.world",
            Name = "Ор выше гор",
            Phone = "+7-222-654-85-25"
        };
        #endregion

        #region EmployeesGroup
        public static List<EmployeeRole> EmployeeRoles => new()
        {
            employeeRoleManager,
            employeeRoleMover
        };
        private static readonly EmployeeRole employeeRoleManager = new()
        {
            Id = 1,
            Description = "Управляющий",
            Name = "Manager",
        };
        private static readonly EmployeeRole employeeRoleMover = new()
        {
            Id = 2,
            Description = "Грузчик",
            Name = "Mover"
        };

        public static List<Employee> Employees => new()
        {
            employeeBoss,
            employeeMover
        };
        private static readonly Employee employeeBoss = new()
        {
            Id = 1,
            Email = "Best@Boss.Ever",
            EmployeeRoleId = employeeRoleManager.Id,
            FirstName = "Vlad",
            MiddleName = "Dronovich",
            LastName = "Krovov",
            Login = "NotVampire",
            Password = "password",
        };
        private static readonly Employee employeeMover = new()
        {
            Id = 2,
            Email = "Good@mover.fr",
            EmployeeRoleId = employeeRoleMover.Id,
            FirstName = "Dima",
            MiddleName = "Dobrynevich",
            LastName = "Lastov",
            Login = "LastaDobra",
            Password = "password",
        };

        public static List<EmployeeDescription> EmployeeDescriptions => new()
        {
            new EmployeeDescription()
            {
                Id = 1,
                Address = "Времевский уступ",
                EmployeeId = employeeBoss.Id,
                MessengerAccount = "WhatsUp",
                SocialMediaAccount = "htmlToAccount"
            },
            new EmployeeDescription()
            {
                Id = 2,
                Address = "улица Взлетевшего клина 1",
                EmployeeId = employeeMover.Id,
                MessengerAccount = "Telegram",
                SocialMediaAccount = "toAccount"
            }
        };

        #endregion

        #region OrdersGroup
        public static List<OrderStatus> OrderStatuses => new()
        {
            orderStatusOk,
            orderStatusProblem,
            new OrderStatus()
            {
                Id = 3,
                Description = "Delayed",
                Name = "Delayed",
            }
        };
        private static readonly OrderStatus orderStatusOk = new()
        {
            Id = 1,
            Description = "OK",
            Name = "OK",
        };
        private static readonly OrderStatus orderStatusProblem = new()
        {
            Id = 2,
            Description = "Problem",
            Name = "Problem",
        };

        public static List<Order> Orders => new()
        {
            orderKudakinVasiliyIvanovich,
            orderKartapolovViktorOk,
            orderKartapolovViktorProblem,
            orderEvgeniyViktorovichVolodin
        };
        private static readonly Order orderKudakinVasiliyIvanovich = new()
        {
            Id = 1,
            AllSumTransaction = 100,
            CreatedAt = DateTime.Now.AddDays(-20),
            CustomerId = customersElsaVendor.Id,
            DeliveryAddressId = deliveryAddressElsaVendorHome.Id,
            DeliveryServiceId = deliveryServiceFast.Id,
            Description = "Новая доставка",
            NumberInvoice = "555869",
            OrderStatusId = orderStatusOk.Id,
            PaymentAt = DateTime.Now.AddDays(-10),
            PaymentInfo = "Полная оплата",
            RecipientId = recipientKudakinVasiliyIvanovich.Id,
            TotalPrice = 100
        };
        private static readonly Order orderKartapolovViktorOk = new()
        {
            Id = 2,
            AllSumTransaction = 150,
            CreatedAt = DateTime.Now.AddDays(-15),
            CustomerId = customerIvanBordof.Id,
            DeliveryAddressId = deliveryAdderssIvanBordof.Id,
            DeliveryServiceId = deliveryServiceFast.Id,
            Description = "Полная оплата",
            NumberInvoice = "454891",
            OrderStatusId = orderStatusOk.Id,
            PaymentAt = DateTime.Now.AddDays(-10),
            PaymentInfo = "Полная оплата",
            RecipientId = recipientKartapolovViktor.Id,
            TotalPrice = 150
        };
        private static readonly Order orderKartapolovViktorProblem = new()
        {
            Id = 3,
            AllSumTransaction = 300,
            CreatedAt = DateTime.Now.AddDays(-1),
            CustomerId = customerIvanBordof.Id,
            DeliveryAddressId = deliveryAdderssIvanBordof.Id,
            DeliveryServiceId = deliveryServiceFast.Id,
            Description = "Полная оплата",
            NumberInvoice = "2537582",
            OrderStatusId = orderStatusProblem.Id,
            PaymentAt = DateTime.Now,
            PaymentInfo = "Полная оплата",
            RecipientId = recipientKartapolovViktor.Id,
            TotalPrice = 300
        };
        private static readonly Order orderEvgeniyViktorovichVolodin = new()
        {
            Id = 4,
            AllSumTransaction = 50,
            CreatedAt = DateTime.Now.AddDays(-8),
            CustomerId = customerIvanBordof.Id,
            DeliveryAddressId = deliveryAdderssIvanBordof.Id,
            DeliveryServiceId = deliveryServiceHigh.Id,
            Description = "OK",
            NumberInvoice = "544613",
            OrderStatusId = orderStatusOk.Id,
            PaymentAt = DateTime.Now,
            PaymentInfo = "Ok",
            RecipientId = recipientEvgeniyViktorovichVolodin.Id,
            TotalPrice = 50
        };

        public static List<OrderItem> OrderItems => new()
        {
            new OrderItem()
            {
                Id = 1,
                Quantity = 1,
                OrderId = orderKudakinVasiliyIvanovich.Id,
                PricePerOne = 60,
                ProductId = productBlueBoots.Id,
                TotalPrice = 60
            },
            /*new OrderItem()
            {
                Id = 2,
                Quantity = 2,
                OrderId = orderKudakinVasiliyIvanovich.Id,
                PricePerOne = 55,
                ProductId = productBlueJacket.Id,
                TotalPrice = 110
            },
            new OrderItem()
            {
                Id = 3,
                Quantity = 1,
                OrderId = orderKartapolovViktorOk.Id,
                PricePerOne = 30,
                ProductId = productBlueJacket.Id,
                TotalPrice = 30
            },
            new OrderItem()
            {
                Id = 4,
                Quantity = 30,
                OrderId = orderKartapolovViktorProblem.Id,
                PricePerOne = 100,
                ProductId = productRedJacket.Id,
                TotalPrice = 3000
            },*/
            new OrderItem()
            {
                Id = 5,
                Quantity = 1,
                OrderId = orderEvgeniyViktorovichVolodin.Id,
                PricePerOne = 100,
                ProductId = productBlueBoots.Id,
                TotalPrice = 100
            }
        };
        #endregion

        #region ProductsGroup
        public static List<Category> Categories => new()
        {
            categoryJackets,
            categoryBoots,
            categoryWaistcoat,
            new Category()
            {
                Id = 4,
                Name = "Футболки",
                Image= "tshirt.jpg"
            },
            new Category()
            {
                Id = 5,
                Name = "Брюки",
                Image= "trousers.jpg"
            },
            new Category()
            {
                Id = 6,
                Name = "Толстовки",
                Image= "jumper.jpg"
            }
        };
        private static readonly Category categoryJackets = new()
        {
            Id = 1,
            Name = "Куртки",
            Image = "coat.jpg"
        };
        private static readonly Category categoryBoots = new()
        {
            Id = 2,
            Name = "Ботинки",
            Image = "boots.jpg"
        };
        private static readonly Category categoryWaistcoat = new()
        {
            Id = 3,
            Name = "Жилеты",
            Image = "waistcoat.jpg"
        };


        public static List<Manufacturer> Manufacturers => new()
        {
            manufacturerChina,
            manufacturerLenina
        };
        private static readonly Manufacturer manufacturerChina = new()
        {
            Id = 1,
            Name = "Китай близко",
        };
        private static readonly Manufacturer manufacturerLenina = new()
        {
            Id = 2,
            Name = "Московский Красный завод нанокурток имени Ленина"
        };

        public static List<Product> Products => CreateFackeCoats();

        /*private static readonly Product productRedJacket = new()
        {
            Id = 1,
            Article = "69031461",
            CategoryId = categoryJackets.Id,
            ShortName = "Красная куртка",
            Image= "coat.jpg",
            ManufacturerId = manufacturerChina.Id,
            Description = "Красная куртка из Китая",
            FullName = "Красная куртка из Китая",
        };
        private static readonly Product productBlueJacket = new()
        {
            Id = 2,
            Article = Guid.NewGuid().ToString(),
            CategoryId = categoryJackets.Id,
            ShortName = "Белая куртка",
            Image = "coat1.png",
            ManufacturerId = manufacturerLenina.Id,
            Description = "Белая куртка, теплая, красива и прочая фигня. Тут каое-то описание продукта",
            FullName = "Белая куртка, произведена в Москве",
        };*/

        private static List<Product> CreateFackeCoats()
        {
            Random rnd = new Random();
            List<Product> list = new List<Product>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(new Product()
                {
                    Id = 1 + i,
                    CategoryId = categoryJackets.Id,
                    ShortName = $"Куртка #{i}",
                    Image = $"coat{i}.png",
                    ManufacturerId = manufacturerLenina.Id,
                    Description = $"Куртка {i}, теплая, красивая и прочая фигня. Тут каое-то описание продукта",
                    FullName = $"Куртка {i}, произведена в Москве",
                });
            }
            return list;
        }


        private static readonly Product productBlueBoots = new()
        {
            Id = 3,
            CategoryId = categoryBoots.Id,
            ShortName = "Синие ботинки",
            ManufacturerId = manufacturerLenina.Id,
            Description = "Синие ботинки, произведены в Москве",
            FullName = "Синие ботинки, произведены в Москве",
        };

        private static readonly Product productGreenBoots = new()
        {
            Id = 4,
            CategoryId = categoryBoots.Id,
            ShortName = "Зеленые ботинки",
            ManufacturerId = manufacturerChina.Id,
            Description = "Зелёные ботинки из Китая",
            FullName = "Зелёные ботинки из Китая"
        };


        private static readonly Product productJacket1 = new()
        {
            Id = 5,
            CategoryId = categoryJackets.Id,
            ShortName = "Коричневая куртка",
            Image = "coat2.png",
            ManufacturerId = manufacturerLenina.Id,
            Description = "Коричневая куртка, теплая, красива и прочая фигня. Тут каое-то описание продукта",
            FullName = "Коричневая куртка, произведена в Москве",
        };
        private static readonly Product productJacket2 = new()
        {
            Id = 6,
            CategoryId = categoryJackets.Id,
            ShortName = "Черная куртка",
            Image = "coat3.png",
            ManufacturerId = manufacturerLenina.Id,
            Description = "Черная куртка, теплая, красива и прочая фигня. Тут каое-то описание продукта",
            FullName = "Черная куртка, произведена в Москве",
        };

        private static readonly Product productJacket4 = new()
        {
            Id = 6,
            CategoryId = categoryJackets.Id,
            ShortName = "Черная куртка",
            Image = "coat3.png",
            ManufacturerId = manufacturerLenina.Id,
            Description = "Черная куртка, теплая, красива и прочая фигня. Тут каое-то описание продукта",
            FullName = "Черная куртка, произведена в Москве",
        };


        private static List<ProductPrice> CreateFackePrices()
        {
            Random rnd = new Random();
            List<ProductPrice> list = new List<ProductPrice>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(new ProductPrice()
                {
                    Id = 1 + i,
                    DateBegin = new DateTime(2023, 1, 1),
                    Deleted = false,
                    Price = rnd.Next(300000, 5000000),
                    ProductId = 1 + i
                });
            }
            return list;
        }


        public static List<ProductPrice> ProductPrices => CreateFackePrices();
        /* new()
  {
        new ProductPrice()
        {
           Id = 1,
           DateBegin=new DateTime(2023, 1, 1),
           Deleted=false,
           Price=10000,
           ProductId=productRedJacket.Id,
        },
        new ProductPrice()
        {
           Id = 2,
           DateBegin=new DateTime(2023, 1, 1),
           Deleted=false,
           Price=20000,
           ProductId=productBlueJacket.Id,
        },
        new ProductPrice()
        {
           Id = 3,
           DateBegin = new DateTime(2023, 1, 1),
           Deleted = false,
           Price = 3000,
           ProductId = productBlueBoots.Id,
        },
        new ProductPrice()
        {
           Id = 4,
           DateBegin=new DateTime(2023, 1, 1),
           Deleted=false,
           Price=4000,
           ProductId=productGreenBoots.Id
        },
    };*/
        #endregion

        #region PromosGroup
        public static List<PromoActionType> PromoActionTypes => new()
        {
            promoActionTypeChildren
        };
        private static readonly PromoActionType promoActionTypeChildren = new()
        {
            Id = 1,
            Description = "Детская",
            Name = "Детская"
        };
        public static List<PromoAction> PromoActions => new()
        {
            promoActionChildren
        };
        private static readonly PromoAction promoActionChildren = new()
        {
            Id = 1,
            Description = "Детская",
            Discount = 200,
            DiscountPersent = 20,
            EmployeeId = employeeBoss.Id,
            FromDate = DateTime.Now.AddDays(-50),
            IsActive = false,
            Name = "Детская",
            PromoActionTypeId = promoActionTypeChildren.Id,
            PromoCode = "6494613",
            ToDate = DateTime.Now.AddDays(-10),
        };
        public static List<PromoProduct> PromoProducts => new()
        {
            new PromoProduct()
            {
                Id = 1,
                ProductId = productGreenBoots.Id,
                PromoActionId = promoActionChildren.Id,
            }
        };
        #endregion

        #region StoragesGroup
        public static List<Storage> Storages => new()
        {
            new Storage()
            {
                Id = 1,
                Name = "Магазин №1",
                Address = "г. Нск, ул. Ленина, 45",
            }
        };

        public static List<StorageItem> StorageItems => new()
        {
            new StorageItem()
            {
               Id = 1,
               Expected=0,
               Quantity=20,
               Reserved=3,
               ProductId=1,
               StorageId=1,
            },
            new StorageItem()
            {
                Id = 2,
                Expected=0,
                Quantity=10,
                Reserved=8,
                StorageId=1,
                ProductId=2,
            },
            new StorageItem()
            {
                Id = 3,
                Expected=0,
                Quantity=10,
                Reserved=8,
                StorageId=1,
                ProductId=3,
            },
            new StorageItem()
            {
                Id = 4,
                Expected=0,
                Quantity=10,
                Reserved=8,
                StorageId=1,
                ProductId=4,
            }
        };
        #endregion Storage
    }
}