﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BrokenByte.OnlineStore.Infrastructure.EntityFramework
{
    public static class EntityFrameworkInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<DatabaseContext>(optionsBuilder
                 => optionsBuilder
                     .EnableSensitiveDataLogging()
                     .UseSqlite(connectionString));
            //.UseSqlServer(connectionString));

            return services;
        }
    }
}
