﻿using BrokenByte.OnlineStore.Domain.Entities;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий.
    /// </summary>
    /// <typeparam name="T"> Тип сущности. </typeparam>
    public class Repository<T> : ReadRepository<T>, IRepository<T> where T
        : BaseEntity
    {
        public Repository(DatabaseContext context) : base(context) { }

        #region Create
        /// <summary>
        /// Добавить в базу одну сущность.
        /// </summary>
        /// <param name="entity"> Сущность для добавления. </param>
        /// <returns> Добавленная сущность. </returns>
        public virtual async Task<T> AddAsync(T entity)
        {
            return (await entitySet.AddAsync(entity)).Entity;
        }

        /// <summary>
        /// Добавить в базу массив сущностей.
        /// </summary>
        /// <param name="entities"> Массив сущностей. </param>
        public virtual async Task AddRangeAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }
            await entitySet.AddRangeAsync(entities);
        }

        #endregion

        #region Update
        public async Task<bool> UpdateAsync(T entity)
        {
            if (!await IsExist(entity.Id))
                return false;
            await Task.Run(() => entitySet.Update(entity));
            return true;
        }
        #endregion

        #region Delete
        public async Task<bool> TryDeleteAsync(long id)
        {
            return await Task.Run(() =>
            {
                var obj = entitySet.Find(id);
                if (obj == null)
                {
                    return false;
                }
                entitySet.Remove(obj);
                return true;
            });
        }

        public async Task<bool> TryDeleteAsync(T entity)
        {
            return await Task.Run(() =>
            {
                if (entity == null)
                {
                    return false;
                }
                Context.Entry(entity).State = EntityState.Deleted;
                return true;
            });
        }

        public async Task<bool> TryDeleteRangeAsync(ICollection<T> entities)
        {
            return await Task.Run(() =>
            {
                if (entities == null || !entities.Any())
                {
                    return false;
                }
                foreach (var entity in entities)
                {
                    Context.Entry(entity).State = EntityState.Deleted;
                }
                //_entitySet.RemoveRange(entities);
                return true;
            });
        }

        #endregion

        #region SaveChanges
        /// <summary>
        /// Сохранить изменения.
        /// </summary>
        public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await Context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IsExist
        public async Task<bool> IsExist(long id)
        {
            return await entitySet
                .Select(e => e.Id)
                .ContainsAsync(id);
        }
        #endregion
    }
}