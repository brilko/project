﻿using BrokenByte.OnlineStore.Domain.Entities;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations
{
    public class ReadRepository<T> : IReadRepository<T> where T
        : BaseEntity
    {
        protected readonly DbContext Context;
        protected readonly DbSet<T> entitySet;

        public ReadRepository(DatabaseContext context)
        {
            Context = context;
            entitySet = Context.Set<T>();
        }

        #region Get

        /// <summary>
        /// Получить сущность по Id.
        /// </summary>
        /// <param name="id"> Id сущности. </param>
        /// <returns> Cущность. </returns>
        public virtual async Task<T?> GetAsync(long id)
        {
            return await entitySet.FindAsync(id);
        }

        #endregion

        #region GetAll

        /// <summary>
        /// Запросить все сущности в базе.
        /// </summary>
        /// <param name="asNoTracking"> Вызвать с AsNoTracking. </param>
        /// <returns> IQueryable массив сущностей. </returns>
        public virtual IQueryable<T> GetAll(bool asNoTracking = false)
        {
            return asNoTracking ? entitySet.AsNoTracking() : entitySet;
        }

        /// <summary>
        /// Запросить все сущности в базе.
        /// </summary>
        /// <param name="cancellationToken"> Токен отмены </param>
        /// <param name="asNoTracking"> Вызвать с AsNoTracking. </param>
        /// <returns> Список сущностей. </returns>
        public async Task<List<T>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false)
        {
            return await GetAll().ToListAsync(cancellationToken);
        }

        public async Task<List<T>> GetPageAsync(Page page)
        {
            return await entitySet
                .Skip(page.PageIndex * page.ItemsPerPage)
                .Take(page.ItemsPerPage)
                .ToListAsync();
        }
        #endregion
    }
}
