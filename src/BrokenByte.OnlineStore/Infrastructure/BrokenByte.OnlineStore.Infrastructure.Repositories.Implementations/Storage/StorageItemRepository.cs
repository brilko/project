﻿using BrokenByte.OnlineStore.Domain.Entities.Storages;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Storage
{
    public class StorageItemRepository : Repository<StorageItem>, IStorageItemRepository
    {
        public StorageItemRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
