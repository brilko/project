﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Contracts.ProductPrice;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Products
{
    public class ProductPriceRepository : Repository<ProductPrice>, IProductPriceRepository
    {
        public ProductPriceRepository(DatabaseContext context) : base(context)
        {
        }
        /// <summary>
        /// Получить все цены на товар, можно с датой применения с указанной
        /// </summary>
        /// <param name="productId">ид товара</param>
        /// <param name="dt">дата применения</param>
        /// <returns>Список цен</returns>
        public async Task<ICollection<ProductPrice>> GetAllAsync(long productId, DateTime? dt)
        {
            var query = GetAll().Where(c => (c.ProductId == productId && (!c.Deleted)));
            if (dt != null)
            {
                query = query.Where(c => c.DateBegin > dt);
            }
            return await query.ToListAsync();
        }

        public Task<ProductPrice?> GetCurrentPrice(long productId)
        {
            var query = GetAll()
             .Where(c => !c.Deleted && (c.ProductId == productId) && (c.DateBegin < DateTime.Now))
             .OrderByDescending(f => f.DateBegin).FirstOrDefault();


            return Task.FromResult(query);
        }

        public async Task<List<ProductPrice>> GetPagedAsync(ProductPriceFilterDto filterDto)
        {
            var query = GetAll().Where(c => !c.Deleted && c.ProductId == filterDto.ProductId);
            if (filterDto.From != null)
            {
                query = query.Where(c => c.DateBegin > filterDto.From).OrderBy(f => f.DateBegin);
            }
            if (filterDto.Page != 0 && filterDto.ItemsPerPage != 0)
            {
                query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage).OrderBy(b => b.Id);
            }

            var result = await query.ToListAsync();
            return result.ToList();
        }
    }
}
