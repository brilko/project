﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Contracts.Manufacturer;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Products
{
    public class ManufacturerRepository : Repository<Manufacturer>, IManufacturerRepository
    {
        public ManufacturerRepository(DatabaseContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список произваодителей. </returns>
        public async Task<List<Manufacturer>> GetPagedAsync(ManufacturerFilterDto filterDto)
        {
            var query = GetAll()
                .Where(c => !c.Deleted);

            if (!string.IsNullOrWhiteSpace(filterDto.Name))
            {
                query = query.Where(c => c.Name.StartsWith(filterDto.Name));
            }

            query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage);

            var result = await query.ToListAsync();
            return result.ToList();
        }
    }

}
