﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Contracts.Category;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Products
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(DatabaseContext context) : base(context) { }

        public async Task<List<Category>> GetPagedAsync(CategoryFilterDto filterDto)
        {
            var query = GetAll().Where(c => !c.Deleted);

            query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage);

            var result = await query.ToListAsync();
            return result.ToList();
        }
    }
}
