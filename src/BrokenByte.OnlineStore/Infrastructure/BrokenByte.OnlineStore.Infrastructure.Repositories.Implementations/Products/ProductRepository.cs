﻿using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Contracts.Product;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Products;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Products
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(DatabaseContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список произваодителей. </returns>
        public async Task<List<Product>> GetPagedAsync(ProductFilterDto filterDto)
        {
            var query = GetAll().Where(c => !c.Deleted);
            if (filterDto.Visible != null)
            {
                query = query.Where(c => c.Visible == filterDto.Visible);
            }

            if (filterDto.CategoryId > 0)
            {
                query = query.Where(c => c.CategoryId == filterDto.CategoryId);
            }

            query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage);

            var result = await query.ToListAsync();
            return result.ToList();
        }
    }
}
