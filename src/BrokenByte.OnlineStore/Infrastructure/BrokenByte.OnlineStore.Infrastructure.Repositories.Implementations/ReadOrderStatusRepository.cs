﻿using BrokenByte.OnlineStore.Domain.Entities.Orders;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Order;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations
{
    public class ReadOrderStatusRepository : ReadRepository<OrderStatus>, IReadOrderStatusRepository
    {
        public ReadOrderStatusRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
