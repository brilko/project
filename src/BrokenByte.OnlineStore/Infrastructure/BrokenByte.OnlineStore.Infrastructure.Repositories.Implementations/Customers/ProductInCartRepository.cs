﻿using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Infrastructure.EntityFramework;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Customers;
using Microsoft.EntityFrameworkCore;

namespace BrokenByte.OnlineStore.Infrastructure.Repositories.Implementations.Customers
{
    public class ProductInCartRepository : Repository<ProductInCart>, IProductInCartRepository
    {
        public ProductInCartRepository(DatabaseContext context) : base(context) { }

        public async Task<ProductInCart?> GetByProductIdAndCustomerId(long ProductId, long CustomerId)
        {
            return await entitySet
                .Where(pc => pc.ProductId == ProductId)
                .Where(pc => pc.CustomerId == CustomerId)
                .FirstOrDefaultAsync();
        }

        public async Task<List<ProductInCart>> GetProductsInCustomersCartIncludePrices(long customerId)
        {
            return await entitySet
                .Where(pc => pc.CustomerId == customerId)
                .Include(pc => pc.Product!.Category)
                .Include(pc => pc.Product!.Manufacturer)
                .ToListAsync();
        }
    }
}
