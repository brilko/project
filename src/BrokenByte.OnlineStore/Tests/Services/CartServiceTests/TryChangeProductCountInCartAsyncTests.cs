﻿using BrokenByte.OnlineStore._Services.Implementations;
using BrokenByte.OnlineStore.Domain.Entities.Customers;
using BrokenByte.OnlineStore.Domain.Entities.Products;
using BrokenByte.OnlineStore.Services.Contracts.ProductInCart;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions;
using BrokenByte.OnlineStore.Services.Repositories.Abstractions.Customers;

namespace Tests.Services.CartServiceTests
{
    public class TryChangeProductCountInCartAsyncTests
    {
        private readonly CartService cartService;
        private readonly Mock<IProductInCartRepository> productInCartRepositoryMock;
        private readonly Mock<IRepository<Product>> productRepositoryMock;
        private readonly Mock<IRepository<Customer>> customerRepositoryMock;
        private readonly IFixture fixture;
        private readonly long customerId;
        private readonly long productId;
        private readonly ProductInCartUpdateDto updateDtoCountZero;
        private readonly ProductInCartUpdateDto updateDtoCountOne;
        private readonly ProductInCart productInCart;


        public TryChangeProductCountInCartAsyncTests()
        {
            fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
            productInCartRepositoryMock = fixture.Freeze<Mock<IProductInCartRepository>>();
            productRepositoryMock = fixture.Freeze<Mock<IRepository<Product>>>();
            customerRepositoryMock = fixture.Freeze<Mock<IRepository<Customer>>>();
            cartService = fixture.Create<CartService>();
            customerId = 1;
            productId = 1;
            updateDtoCountZero = new()
            {
                Count = 0,
                CustomerId = customerId,
                ProductId = productId
            };
            updateDtoCountOne = new()
            {
                Count = 1,
                CustomerId = customerId,
                ProductId = productId
            };
            productInCart = new()
            {
                CustomerId = customerId,
                ProductId = productId,
                Count = 1
            };
        }

        #region ArrangeConditions
        private void ProductInCartIsExist()
        {
            productInCartRepositoryMock
                .Setup(repo => repo.GetByProductIdAndCustomerId(productId, customerId))
                .Returns(Task.FromResult<ProductInCart?>(productInCart));
        }
        private void ProductInCartNotExist()
        {
            productInCartRepositoryMock
                .Setup(repo => repo.GetByProductIdAndCustomerId(productId, customerId))
                .Returns(Task.FromResult<ProductInCart?>(null));
        }
        private void ProductInCartIsDeletable()
        {
            productInCartRepositoryMock
                .Setup(repo => repo.TryDeleteAsync(productInCart))
                .Returns(Task.FromResult(true));
        }
        private void SetProductExistance(bool isExist)
        {
            productRepositoryMock
                .Setup(repo => repo.IsExist(updateDtoCountOne.ProductId))
                .Returns(Task.FromResult(isExist));
        }
        private void SetCustomerExistance(bool isExist)
        {
            customerRepositoryMock
                .Setup(repo => repo.IsExist(updateDtoCountOne.CustomerId))
                .Returns(Task.FromResult(isExist));
        }
        #endregion

        [Fact]
        public async void ProductInCartExistCountProductInDtoUpdateIsZero_ProductInCartDeletedReturnTrue()
        {
            //Arrange
            ProductInCartIsExist();

            ProductInCartIsDeletable();

            //Act
            var result = await cartService.TryChangeProductCountInCartAsync(updateDtoCountZero);

            //Asset
            result.Should().BeTrue();
        }

        [Fact]
        public async void ProductInCartExistAndCountProductInDtoNotZero_ProductInCartUpdatedAndReturnTrue()
        {
            //Arrange
            ProductInCartIsExist();

            //Act
            var result = await cartService.TryChangeProductCountInCartAsync(updateDtoCountOne);

            //Asset
            result.Should().BeTrue();
        }

        [Theory]
        [InlineData(false, false)]
        [InlineData(false, true)]
        [InlineData(true, false)]
        public async void ProductInCartNotExistAndCustomerOrProductNotExist_ReturnFalse(
            bool isProductExist, bool isCustomerExist)
        {
            //Arrange
            ProductInCartNotExist();

            SetProductExistance(isProductExist);

            SetCustomerExistance(isCustomerExist);

            //Act
            var result = await cartService.TryChangeProductCountInCartAsync(updateDtoCountOne);

            //Asset
            result.Should().BeFalse();
        }

        [Fact]
        public async void ProductInCartNotExistAndCustomerAndProductExist_ProductInCartCreatedReturnTrue()
        {
            //Arrange
            ProductInCartNotExist();

            SetProductExistance(true);

            SetCustomerExistance(true);

            //Act
            var result = await cartService.TryChangeProductCountInCartAsync(updateDtoCountOne);

            //Asset
            result.Should().BeTrue();
        }
    }
}
